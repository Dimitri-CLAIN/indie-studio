/*
** EPITECH PROJECT, 2020
** MeshError
** File description:
** MeshError.hpp
*/

#ifndef __MESH_ERROR_HPP__
#define __MESH_ERROR_HPP__

#include "IndieError.hpp"

namespace indie {
    class MeshError : public IndieError
    {
    public:
        MeshError(const std::string &where, const std::string &what)
        : IndieError(where, what) {};
    };
}

#endif //__MESH_ERROR_HPP__
