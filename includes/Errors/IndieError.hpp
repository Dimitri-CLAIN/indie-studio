/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** IndieError.hpp
*/

#ifndef MAP_GEN_INDIEERROR_HPP
#define MAP_GEN_INDIEERROR_HPP

#include <exception>
#include <string>

namespace indie
{
    class IndieError : public std::exception
    {
    public:
        IndieError(const std::string &where, const std::string &what);
        const char *what() const noexcept final;
        const std::string &where() const noexcept;

    protected:
        const std::string _origin;
        const char *_message;
    };
}

#endif //MAP_GEN_INDIEERRORS_HPP
