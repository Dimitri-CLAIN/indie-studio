/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** FileError.hpp
*/

#ifndef MAP_GEN_FILEERROR_HPP
#define MAP_GEN_FILEERROR_HPP

#include "IndieError.hpp"

namespace indie
{
    class FileError : public IndieError
    {
    public:
        FileError(const std::string &where, const std::string &what)
        : IndieError(where, what) {};
    };
}

#endif //MAP_GEN_FILEERROR_HPP
