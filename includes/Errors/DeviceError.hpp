/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** DeviceError.hpp
*/

#ifndef MAP_GEN_DEVICEERROR_HPP
#define MAP_GEN_DEVICEERROR_HPP

#include "IndieError.hpp"

namespace indie
{
    class DeviceError : public IndieError
    {
    public:
        DeviceError(const std::string &where, const std::string &what)
        : IndieError(where, what) {};
    };
}

#endif //MAP_GEN_DEVICEERROR_HPP
