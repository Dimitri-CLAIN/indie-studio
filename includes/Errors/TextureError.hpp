/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** TextureError.hpp
*/

#ifndef MAP_GEN_TEXTUREERROR_HPP
#define MAP_GEN_TEXTUREERROR_HPP

#include "IndieError.hpp"

namespace indie
{
    class TextureError : public IndieError
    {
    public:
        TextureError(const std::string &where, const std::string &what)
        : IndieError(where, what) {};
    };
}

#endif //MAP_GEN_TEXTUREERROR_HPP
