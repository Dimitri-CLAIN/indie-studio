/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Crate.hpp
*/

#ifndef MAP_GEN_CRATE_HPP
#define MAP_GEN_CRATE_HPP

#include "Block.hpp"

namespace indie
{
    class Crate : public Block
    {
    public:
        explicit Crate(float x, float y);
        Crate() = delete;
        Crate(const Crate &bloc) = delete;
    };
}

#endif //MAP_GEN_CRATE_HPP
