/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Wall.hpp
*/

#ifndef MAP_GEN_WALL_HPP
#define MAP_GEN_WALL_HPP

#include "Block.hpp"

namespace indie
{
    class Wall : public Block
    {
    public:
        explicit Wall(float x, float y);
        Wall() = delete;
        Wall(const Wall &wall) = delete;
    };
}

#endif //MAP_GEN_WALL_HPP
