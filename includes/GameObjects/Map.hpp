/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Map.hpp
*/

#ifndef MAP_GEN_MAP_HPP
#define MAP_GEN_MAP_HPP

#include "Block.hpp"
#include "map_tools.hpp"

#include <algorithm>

namespace indie
{
    class Map
    {
    public:
        explicit Map(const indie::mapPlan_t &mapPlan);
        Map() = delete;
        Map(const Map &map);
        Map &operator=(const Map &map);

        const unsigned int getWidth() const noexcept;
        const unsigned int getHeight() const noexcept;
        std::shared_ptr<Block> getBlockByPos(float posX, float posZ);
        const std::vector<std::shared_ptr<indie::Block>> &getBlocks() const noexcept;
        std::vector<std::shared_ptr<indie::Block>> &retrieveBlocks() noexcept;
        mapPlan_t &getGrid();

//        std::shared_ptr<indie::Block> findBlockFromPos(const vector2<int> &coords);

    private:
        indie::mapPlan_t _grid;
        std::vector<std::shared_ptr<indie::Block>> _blocks;
        unsigned int _width, _height;
        //std::vector<std::vector<std::shared_ptr<indie::Block>>> _blocks;
    };
}

#endif //MAP_GEN_MAP_HPP
