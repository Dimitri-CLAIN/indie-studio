/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Block.hpp
*/

#ifndef MAP_GEN_BLOCK_HPP
#define MAP_GEN_BLOCK_HPP

#include "Entity.hpp"
#include "PositionComponent.hpp"
#include "TextureComponent.hpp"
#include "ColliderComponent.hpp"
#include "CubeComponent.hpp"

namespace indie
{
    class Block
    {
    public:
        explicit Block(float x, float y, ColliderStatus isBreakable, const std::string &texPath);
        Block() = delete;
        Block(const Block &block);
        Block &operator=(const Block &block);

        std::shared_ptr<PositionComponent> getPosition() noexcept;
        std::shared_ptr<TextureComponent> getTexture() noexcept;
        ENTITY &getEntity();

    protected:
        ENTITY _e;
    };
}

#endif //MAP_GEN_BLOCK_HPP
