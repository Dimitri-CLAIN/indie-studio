/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Bomb
*/

#ifndef BOMB_HPP_
#define BOMB_HPP_

#include "Block.hpp"

#include <chrono>
#include "map_tools.hpp"

namespace indie
{
class Bomb : public Block
    {
    public:
        explicit Bomb(float pos_x, float pos_y, int dmg, int range);
        Bomb() = delete;
        Bomb(const Bomb &bomb) = delete;
        void explode();
        void clock();
        bool amIExploded();
        std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>> getExplosion();
        std::chrono::time_point<std::chrono::system_clock> getbtimer();
        const vector2<int> &getGridPos() const noexcept;


    private:
        std::chrono::time_point<std::chrono::system_clock> _before;
        int _dmg;
        int _range;
        int _hp;
        bool _exploded;
        float _pos_x;
        float _pos_y;
        const vector2<int> _gPos;
    };
}

#endif /* !BOMB_HPP_ */
