/*
** EPITECH PROJECT, 2020
** Particuls.hpp
** File description:
** Particuls
*/

#ifndef PARTICULS_HPP_
#define PARTICULS_HPP_

#include <irrlicht.h>

class Particuls {
    public:
        Particuls(irr::scene::ISceneManager *smgr, irr::core::aabbox3d<irr::f32> coord, const irr::core::vector3df &dir, irr::video::IVideoDriver *driver, irr::video::ITexture *texture, irr::core::vector3df pos);
        ~Particuls();

        irr::scene::IParticleSystemSceneNode *getParticuls();

    protected:
    private:
        irr::scene::IParticleSystemSceneNode *_particuleSystem;
};

#endif /* !PARTICULS_HPP_ */
