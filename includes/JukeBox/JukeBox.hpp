/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** JukeBox.hpp
*/

#ifndef MAP_GEN_JUKEBOX_HPP
#define MAP_GEN_JUKEBOX_HPP

#include "IScene.hpp"
#include <SFML/Audio.hpp>

namespace indie
{
    class JukeBox {
    public:
        enum typeSound {
            NoThing = -1,
            Damage,
            Explosion,
            Death,
            Button
        };
    public:
        // Ctor
        JukeBox();

        // Ctor Cpy
        JukeBox(const JukeBox &jukeBoxCpy) = delete;
        JukeBox &operator=(const JukeBox &jukeBoxCpy) = delete;

        // Dtor
        ~JukeBox();

        // Member
        void startMusic(IScene::typeScene nextMusic);
        void changeMusic(IScene::typeScene nextMusic);
        bool isMusic(IScene::typeScene lastMusic);
        void endMusic();
        void mute();
        void deMute();
        void playSound(typeSound typeOfSound);
        void endSound();
        void changeMusicVolumeUp();
        void changeMusicVolumeDown();
        void changeSoundVolumeUp();
        void changeSoundVolumeDown();
    private:
        // Variable
        IScene::typeScene _actualMusic;
        sf::Music _music;
        float _musicVolume;
        sf::SoundBuffer _soundBuffer;
        sf::Sound _sound;
        float _soundVolume;
    };
}

#endif //MAP_GEN_JUKEBOX_HPP
