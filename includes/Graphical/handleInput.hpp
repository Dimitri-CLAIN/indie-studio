/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** handleInput.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_HANDLEINPUT_HPP
#define OOP_INDIE_STUDIO_2019_HANDLEINPUT_HPP

#include <iostream>
#include <irrlicht.h>
#include <map>

class handleInput : public irr::IEventReceiver
{
public:
    handleInput();
    virtual bool OnEvent(const irr::SEvent& event);
    void ResetList(void);
    const std::map<std::string, bool> &GetList(void);

private:
    std::map<std::string, bool> inputs;
};

#endif