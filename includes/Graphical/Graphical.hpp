/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** Graphical.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_GRAPHICAL_HPP
#define OOP_INDIE_STUDIO_2019_GRAPHICAL_HPP

#include <iostream>
#include <irrlicht.h>
#include <string>
#include <chrono>
#include <unordered_map>
#include "IGraphical.hpp"
#include "DeviceError.hpp"
#include "TextureError.hpp"
#include "handleInput.hpp"
#include "Map.hpp"
#include "Particuls.hpp"

#include "SizeComponent.hpp"
#include "TextureComponent.hpp"
#include "MeshComponent.hpp"
#include "StringComponent.hpp"
#include "ColorComponent.hpp"
#include "PositionComponent.hpp"
#include "ColliderComponent.hpp"
#include "ActionComponent.hpp"
#include "CubeComponent.hpp"

#define GROUND_TEXTURE  "./files/textures/groundTile.png"
#define WALL_TEXTURE    "./files/textures/wall.png"
#define CRATE_TEXTURE   "./files/textures/crate.png"
#define FIRE_TEXTURE    "./files/textures/fireball.png"
#define TEST_TEXTURE   "./files/textures/test.png"
#define TITLEBACK_TEXTURE   "./files/textures/Title_back.png"
#define TITLEBT_TEXTURE   "./files/textures/Title_bt.png"
#define MENUBACK_TEXTURE   "./files/textures/menubk.png"
#define MENUPBT_TEXTURE   "./files/textures/Menu_Pbt.png"
#define MENUOBT_TEXTURE   "./files/textures/Menu_Obt.png"
#define GAMEBACK_TEXTURE   "./files/textures/gamebk.png"
#define MUSICPBT_TEXTURE   "./files/textures/Music_Pbt.png"
#define SOUNDPBT_TEXTURE   "./files/textures/Sound_Pbt.png"
#define MENUQBT_TEXTURE   "./files/textures/Menu_Qbt.png"
#define ONEPLAYER_TEXTURE   "./files/textures/1Player.png"
#define TWOPLAYER_TEXTURE   "./files/textures/2Player.png"
#define THREEPLAYER_TEXTURE   "./files/textures/3Player.png"
#define FOURPLAYER_TEXTURE   "./files/textures/4Player.png"
#define BACKBT_TEXTURE   "./files/textures/Backbt.png"
#define MENURBT_TEXTURE   "./files/textures/Menu_Rbt.png"

#define PLAYERO_TEXTURE   "./files/textures/PLAYER1.png"
#define PLAYERT_TEXTURE   "./files/textures/PLAYER2.png"
#define PLAYERTH_TEXTURE   "./files/textures/PLAYER3.png"
#define PLAYERF_TEXTURE   "./files/textures/PLAYER4.png"
#define DRAW_TEXTURE   "./files/textures/DRAW.png"

#define W_WIDHT 800
#define W_HEIGHT 600

namespace indie {
    class Graphical : public IGraphical {
    public:
        Graphical();
        Graphical(const Graphical &graphicalCpy);
        Graphical &operator=(const Graphical &graphicalCpy);
        ~Graphical() override;
        void initWindow() override;
        void closeWindow() override;
        const std::map<std::string, bool> &handleInputs() override;

        void displayScene(std::vector<ENTITY> &entities) override;
        void createCamera();

        void initTexture(ENTITY &entitiy);
        void initMesh(ENTITY &entitiy);
        void entityToMesh(ENTITY &entity);
        void EntityToBlock(ENTITY &entity, std::vector<ENTITY> &entities);
        void manageEntities(std::vector<ENTITY> &entities);
        void renderTerrain(const std::unique_ptr<indie::Map> &map, std::vector<ENTITY> &entities);

        void addTexture(const std::shared_ptr<indie::TextureComponent> &tex);
        void createGround(const unsigned int width, const unsigned int height);
        void renderMap(const std::unique_ptr<indie::Map> &map, std::vector<ENTITY> &entities);

        const bool isWindowActive();
        void clearWindow();

        irr::IrrlichtDevice *getDevice(void);
        irr::video::IVideoDriver *getDriver(void);
        void EntityToSprite(ENTITY &entity);
        handleInput *getReceiver();
        void drawParts(void);
        void pushParts(std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>>);
        void pushbtimer(std::chrono::time_point<std::chrono::system_clock>);

        void setPlayerNb(int val);
        int &getPlayerNb(void);
        void deleteComponent(irr::scene::IMeshSceneNode *node);
        void clearsmgr(void);
        void setWinner(int val);
        int getWinner(void);

    private:
        irr::IrrlichtDevice *_device;
        handleInput *_receiver;
        irr::scene::ISceneManager *_smgr;
        irr::video::IVideoDriver *_driver;
        irr::gui::IGUIEnvironment *_guienv;
        std::unordered_map<std::string, irr::video::ITexture *> _textures;
        std::vector<std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>>> _parts;
        std::vector<std::vector<Particuls>> _particuls;
        std::vector<std::chrono::time_point<std::chrono::system_clock>> _btimer;
        int _playerNb;
        int _winner;
    };
}

#endif /* OOP_INDIE_STUDIO_2019_GRAPHICAL_HPP */
