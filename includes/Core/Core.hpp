/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Core.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_CORE_HPP
#define OOP_INDIE_STUDIO_2019_CORE_HPP

#include "IScene.hpp"
#include "Title.hpp"
#include "GameScene.hpp"
#include "Menu.hpp"
#include "Options.hpp"
#include "SelectSave.hpp"
#include "SelectPlayer.hpp"
#include "Graphical.hpp"
#include "JukeBox.hpp"

#include <memory>
#include <vector>
#include <algorithm>
#include <chrono>

namespace indie
{
    class Core {
    public:
        // Ctor
        Core();

        Core(const Core &coreCpy) = delete;

        Core &operator=(const Core &coreCpy) = delete;

        // Dtor
        ~Core() = default;

        // Member
        void generalLoop(void);

    private:
        //Member
        bool clock();

        // Variable
        std::vector<std::shared_ptr<IScene>> _scenes;
        IScene::typeScene _actualScene;
        IScene::typeScene _lastScene;
        std::shared_ptr<Graphical> _display;
        std::shared_ptr<JukeBox> _jukebox;
    };
}

#endif //OOP_INDIE_STUDIO_2019_CORE_HPP