/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** SelectSave.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_SELECTSAVE_HPP
#define OOP_INDIE_STUDIO_2019_SELECTSAVE_HPP

#include "IScene.hpp"
#include "Graphical.hpp"
#include "JukeBox.hpp"
#include "Button.hpp"

namespace indie
{
    class SelectSave : public IScene {
    public:
        enum event {
            Nothing = -1,
            ClickSave,
            ClickConfirm,
            Back
        };

    public:
        // ctor
        SelectSave() = delete;
        explicit SelectSave(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox);
        SelectSave(const SelectSave &SelectSaveCpy) = default;
        SelectSave &operator=(const SelectSave &SelectSaveCpy) = default;

        // dtor
        ~SelectSave() = default;

        // Member
        void init() override;
        void dest() override;
        typeScene launchLoop() override;
        indie::IScene::typeScene inputAction(std::map<std::string, bool> inputs);
        void createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY);
        void updateEntities();

    private:
        // Member
        //Variable
        std::vector<ENTITY> _entities;
        std::weak_ptr<Graphical> _display;
        event _playerEvent;
        std::weak_ptr<JukeBox> _jukebox;
        std::vector<indie::Button> _buttons;
        int _cursor;
    };
}

#endif //OOP_INDIE_STUDIO_2019_SELECTSAVE_HPP
