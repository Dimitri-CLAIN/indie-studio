/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Menu.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_MENU_HPP
#define OOP_INDIE_STUDIO_2019_MENU_HPP

#include "IScene.hpp"
#include "Graphical.hpp"
#include "JukeBox.hpp"
#include "Button.hpp"

namespace indie
{
    class Menu : public IScene {
    public:
        enum event {
            Nothing = -1,
            ClickNewGame,
            ClickLoadGame,
            CLickOption,
            ClickQuit
        };

    public:
        // ctor
        Menu() = delete;
        explicit Menu(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox);
        Menu(const Menu &menuCpy) = default;
        Menu &operator=(const Menu &menuCpy) = default;

        // dtor
        ~Menu() = default;

        // Member
        void init() override;
        void dest() override;
        typeScene launchLoop() override;
        void updateEntities();
        indie::IScene::typeScene inputAction(std::map<std::string, bool> inputs);
        void createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY);

    private:
        // Member
        //Variable
        std::vector<ENTITY> _entities;
        int _cursor;
        std::weak_ptr<Graphical> _display;
        event _playerEvent;
        std::weak_ptr<JukeBox> _jukebox;
        std::vector<indie::Button> _buttons;
    };
}

#endif //OOP_INDIE_STUDIO_2019_MENU_HPP
