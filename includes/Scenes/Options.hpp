/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Options.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_OPTIONS_HPP
#define OOP_INDIE_STUDIO_2019_OPTIONS_HPP

#include "IScene.hpp"
#include "Graphical.hpp"
#include "JukeBox.hpp"
#include "Button.hpp"

namespace indie
{
    class Options : public IScene {
    public:
        enum event {
            Nothing = -1,
            Back
        };

    public:
        // ctor
        Options() = delete;
        explicit Options(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox);
        Options(const Options &OptionCpy) = default;
        Options &operator=(const Options &OptionCpy) = default;

        // dtor
        ~Options() = default;

        // Member
        void init() override;
        void dest() override;
        typeScene launchLoop() override;

    private:
        // Member
        typeScene inputAction(std::map<std::string, bool> inputs);
        void updateEntities();
        void createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY);

        //Variable
        std::vector<ENTITY> _entities;
        int _cursor;
        std::weak_ptr<Graphical> _display;
        event _playerEvent;
        std::weak_ptr<JukeBox> _jukebox;
        std::vector<indie::Button> _buttons;
    };
}

#endif //OOP_INDIE_STUDIO_2019_OPTIONS_HPP
