/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Title.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_Title_HPP
#define OOP_INDIE_STUDIO_2019_Title_HPP

#include "IScene.hpp"
#include "Button.hpp"
#include "Graphical.hpp"
#include "JukeBox.hpp"

namespace indie
{
    class Title : public IScene {
    public:
        enum event {
            Nothing = -1,
            DoAnithing
        };

    public:
        // ctor
        Title() = delete;
        explicit Title(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox);
        Title(const Title &TitleCpy) = default;
        Title &operator=(const Title &TitleCpy) = default;

        // dtor
        ~Title() = default;

        // Member
        void init() override;
        void dest() override;
        typeScene launchLoop() override;
        void updateEntities();
        indie::IScene::typeScene inputAction(std::map<std::string, bool> inputs);
        void createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY);

    private:
        // Member
        //Variable
        std::vector<indie::Button> _buttons;
        int _cursor;
        std::vector<ENTITY> _entities;
        std::weak_ptr<Graphical> _display;
        event _playerEvent;
        std::weak_ptr<JukeBox> _jukebox;
    };
}

#endif //OOP_INDIE_STUDIO_2019_Title_HPP
