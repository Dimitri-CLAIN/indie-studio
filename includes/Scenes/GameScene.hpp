/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** GameScene.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_GAMESCENE_HPP
#define OOP_INDIE_STUDIO_2019_GAMESCENE_HPP

#include <memory>
#include "IScene.hpp"
#include "Graphical.hpp"
#include "Perso.hpp"
#include "MapFactory.hpp"
#include "Entity.hpp"
#include "CubeComponent.hpp"
#include "JukeBox.hpp"

namespace indie
{
    class GameScene : public IScene {
    public:
        enum event {
            Nothing = -1,
            Back
        };
        std::string inputTables[4][5] = {
                {"z", "s", "q", "d", "a"},
                {"t", "g", "f", "h", "r"},
                {"o", "l", "k", "m", "i"},
                {"UP", "DOWN", "LEFT", "RIGHT", " "}
        };

    public:
        // ctor
        GameScene() = delete;
        explicit GameScene(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox);
        GameScene(const GameScene &GameSceneCpy);
        GameScene &operator=(const GameScene &GameSceneCpy);

        // dtor
        ~GameScene() = default;

        // Member
        void init() override;
        void dest() override;
        typeScene launchLoop() override;
        void setPosition();
        void createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY);
        void createPlayers(void);
        void putBomb(std::map<std::string, bool> inputs);
        void manageBombs();

        void destroyMap(const vector2<int> &origin);
        void destroyBlock(const vector2<int> &coords);
        bool checkEndOfTheGame();

    private:
        // Member
        void inputAction(std::map<std::string, bool> inputs);
        void removeEntityByID(ID, std::vector<std::unique_ptr<indie::Bomb>>::iterator);
        //Variable
        std::vector<std::shared_ptr<indie::Perso>> _players;

        std::vector<ENTITY> _entities;
        std::weak_ptr<Graphical> _display;
        event _playerEvent;
        std::unique_ptr<indie::Map> _map;
        std::weak_ptr<JukeBox> _jukebox;
        std::vector <std::unique_ptr<Bomb>> _bombs;
    };
}

#endif //OOP_INDIE_STUDIO_2019_GAMESCENE_HPP
