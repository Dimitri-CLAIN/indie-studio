/*
** EPITECH PROJECT, 2020
** Collision
** File description:
** Collision
*/

#ifndef COLLISION_HPP_
#define COLLISION_HPP_

#include <irrlicht.h>

namespace indie {
    class Collision {
        public:
            Collision();
            ~Collision();
            static void setCollision(irr::scene::IAnimatedMeshSceneNode *, irr::scene::IMeshSceneNode *, irr::scene::ISceneManager *);
    };
}

#endif /* !COLLISION_HPP_ */
