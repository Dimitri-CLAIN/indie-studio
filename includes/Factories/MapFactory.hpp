/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** MapFactory.hpp
*/

#ifndef MAP_GEN_MAPFACTORY_HPP
#define MAP_GEN_MAPFACTORY_HPP

#include <vector>
#include <ostream>
#include <string>
#include <memory>
#include "Map.hpp"
#include "map_tools.hpp"

namespace indie
{
    class MapFactory
    {
    public:
        static mapPlan_t createMapPlan(const std::string &filename = "./files/maps/map1.txt");
        static void checkMap(const mapPlan_t &map);
        static std::shared_ptr<indie::Map>createMapFromMapPlan(const indie::mapPlan_t &map);

    private:
        static void readObstacleLine(std::vector<slot_t> &obstacles, const std::string &line);
        static void cleanCorners(mapPlan_t &map);
    };
}

#endif //MAP_GEN_MAPFACTORY_HPP
