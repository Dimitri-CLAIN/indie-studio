/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** map_tools.hpp
*/

#ifndef MAP_GEN_MAP_TOOLS_HPP
#define MAP_GEN_MAP_TOOLS_HPP

#include <fstream>
#include <vector>

namespace indie
{
    typedef enum slotType_e
    {
        EMPTY, CRATE, WALL
    } slot_t;

    std::ostream &operator<<(std::ostream &os, const slot_t &s);

    typedef std::vector<std::vector<slot_t>> mapPlan_t;

    template <typename T = int>
    struct vector2 {
        T x;
        T y;

        vector2<T>() {
            x = 0;
            y = 0;
        }

        vector2<T>(const T &v1, const T &v2) {
            x = v1;
            y = v2;
        }
        bool operator==(const vector2<T> &vec) const {
            return x == vec.x && y == vec.y;
        }
    };
}

#endif //MAP_GEN_MAP_TOOLS_HPP
