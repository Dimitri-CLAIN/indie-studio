/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** Button.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_Button_HPP
#define OOP_INDIE_STUDIO_2019_Button_HPP

#include <iostream>
#include <irrlicht.h>
#include "Entity.hpp"
#include "IScene.hpp"
#include "Component.hpp"
#include "PositionComponent.hpp"
#include "SizeComponent.hpp"
#include "TextureComponent.hpp"

namespace indie
{
    enum status
    {
        ON,
        OFF
    };

    struct Button {
    public:
        Button(IScene::typeScene scene, const std::string &tex, float sizeX, float sizeY, float posX, float posY);
        ~Button();

        void changeState(status state);
        void refresh();

        ENTITY &getEntity();
        IScene::typeScene getScene();

    private:
        status _state;
        ENTITY _sprite;
        IScene::typeScene _scene;
        std::string _basicTexture;
        float _sizeX;
        float _sizeY;
    };
}

#endif //OOP_INDIE_STUDIO_2019_Button_HPP
