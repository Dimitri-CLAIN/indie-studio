/*
** EPITECH PROJECT, 2020
** Perso
** File description:
** Perso
*/

#ifndef PERSO_HPP_
#define PERSO_HPP_

#include <vector>
#include <algorithm>

#include "ColliderComponent.hpp"
#include "NameComponent.hpp"
#include "PositionComponent.hpp"
#include "MeshComponent.hpp"
#include "ActionComponent.hpp"
#include "Entity.hpp"
#include "Bomb.hpp"

namespace indie
{
    class Perso {
        public:
            Perso(float positionX, float positionY, float positionZ, const std::string &name);
            Perso(const Perso &perso);
            Perso &operator=(const Perso &perso);
            ~Perso() = default;

            void creatStatistics();
            void animeRun(bool activate);
            void animeStand(bool activate);
            // void animePutABomb(bool activate);
            bool takeDamage();
            void putABomb();
            ActionComponent::action actionInput(std::map<std::string, bool> inputs);
            void assignInput(std::string up, std::string down, std::string left, std::string right, std::string bomb);
            std::vector <std::unique_ptr<indie::Bomb>> &getBombs();
            irr::core::vector3df &getPosition();
            const std::string &getName() const;
            const int getMaxBombs() const;
            void destroyBombs(const ID &bombId);
            ENTITY &getEntity();
            const int &getLife() const noexcept;

    private:
            void move(const ActionComponent::action &dir);
            bool isBombAlreadyPosed(float x, float z);
            std::string _name;
            irr::core::vector3df _pos;
            ENTITY _entity;
            bool _is_running;
            int _hp;
            int _maxBombs;
            std::map <ActionComponent::action, std::string> _inputs;
            std::vector <std::unique_ptr<indie::Bomb>> _bombs;
            int _bonusRange;
            int _bonusDmg;
    };
    std::ostream &operator<<(std::ostream &os, Perso &perso);
}
#endif /* !PERSO_HPP_ */
