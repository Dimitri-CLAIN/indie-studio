/*
** EPITECH PROJECT, 2020
** Entity
** File description:
** Entity
*/

#ifndef OOP_INDIE_2019_ENTITY_HPP
#define OOP_INDIE_2019_ENTITY_HPP

#include <vector>
#include <map>
#include <memory>
#include "../components/Component.hpp"
#include "../ID/ID.hpp"

#define NEW std::make_shared
#define ENTITY std::shared_ptr<indie::Entity>

namespace indie
{
    class Entity {
    public:
        Entity();
        Entity(const Entity &entity);
        Entity &operator=(const Entity &entity);
        ~Entity();

        virtual void add(std::shared_ptr<Component> component);

        virtual ID getID() const;
        template <typename T>
        std::shared_ptr<T> get() {
            for (size_t i = 0; i < _component.size(); i++) {
                if (std::dynamic_pointer_cast<T>(_component[i]))
                    return (std::dynamic_pointer_cast<T>(_component[i]));
            }
            return (nullptr);
        }
    protected:
        ID _id;
        std::vector<std::shared_ptr <Component>> _component;
    };
}
#endif //OOP_INDIE_2019_ENTITY_HPP
