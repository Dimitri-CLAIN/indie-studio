/*
** EPITECH PROJECT, 2020
** id
** File decription:
** ID.hpp:
*/

#ifndef OOP_INDIE_2019_ID_HPP
#define OOP_INDIE_2019_ID_HPP

class ID {
public:
    ID();
    ~ID();

    bool operator==(ID const &id) const;

    static ID generate();
    unsigned int getID() const;
private:
    unsigned int _id;
};

#endif //OOP_INDIE_2019_ID_HPP
