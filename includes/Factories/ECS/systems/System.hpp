/*
** EPITECH PROJECT, 2020
** system
** File description:
** System
*/

#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

#include <vector>
#include "Entity.hpp"

class System {
    public:
        System() = default;
        ~System() = default;

        void addEntity(ENTITY entity);

        const std::vector<ENTITY> &getEntities() const;
        // ENTITY &getEntity(ID &id);

    private:
        std::vector<ENTITY> _entities;
};

#endif /* !SYSTEM_HPP_ */
