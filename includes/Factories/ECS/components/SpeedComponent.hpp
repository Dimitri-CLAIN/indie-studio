/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** SpeedComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_SPEEDCOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_SPEEDCOMPONENT_HPP

#include "Component.hpp"

namespace indie
{
    struct SpeedComponent : public indie::Component {
        SpeedComponent(float Tspeed);
        ~SpeedComponent() override;

        float speed;
    };
}

#endif //OOP_INDIE_STUDIO_2019_SPEEDCOMPONENT_HPP
