/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** ColliderComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_COLLIDERCOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_COLLIDERCOMPONENT_HPP

#include "Component.hpp"

namespace indie
{
    enum ColliderStatus {
        Breakable,
        Unbreakable,
        Nothing
    };
    struct ColliderComponent : public indie::Component
    {
        ColliderComponent(ColliderStatus TisCollider);
        ~ColliderComponent() override;
        ColliderStatus isCollider;
    };
}

#endif //OOP_INDIE_STUDIO_2019_COLLIDERCOMPONENT_HPP
