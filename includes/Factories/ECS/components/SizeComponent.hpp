/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** SizeComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_SIZECOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_SIZECOMPONENT_HPP

#include "Component.hpp"

namespace indie
{
    struct SizeComponent : public indie::Component {
    public:
        SizeComponent(float TsizeY, float TsizeX);
        ~SizeComponent() override;

        void incY(float val);
        void incX(float val);
        void setY(float val);
        void setX(float val);
        float sizeY;
        float sizeX;
    };
}
#endif //OOP_INDIE_STUDIO_2019_SIZECOMPONENT_HPP
