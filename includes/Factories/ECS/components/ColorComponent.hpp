/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** ComponentColor.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_COLORCOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_COLORCOMPONENT_HPP

#include "Component.hpp"

namespace indie
{
    struct ColorComponent : public indie::Component
    {
        ColorComponent(unsigned char Tred, unsigned char Tgreen, unsigned char Tblue);
        ~ColorComponent() override;

        void update(unsigned char Tred, unsigned char Tgreen, unsigned char Tblue);

        unsigned char red;
        unsigned char green;
        unsigned char blue;
    };
}

#endif //OOP_INDIE_STUDIO_2019_COLORCOMPONENT_HPP
