/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** PositionComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_POSITIONCOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_POSITIONCOMPONENT_HPP

#include "Component.hpp"

namespace indie
{
    struct PositionComponent : public indie::Component {
        PositionComponent(float Tx, float Ty);
        ~PositionComponent() override;

        float x;
        float y;
    };
}
#endif //OOP_INDIE_STUDIO_2019_POSITIONCOMPONENT_HPP
