/*
** EPITECH PROJECT, 2020
** component
** File decription:
** Component.hpp:
*/

#ifndef OOP_INDIE_2019_COMPONENT_HPP
#define OOP_INDIE_2019_COMPONENT_HPP

namespace indie
{
    struct Component {
        Component();
        virtual ~Component() = default;
    };
}
#endif //OOP_INDIE_2019_COMPONENT_HPP
