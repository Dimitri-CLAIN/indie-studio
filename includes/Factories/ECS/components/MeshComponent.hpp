/*
** EPITECH PROJECT, 2020
** MeshComponent
** File description:
** MeshComponent
*/

#ifndef MESHCOMPONENT_HPP_
#define MESHCOMPONENT_HPP_

#include <irrlicht.h>
#include <iostream>
#include "Entity.hpp"
#include "MeshError.hpp"
#include "Component.hpp"

class MeshComponent : public indie::Component {
    public:
        MeshComponent(const std::string &path, const std::string &texturePath);
        MeshComponent(const std::shared_ptr<MeshComponent> &mesh);
        ~MeshComponent();

        void loadMesh(irr::scene::ISceneManager *smgr, irr::video::IVideoDriver *driver);
        void changeAnimation(enum irr::scene::EMD2_ANIMATION_TYPE animation);

        irr::scene::IAnimatedMesh* getMesh() const;
        const bool &getIsLoad() const;
        const std::string &getModelPath() const;
        const std::string &getTexturePath() const;
        irr::scene::IAnimatedMeshSceneNode *getNode() const;

    private:
        std::string _modelPath;
        std::string _texturePath;
        irr::scene::IAnimatedMeshSceneNode *_node;
        bool _isLoad;
        irr::scene::IAnimatedMesh* _mesh;
};

#endif /* !MESHCOMPONENT_HPP_ */
