/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** StringComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_STRINGCOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_STRINGCOMPONENT_HPP

#include <string>
#include "Component.hpp"

namespace indie
{
    struct StringComponent : public indie::Component {
        StringComponent(const std::string &Tstring);
        ~StringComponent() override;

        void update(const std::string &Tstring);

        std::string string;
    };
}

#endif //OOP_INDIE_STUDIO_2019_STRINGCOMPONENT_HPP
