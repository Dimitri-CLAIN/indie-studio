/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** ActionComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_DIRECTIONCOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_DIRECTIONCOMPONENT_HPP

#include <irrlicht.h>
#include <cmath>
#include "Component.hpp"

struct ActionComponent : public indie::Component {
    enum action {
        NOTHING = -1,
        UP,
        DOWN,
        RIGHT,
        LEFT,
        BOMB
    };
    ActionComponent(const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0));
    ~ActionComponent() override;

    void changeDirection(const ActionComponent::action &dir, irr::scene::IAnimatedMeshSceneNode *node);

    irr::core::vector3df _rotation;
};

#endif //OOP_INDIE_STUDIO_2019_DIRECTIONCOMPONENT_HPP