/*
** EPITECH PROJECT, 2020
** CubeComponent
** File description:
** CubeComponent
*/

#ifndef CubeCOMPONENT_HPP_
#define CubeCOMPONENT_HPP_

#include <irrlicht.h>
#include <iostream>
#include "Entity.hpp"
#include "Component.hpp"

class CubeComponent : public indie::Component {
    public:
        CubeComponent();
        ~CubeComponent();

        void loadCube(irr::scene::ISceneManager *smgr, unsigned x, unsigned z);

        const bool &getIsLoad() const;
        irr::scene::IMeshSceneNode *getNode() const;

    private:

        irr::scene::IMeshSceneNode *_node;
        bool _isLoad;
};

#endif /* !CubeCOMPONENT_HPP_ */
