/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** NameComponent.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_NAMECOMPONENT_HPP
#define OOP_INDIE_STUDIO_2019_NAMECOMPONENT_HPP

#include <string>
#include "Component.hpp"

namespace indie
{
    struct NameComponent : public indie::Component {
        NameComponent(const std::string &Tname);
        ~NameComponent() override;

        std::string name;
    };
}
#endif //OOP_INDIE_STUDIO_2019_NAMECOMPONENT_HPP
