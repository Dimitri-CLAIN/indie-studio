/*
** EPITECH PROJECT, 2020
** TextureComponent
** File description:
** TextureComponent
*/

#ifndef TEXTURECOMPONENT_HPP_
#define TEXTURECOMPONENT_HPP_

#include <irrlicht.h>
#include <iostream>
#include "Component.hpp"
#include "TextureError.hpp"

namespace indie
{
    class TextureComponent : public indie::Component
    {
    public:
        TextureComponent(const std::string &path);
        ~TextureComponent();
        const std::string &getPath() const;
        bool isLoad();
        void setIsLoad(bool);

    private:
        std::string _path;
        bool _isLoad;
    };
}
#endif /* !TEXTURECOMPONENT_HPP_ */
