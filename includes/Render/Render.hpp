/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Render.hpp
*/

#ifndef MAP_GEN_RENDER_HPP
#define MAP_GEN_RENDER_HPP

#include <irrlicht.h>

#include "Map.hpp"

namespace indie
{
    class Render
    {
    public:
        Render();
        ~Render();
        irr::IrrlichtDevice *getDevice() const noexcept;
        void renderTerrain(const std::shared_ptr<Map> map);
        void createCamera();

    private:
        irr::IrrlichtDevice *_device;
        irr::scene::ISceneManager *_smgr;
        irr::video::ITexture *_groundTileTex, *_wallTex, *_crateTex;

        void createGround(unsigned width, unsigned height);
        void renderMap(const std::shared_ptr<Map> map);
        void placeCube(irr::video::ITexture *tex, unsigned x, unsigned z);
    };
}

#endif //MAP_GEN_RENDER_HPP
