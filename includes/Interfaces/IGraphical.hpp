/*
** EPITECH PROJECT, 2020
** OOP_Indie_studio_2019
** File decription:
** IGraphical.hpp:
*/

#ifndef OOP_INDIE_STUDIO_2019_IGRAPHICAL_HPP
#define OOP_INDIE_STUDIO_2019_IGRAPHICAL_HPP

#include "Entity.hpp"

class IGraphical {
public:
    typedef struct pos_t {
        float x;
        float y;
    } pos_t;

    typedef struct color_t {
        int r;
        int g;
        int b;
    } color_t;

    virtual ~IGraphical() = default;
    virtual void initWindow() = 0;
    virtual void closeWindow() = 0;
    virtual const std::map<std::string, bool> &handleInputs() = 0;
    virtual void displayScene(std::vector<ENTITY> &) = 0;
};

#endif /* OOP_INDIE_STUDIO_2019_IGRAPHICAL_HPP */