/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** IScene.hpp
*/

#ifndef OOP_INDIE_STUDIO_2019_ISCENE_HPP
#define OOP_INDIE_STUDIO_2019_ISCENE_HPP

namespace indie
{
    class IScene {
    public:
        enum typeScene {
            Nothing = -1,
            Title,
            Menu,
            Options,
            SelectSave,
            SelectPlayer,
            GameScene,
            Quit
        };

    public:
        ~IScene() = default;

        virtual void init() = 0;
        virtual void dest() = 0;
        virtual typeScene launchLoop(void) = 0;
    };
}

#endif //OOP_INDIE_STUDIO_2019_ISCENE_HPP
