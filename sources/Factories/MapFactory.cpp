/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** MapFactory.cpp
*/

#include <fstream>
#include <algorithm>
#include <random>

#include "MapFactory.hpp"
#include "FileError.hpp"

indie::mapPlan_t indie::MapFactory::createMapPlan(const std::string &filename)
{
    std::fstream fs;
    mapPlan_t map;

    fs.open(filename);
    if (fs.is_open() == false)
        throw indie::FileError("Map Creation", "Can't open file " + filename + ".");

    while (fs.eof() == false) {
        std::string line;
        std::getline(fs, line);
        if (line.empty() == true)
            continue;
        std::vector<slot_t> obstacles;
        readObstacleLine(obstacles, line);
        map.emplace_back(obstacles);
    }
    cleanCorners(map);

    fs.close();
    return map;
}

void indie::MapFactory::readObstacleLine(std::vector<slot_t> &obstacles, const std::string &line)
{
    static std::random_device rd;
    static std::mt19937 mt(rd());
    static std::uniform_int_distribution<unsigned> dist(0, 3);

    std::for_each(begin(line), end(line), [&] (const char &c) {
        if (c == 'X')
            obstacles.emplace_back(indie::slot_t::WALL);
        else {
            if (dist(mt) % 4 == 0)
                obstacles.emplace_back(indie::slot_t::EMPTY);
            else
                obstacles.emplace_back(indie::slot_t::CRATE);
        }
    });
}

void indie::MapFactory::cleanCorners(indie::mapPlan_t &map)
{
    unsigned height = map.size();
    unsigned width = map[0].size();

    map[1][1] = EMPTY;
    map[1][2] = EMPTY;
    map[2][1] = EMPTY;
    map[1][width-2] = EMPTY;
    map[1][width-3] = EMPTY;
    map[2][width-2] = EMPTY;
    map[height-2][1] = EMPTY;
    map[height-2][2] = EMPTY;
    map[height-3][1] = EMPTY;
    map[height-2][width-2] = EMPTY;
    map[height-3][width-2] = EMPTY;
    map[height-2][width-3] = EMPTY;
}

#include <iostream>

void indie::MapFactory::checkMap(const indie::mapPlan_t &map)
{
    std::for_each(begin(map), end(map), [] (const std::vector<slot_t> &row) {
        std::for_each(begin(row), end(row), [] (const slot_t &o) {
            std::cout << o;
        });
        std::cout << std::endl;
    });
}

std::ostream &indie::operator<<(std::ostream &os, const indie::slot_t &s)
{
    switch (s) {
        case indie::slot_t::WALL: os << "#"; break;
        case indie::slot_t::EMPTY: os << " "; break;
        case indie::slot_t::CRATE: os << "+"; break;
    }
    return os;
}

std::shared_ptr<indie::Map> indie::MapFactory::createMapFromMapPlan(const indie::mapPlan_t &mapPlan)
{
    return std::make_shared<indie::Map>(mapPlan);
}