/*
** EPITECH PROJECT, 2020
** system
** File description:
** System
*/

#include <algorithm>
#include "System.hpp"

void System::addEntity(ENTITY entity)
{
    _entities.push_back(entity);
}

const std::vector<ENTITY> &System::getEntities() const
{
    return _entities;
}