/*
** EPITECH PROJECT, 2020
** entity
** File decription:
** Entity.cpp:
*/

#include "Entity.hpp"

indie::Entity::Entity() :
    _id(ID::generate()),
    _component()
{}

indie::Entity::Entity(const Entity &entity)
{
    _id = ID::generate();
    _component = entity._component;
}

indie::Entity &indie::Entity::operator=(const indie::Entity &entity)
{
    _id = ID::generate();
    _component = entity._component;
    return (*this);
}

indie::Entity::~Entity() {}

ID indie::Entity::getID() const
{
    return _id;
}

void indie::Entity::add(std::shared_ptr<Component> component) {
    _component.push_back(component);
}
