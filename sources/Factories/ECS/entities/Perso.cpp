/*
** EPITECH PROJECT, 2020
** Perso
** File description:
** Perso.cpp
*/

#include "Perso.hpp"

indie::Perso::Perso(float positionX, float positionY, float positionZ, const std::string &name) :
    _name(name),
    _pos(irr::core::vector3df(positionX, positionY, positionZ)),
    _is_running(false),
    _hp(3),
    _inputs(),
    _maxBombs(3),
    _bombs(),
    _bonusRange(0),
    _bonusDmg(0)
{
    _entity = NEW <Entity>();
    creatStatistics();
}

indie::Perso::Perso(const Perso &perso)
{
    if (perso._entity)
        _entity = perso._entity;
    if (!perso._name.empty())
        _entity = perso._entity;
    _inputs[ActionComponent::UP] = perso._inputs.at(ActionComponent::UP);
    _inputs[ActionComponent::DOWN] = perso._inputs.at(ActionComponent::DOWN);
    _inputs[ActionComponent::LEFT] = perso._inputs.at(ActionComponent::LEFT);
    _inputs[ActionComponent::RIGHT] = perso._inputs.at(ActionComponent::RIGHT);
    _inputs[ActionComponent::BOMB] = perso._inputs.at(ActionComponent::BOMB);
}

indie::Perso &indie::Perso::operator=(const indie::Perso &perso)
{
    if (perso._entity)
        _entity = perso._entity;
    if (!perso._name.empty())
        _entity = perso._entity;
    _inputs[ActionComponent::UP] = perso._inputs.at(ActionComponent::UP);
    _inputs[ActionComponent::DOWN] = perso._inputs.at(ActionComponent::DOWN);
    _inputs[ActionComponent::LEFT] = perso._inputs.at(ActionComponent::LEFT);
    _inputs[ActionComponent::RIGHT] = perso._inputs.at(ActionComponent::RIGHT);
    _inputs[ActionComponent::BOMB] = perso._inputs.at(ActionComponent::BOMB);
    return (*this);
}

const std::string &indie::Perso::getName() const
{
    return _name;
}

std::ostream &operator<<(std::ostream &os, indie::Perso &perso)
{
    os << "Perso: " << perso.getName() << std::endl;
    if (perso.getEntity()->get<MeshComponent>()) {
        irr::core::vector3df pos = perso.getEntity()->get<MeshComponent>()->getNode()->getPosition();
        os << "X = " << pos.X << "Y = " << pos.Y << "Z = " << pos.Z << std::endl;
    }
    return os;
}

void indie::Perso::creatStatistics()
{
    _entity->add(NEW <ColliderComponent>(indie::Breakable));
    _entity->add(NEW <ActionComponent>());
    _entity->add(NEW <MeshComponent>("./files/models/sydney.md2", "./files/textures/sydney.bmp"));
}

irr::core::vector3df &indie::Perso::getPosition()
{
    _entity->get<MeshComponent>()->getNode()->setPosition(_pos);
    return _pos;
}

ENTITY &indie::Perso::getEntity()
{
    return _entity;
}

const int &indie::Perso::getLife() const noexcept
{ return _hp; }

void indie::Perso::animeRun(bool activate)
{
    if (activate && !_is_running) {
        _entity->get<MeshComponent>()->changeAnimation(irr::scene::EMD2_ANIMATION_TYPE::EMAT_RUN);
        _is_running = true;
    }
    if (!activate)
        _is_running = false;
}

void indie::Perso::animeStand(bool activate)
{
    if (_is_running) {
        _entity->get<MeshComponent>()->changeAnimation(irr::scene::EMD2_ANIMATION_TYPE::EMAT_STAND);
        _is_running = false;
    }
}

void indie::Perso::move(const ActionComponent::action &dir)
{
    _entity->get<ActionComponent>()->changeDirection(dir, _entity->get<MeshComponent>()->getNode());
    irr::core::vector3df pos = _entity->get<MeshComponent>()->getNode()->getPosition();
    switch (dir) {
    case ActionComponent::UP:
        pos.Z += 6;
        break;
    case ActionComponent::DOWN:
        pos.Z -= 6;
        break;
    case ActionComponent::LEFT:
        pos.X -= 6;
        break;
    case ActionComponent::RIGHT:
        pos.X += 6;
        break;
    default:
        break;
    }
    _pos = pos;
    _entity->get<MeshComponent>()->getNode()->setPosition(pos);
}

ActionComponent::action indie::Perso::actionInput(std::map<std::string, bool> inputs)
{
    if (inputs[_inputs[ActionComponent::UP]] ||
        inputs[_inputs[ActionComponent::DOWN]] ||
        inputs[_inputs[ActionComponent::LEFT]] ||
        inputs[_inputs[ActionComponent::RIGHT]])
        animeRun(true);
    else if (inputs[_inputs[ActionComponent::BOMB]])
        _entity->get<MeshComponent>()->changeAnimation(irr::scene::EMD2_ANIMATION_TYPE::EMAT_CROUCH_ATTACK);
    else
        animeStand(false);

    if (inputs[_inputs[ActionComponent::UP]]) {
        move(ActionComponent::UP);
        return (ActionComponent::UP);
    } else if (inputs[_inputs[ActionComponent::DOWN]]) {
        move(ActionComponent::DOWN);
        return (ActionComponent::DOWN);
    } else if (inputs[_inputs[ActionComponent::LEFT]]) {
        move(ActionComponent::LEFT);
        return (ActionComponent::LEFT);
    } else if (inputs[_inputs[ActionComponent::RIGHT]]) {
        move(ActionComponent::RIGHT);
        return (ActionComponent::RIGHT);
    } else if (inputs[_inputs[ActionComponent::BOMB]]) {
        putABomb();
        return (ActionComponent::BOMB);
    }
    return (ActionComponent::NOTHING);
}

void indie::Perso::assignInput(std::string up, std::string down, std::string left, std::string right, std::string bomb)
{
    _inputs[ActionComponent::UP] = up;
    _inputs[ActionComponent::DOWN] = down;
    _inputs[ActionComponent::LEFT] = left;
    _inputs[ActionComponent::RIGHT] = right;
    _inputs[ActionComponent::BOMB] = bomb;
}

bool indie::Perso::takeDamage()
{
    if (_hp <= 0) {
        _entity->get<MeshComponent>()->getNode()->setPosition(irr::core::vector3df(0, 0, 0));
        _entity->get<MeshComponent>()->getNode()->setVisible(false);
        _entity->get<MeshComponent>()->changeAnimation(irr::scene::EMD2_ANIMATION_TYPE::EMAT_CROUCH_DEATH);
        return true;
    } else {
        _entity->get<MeshComponent>()->changeAnimation(irr::scene::EMD2_ANIMATION_TYPE::EMAT_BOOM);
        std::cout << _name << " dit : \"OUAIL\"" << std::endl;
        _hp--;
    }
    return false;
}

bool indie::Perso::isBombAlreadyPosed(float x, float z)
{
    for (const auto &b : _bombs) {
        auto pos = b->getPosition();
        if (pos->x == x && pos->y == z)
            return true;
    }
    return false;
}

void indie::Perso::putABomb()
{
    if (_bombs.size() < _maxBombs) {
        irr::core::vector3df pos = _entity->get<MeshComponent>()->getNode()->getPosition();
        float x = pos.X / 100;
        float z = pos.Z / -100;
        if (x != (pos.X + 25) / 100)
            x = (pos.X + 25) / 100;
        if (z != (pos.Z - 25) / -100)
            z = (pos.Z - 25) / -100;
        if (isBombAlreadyPosed(x, z) == true)
            return;
        _bombs.push_back(std::make_unique<Bomb>(x, z, _bonusDmg, _bonusRange));
        _bombs[_bombs.size()-1]->clock();
    }
}

const int indie::Perso::getMaxBombs() const
{
    return _maxBombs;
}

std::vector <std::unique_ptr<indie::Bomb>> &indie::Perso::getBombs()
{
    return _bombs;
}

void indie::Perso::destroyBombs(const ID &bombId)
{
    for (const auto &n : _bombs) {
        if (n->getEntity()->getID() == bombId) {
            _bombs.erase(std::find(_bombs.begin(), _bombs.end(), n));
        }
    }
}
