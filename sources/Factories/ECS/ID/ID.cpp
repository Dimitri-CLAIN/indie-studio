/*
** EPITECH PROJECT, 2020
** ID
** File decription:
** ID.cpp:
*/

#include "ID.hpp"

ID::ID() {
    static unsigned int nbStace = 0;

    _id = nbStace;
    nbStace++;
}

ID::~ID() {

}

bool ID::operator==(ID const &id) const {
    return _id == id.getID();
}

unsigned int ID::getID() const{
    return _id;
}

ID ID::generate() {
    return ID();
}
