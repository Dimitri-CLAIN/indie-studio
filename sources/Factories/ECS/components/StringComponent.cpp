/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** StringComponent.cpp:
*/

#include "StringComponent.hpp"

indie::StringComponent::StringComponent(const std::string &Tstring)
    : string(Tstring)
{}

indie::StringComponent::~StringComponent(){}

void indie::StringComponent::update(const std::string &Tstring) {
    string = Tstring;
}
