/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** indie::TextureComponent.cpp:
*/

#include "TextureComponent.hpp"

indie::TextureComponent::TextureComponent(const std::string &path) :
    _path(path),
    _isLoad(false)
{}

indie::TextureComponent::~TextureComponent()
{}

const std::string &indie::TextureComponent::getPath() const
{
    return _path;
}

bool indie::TextureComponent::isLoad()
{
    return _isLoad;
}

void indie::TextureComponent::setIsLoad(bool val)
{
    _isLoad = val;
}