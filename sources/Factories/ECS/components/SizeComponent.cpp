/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** SizeComponent.cpp:
*/

#include "SizeComponent.hpp"

indie::SizeComponent::SizeComponent(float TsizeY, float TsizeX)
    : sizeY(TsizeY), sizeX(TsizeX)
{}

indie::SizeComponent::~SizeComponent() {

}

void indie::SizeComponent::incY(float val)
{
    sizeY += val;
}

void indie::SizeComponent::incX(float val)
{
    sizeX += val;
}

void indie::SizeComponent::setY(float val)
{
    sizeY = val;
}

void indie::SizeComponent::setX(float val)
{
    sizeX = val;
}
