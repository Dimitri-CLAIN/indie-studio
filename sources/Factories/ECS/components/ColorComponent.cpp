/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** ComponentColor.cpp:
*/

#include "ColorComponent.hpp"

indie::ColorComponent::ColorComponent(unsigned char Tred, unsigned char Tgreen, unsigned char Tblue)
: Component(), red(Tred), green(Tgreen), blue(Tblue) {
}

indie::ColorComponent::~ColorComponent() {}

void indie::ColorComponent::update(unsigned char Tred, unsigned char Tgreen, unsigned char Tblue) {
    red = Tred;
    green = Tgreen;
    blue = Tblue;
}
