/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** MeshComponent.cpp:
*/

#include "MeshComponent.hpp"

MeshComponent::MeshComponent(const std::string &modelPath, const std::string &texturePath) :
    _modelPath(modelPath),
    _texturePath(texturePath),
    _isLoad(false)
{}

MeshComponent::MeshComponent(const std::shared_ptr<MeshComponent> &mesh)
{
    _modelPath = mesh->getModelPath();
    _texturePath = mesh->getTexturePath();
    _isLoad = mesh->getIsLoad();
    _node = mesh->getNode(); // TODO check si fait une copy ou non
}

MeshComponent::~MeshComponent()
{}

void MeshComponent::loadMesh(irr::scene::ISceneManager *smgr, irr::video::IVideoDriver *driver)
{
    _mesh = smgr->getMesh(_modelPath.c_str());
    _isLoad = true;
    _node = smgr->addAnimatedMeshSceneNode(_mesh);
    if (_node) {
        _node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        _node->setMD2Animation(irr::scene::EMAT_STAND);
        _node->setMaterialTexture(0, driver->getTexture(_texturePath.c_str()));
        _node->setScale(irr::core::vector3df(2.f, 2.f, 2.f));
    }
	if (!_mesh) {
        _isLoad = false;
        throw indie::MeshError("loadMesh", "Error when mesh loading");
    }
}

void MeshComponent::changeAnimation(enum irr::scene::EMD2_ANIMATION_TYPE animation)
{
    _node->setMD2Animation(animation);
}

irr::scene::IAnimatedMesh* MeshComponent::getMesh() const
{
    return _mesh;
}

const bool &MeshComponent::getIsLoad() const
{
    return _isLoad;
}

const std::string &MeshComponent::getModelPath() const
{
    return _modelPath;
}

const std::string &MeshComponent::getTexturePath() const
{
    return _texturePath;
}

irr::scene::IAnimatedMeshSceneNode *MeshComponent::getNode() const
{
    return _node;
}