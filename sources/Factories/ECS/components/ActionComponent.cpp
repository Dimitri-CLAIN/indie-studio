/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** ActionComponent.cpp:
*/

#include "ActionComponent.hpp"

ActionComponent::ActionComponent(const irr::core::vector3df &rotation) :
    Component(),
    _rotation(rotation)
{}

ActionComponent::~ActionComponent() {}

void ActionComponent::changeDirection(const ActionComponent::action &dir, irr::scene::IAnimatedMeshSceneNode *node)
{
    irr::core::vector3df pos = node->getPosition();
    irr::core::vector3df facing;

    switch (dir) {
    case LEFT:
        _rotation.Y = 190;
        break;
    case RIGHT:
        _rotation.Y = 0;
        break;
    case UP:
        _rotation.Y = -90;
        break;
    case DOWN:
        _rotation.Y = 90;
        break;
    default:
        break;
    }
    node->setRotation(_rotation);
}
