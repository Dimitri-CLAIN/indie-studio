/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** PositionComponent.cpp:
*/

#include "PositionComponent.hpp"

indie::PositionComponent::PositionComponent(float Tx, float Ty)
    : Component(), x(Tx), y(Ty)
{}

indie::PositionComponent::~PositionComponent() {}
