/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** CubeComponent.cpp:
*/

#include "CubeComponent.hpp"

CubeComponent::CubeComponent() :
    _isLoad(false)
{}

CubeComponent::~CubeComponent()
{}

void CubeComponent::loadCube(irr::scene::ISceneManager *smgr, unsigned x, unsigned z)
{
    float gridSize = 100.f;

    _node = smgr->addCubeSceneNode(gridSize,
        0, -1,
        irr::core::vector3df(x * gridSize, 0, -gridSize * z),
        irr::core::vector3df(0, 0, 0),
        irr::core::vector3df(0.8f, 0.8f, 0.8f)
    );
    _isLoad = true;
}

const bool &CubeComponent::getIsLoad() const
{
    return _isLoad;
}

irr::scene::IMeshSceneNode *CubeComponent::getNode() const
{
    return _node;
}