/*
** EPITECH PROJECT, 2020
** Collision
** File description:
** Collision
*/

#include "Collision.hpp"

indie::Collision::Collision()
{}

indie::Collision::~Collision()
{}

void indie::Collision::setCollision(irr::scene::IAnimatedMeshSceneNode *perso, irr::scene::IMeshSceneNode *obj, irr::scene::ISceneManager *scene)
{
    irr::core::aabbox3d<irr::f32> hitbox = obj->getBoundingBox();
    irr::core::vector3df rad = hitbox.MaxEdge - hitbox.getCenter();
    irr::scene::ITriangleSelector *selector = scene->createOctreeTriangleSelector(obj->getMesh(), obj);
    irr::scene::ISceneNodeAnimator *animator = scene->createCollisionResponseAnimator(selector, perso, rad, irr::core::vector3df(0, 0, 0), irr::core::vector3df(0, 0, 0));

    obj->setTriangleSelector(selector);
    selector->drop();
    perso->addAnimator(animator);
    animator->drop();
}