/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** Button.cpp:
*/

#include "Button.hpp"

indie::Button::Button(IScene::typeScene scene, const std::string &tex, float sizeX, float sizeY, float posX, float posY) :
    _scene(scene),
    _basicTexture(tex),
    _state(OFF),
    _sizeX(sizeX),
    _sizeY(sizeY)
{
    _sprite = NEW <Entity>();

    _sprite->add(NEW <PositionComponent>(posX, posY));
    _sprite->add(NEW <SizeComponent>(sizeY, sizeX));
    _sprite->add(NEW <TextureComponent>(tex));
}

indie::Button::~Button()
{}

void indie::Button::changeState(status state)
{
    if (state == ON && _state == OFF) {
        //_sprite->get<SizeComponent>()->incX(20);
        _sprite->get<SizeComponent>()->incY(30);
        _state = ON;
    }
}

void indie::Button::refresh()
{
    _sprite->get<SizeComponent>()->setX(_sizeX);
    _sprite->get<SizeComponent>()->setY(_sizeY);
    _state = OFF;
}

ENTITY &indie::Button::getEntity()
{
    return _sprite;
}

indie::IScene::typeScene indie::Button::getScene()
{
    return _scene;
}