/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** handleInput
*/

#include "handleInput.hpp"

bool handleInput::OnEvent(const irr::SEvent& event)
{
    if (event.EventType == irr::EET_KEY_INPUT_EVENT) {
        switch (event.KeyInput.Key)
        {
        case irr::KEY_KEY_Z:
            this->inputs["z"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_Q:
            this->inputs["q"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_S:
            this->inputs["s"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_D:
            this->inputs["d"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_A:
            this->inputs["a"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_T:
            this->inputs["t"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_G:
            this->inputs["g"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_F:
            this->inputs["f"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_H:
            this->inputs["h"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_R:
            this->inputs["r"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_SPACE:
            this->inputs[" "] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_RETURN:
            this->inputs["\n"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_ESCAPE:
            this->inputs["esc"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_UP:
            this->inputs["UP"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_DOWN:
            this->inputs["DOWN"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_LEFT:
            this->inputs["LEFT"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_RIGHT:
            this->inputs["RIGHT"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_O:
            this->inputs["o"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_L:
            this->inputs["l"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_K:
            this->inputs["k"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_M:
            this->inputs["m"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_I:
            this->inputs["i"] = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_P:
            this->inputs["p"] = event.KeyInput.PressedDown;
        default:
            break;
        }
    }
    return (false);
}

void handleInput::ResetList(void)
{
    this->inputs["z"] = false;
    this->inputs["q"] = false;
    this->inputs["s"] = false;
    this->inputs["d"] = false;
    this->inputs["a"] = false;
    this->inputs["t"] = false;
    this->inputs["g"] = false;
    this->inputs["f"] = false;
    this->inputs["h"] = false;
    this->inputs["r"] = false;
    this->inputs["i"] = false;
    this->inputs["m"] = false;
    this->inputs["k"] = false;
    this->inputs["o"] = false;
    this->inputs["l"] = false;
    this->inputs[" "] = false;
    this->inputs["\n"] = false;
    this->inputs["esc"] = false;
    this->inputs["UP"] = false;
    this->inputs["DOWN"] = false;
    this->inputs["LEFT"] = false;
    this->inputs["RIGHT"] = false;
    this->inputs["p"] = false;
}

const std::map<std::string, bool> &handleInput::GetList(void)
{
    return (this->inputs);
}

handleInput::handleInput()
{
    this->inputs["z"] = false;
    this->inputs["q"] = false;
    this->inputs["s"] = false;
    this->inputs["d"] = false;
    this->inputs["a"] = false;
    this->inputs["t"] = false;
    this->inputs["g"] = false;
    this->inputs["f"] = false;
    this->inputs["h"] = false;
    this->inputs["r"] = false;
    this->inputs[" "] = false;
    this->inputs["\n"] = false;
    this->inputs["esc"] = false;
    this->inputs["UP"] = false;
    this->inputs["DOWN"] = false;
    this->inputs["LEFT"] = false;
    this->inputs["RIGHT"] = false;
}