/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File decription:
** Graphical.cpp
*/

#include <algorithm>
#include <iostream>

#include "Graphical.hpp"
#include "TextureError.hpp"
#include "Wall.hpp"
#include "Crate.hpp"
#include "Collision.hpp"
#include "Particuls.hpp"

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

indie::Graphical::Graphical()
{
    _receiver = new handleInput();
}

indie::Graphical::~Graphical()
{
    if (_device->run())
        _device->closeDevice();
    _device->drop();
    delete(_receiver);
}

void indie::Graphical::initWindow()
{
    _device = irr::createDevice(irr::video::EDT_OPENGL, irr::core::dimension2d<irr::u32>(W_WIDHT, W_HEIGHT), 16, false, false, false, _receiver);
    if (_device == 0)
        throw indie::DeviceError("Render Creation", "could not create an Irrlicht Device.");
    _smgr = _device->getSceneManager();
    _driver = _device->getVideoDriver();
    _guienv = _device->getGUIEnvironment();
    _driver->setTextureCreationFlag(irr::video::ETCF_ALWAYS_32_BIT, true);
    _textures.emplace(GROUND_TEXTURE, _driver->getTexture(GROUND_TEXTURE));
    _textures.emplace(FIRE_TEXTURE, _driver->getTexture(FIRE_TEXTURE));
    _textures.emplace(TITLEBT_TEXTURE, _driver->getTexture(TITLEBT_TEXTURE));
    _textures.emplace(TITLEBACK_TEXTURE, _driver->getTexture(TITLEBACK_TEXTURE));
    _textures.emplace(MENUBACK_TEXTURE, _driver->getTexture(MENUBACK_TEXTURE));
    _textures.emplace(MENUPBT_TEXTURE, _driver->getTexture(MENUPBT_TEXTURE));
    _textures.emplace(MENUOBT_TEXTURE, _driver->getTexture(MENUOBT_TEXTURE));
    _textures.emplace(SOUNDPBT_TEXTURE, _driver->getTexture(SOUNDPBT_TEXTURE));
    _textures.emplace(MUSICPBT_TEXTURE, _driver->getTexture(MUSICPBT_TEXTURE));
    _textures.emplace(MENUQBT_TEXTURE, _driver->getTexture(MENUQBT_TEXTURE));
    _textures.emplace(GAMEBACK_TEXTURE, _driver->getTexture(GAMEBACK_TEXTURE));
    _textures.emplace(ONEPLAYER_TEXTURE, _driver->getTexture(ONEPLAYER_TEXTURE));
    _textures.emplace(TWOPLAYER_TEXTURE, _driver->getTexture(TWOPLAYER_TEXTURE));
    _textures.emplace(THREEPLAYER_TEXTURE, _driver->getTexture(THREEPLAYER_TEXTURE));
    _textures.emplace(FOURPLAYER_TEXTURE, _driver->getTexture(FOURPLAYER_TEXTURE));
    _textures.emplace(BACKBT_TEXTURE, _driver->getTexture(BACKBT_TEXTURE));
    _textures.emplace(MENURBT_TEXTURE, _driver->getTexture(MENURBT_TEXTURE));
    _textures.emplace(DRAW_TEXTURE, _driver->getTexture(DRAW_TEXTURE));
    _textures.emplace(PLAYERO_TEXTURE, _driver->getTexture(PLAYERO_TEXTURE));
    _textures.emplace(PLAYERT_TEXTURE, _driver->getTexture(PLAYERT_TEXTURE));
    _textures.emplace(PLAYERTH_TEXTURE, _driver->getTexture(PLAYERTH_TEXTURE));
    _textures.emplace(PLAYERF_TEXTURE, _driver->getTexture(PLAYERF_TEXTURE));
    if ((_textures.find(GROUND_TEXTURE) == _textures.end() || _textures[GROUND_TEXTURE] == nullptr) ||
        (_textures.find(TITLEBT_TEXTURE) == _textures.end() || _textures[TITLEBT_TEXTURE] == nullptr) ||
        (_textures.find(TITLEBACK_TEXTURE) == _textures.end() || _textures[TITLEBACK_TEXTURE] == nullptr) ||
        (_textures.find(MENUBACK_TEXTURE) == _textures.end() || _textures[MENUBACK_TEXTURE] == nullptr) ||
        (_textures.find(MENUPBT_TEXTURE) == _textures.end() || _textures[MENUPBT_TEXTURE] == nullptr) ||
        (_textures.find(MENUQBT_TEXTURE) == _textures.end() || _textures[MENUQBT_TEXTURE] == nullptr) ||
        (_textures.find(GAMEBACK_TEXTURE) == _textures.end() || _textures[GAMEBACK_TEXTURE] == nullptr) ||
        (_textures.find(ONEPLAYER_TEXTURE) == _textures.end() || _textures[ONEPLAYER_TEXTURE] == nullptr) ||
        (_textures.find(TWOPLAYER_TEXTURE) == _textures.end() || _textures[TWOPLAYER_TEXTURE] == nullptr) ||
        (_textures.find(THREEPLAYER_TEXTURE) == _textures.end() || _textures[THREEPLAYER_TEXTURE] == nullptr) ||
        (_textures.find(FOURPLAYER_TEXTURE) == _textures.end() || _textures[FOURPLAYER_TEXTURE] == nullptr) ||
        (_textures.find(BACKBT_TEXTURE) == _textures.end() || _textures[BACKBT_TEXTURE] == nullptr) ||
        (_textures.find(FIRE_TEXTURE) == _textures.end() || _textures[FIRE_TEXTURE] == nullptr)  ||
        (_textures.find(MENURBT_TEXTURE) == _textures.end() || _textures[MENURBT_TEXTURE] == nullptr)  ||
        (_textures.find(DRAW_TEXTURE) == _textures.end() || _textures[DRAW_TEXTURE] == nullptr) ||
        (_textures.find(PLAYERO_TEXTURE) == _textures.end() || _textures[PLAYERO_TEXTURE] == nullptr) ||
        (_textures.find(PLAYERT_TEXTURE) == _textures.end() || _textures[PLAYERT_TEXTURE] == nullptr) ||
        (_textures.find(PLAYERTH_TEXTURE) == _textures.end() || _textures[PLAYERTH_TEXTURE] == nullptr)  ||
        (_textures.find(PLAYERF_TEXTURE) == _textures.end() || _textures[PLAYERF_TEXTURE] == nullptr))
        throw indie::TextureError("Graphical Creation", "Textures could not be created.");
    createCamera();
}

irr::IrrlichtDevice *indie::Graphical::getDevice(void)
{
    return (_device);
}

irr::video::IVideoDriver *indie::Graphical::getDriver(void)
{
    return (_driver);
}

void indie::Graphical::closeWindow()
{
    _device->closeDevice();
}

const std::map<std::string, bool> &indie::Graphical::handleInputs()
{
    return _receiver->GetList();
}

void indie::Graphical::initMesh(ENTITY &entity)
{
    if (entity->get<MeshComponent>() && !entity->get<MeshComponent>()->getIsLoad() && entity->get<ActionComponent>())
        entity->get<MeshComponent>()->loadMesh(_smgr, _driver);
}

void indie::Graphical::manageEntities(std::vector<ENTITY> &entities)
{
    for (int i = 0; i < entities.size(); i++) {
        initMesh(entities[i]);
        EntityToSprite(entities[i]);
        EntityToBlock(entities[i], entities);
    }
}

void indie::Graphical::createCamera()
{
    irr::core::vector3df cPosition = {635, 1130, -875};
    irr::core::vector3df cTarget = {635, -405, -710};

    _smgr->addCameraSceneNode(0, cPosition, cTarget);
}

void indie::Graphical::drawParts(void)
{
    std::chrono::time_point<std::chrono::system_clock> after = std::chrono::system_clock::now();
    std::vector<Particuls> stand_by;
    irr::core::vector3df dir = {0, 0.1, 0};

    std::for_each(_parts.begin(), _parts.end(),
        [&](std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>> item) {
            std::for_each(item.begin(), item.end(),
                [&](std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>> item) {
                    Particuls part(_smgr, item.second, dir, _driver, _textures[FIRE_TEXTURE], item.first);
                    stand_by.push_back(part);
                });
            _parts.erase(_parts.begin());
            _particuls.push_back(stand_by);
            stand_by.clear();
    });
    std::for_each(_btimer.begin(), _btimer.end(),
        [&](std::chrono::time_point<std::chrono::system_clock> before) {
            if (std::chrono::duration_cast<std::chrono::seconds> (after - before).count() >= 1) {
                _btimer.erase(_btimer.begin());
                std::for_each(_particuls[0].begin(), _particuls[0].end(), [&](Particuls &tab) {
                    _smgr->addToDeletionQueue(tab.getParticuls());
                });
                _particuls.erase(_particuls.begin());
            }
    });
}

void indie::Graphical::displayScene(std::vector<ENTITY> &entities)
{
    manageEntities(entities);
    if (_device->run()) {
        if (_device->isWindowActive()) {
            _driver->beginScene(true, true, irr::video::SColor(255, 255, 255, 255));
            manageEntities(entities);
            drawParts();
            _smgr->drawAll();
            _guienv->drawAll();
            _driver->endScene();
        }
    }
}

void indie::Graphical::createGround(const unsigned int width,
                                    const unsigned int height)
{
    irr::scene::IAnimatedMesh *mesh = _smgr->addHillPlaneMesh("ground",
        irr::core::dimension2d<irr::f32>(width, height),
        irr::core::dimension2d<irr::u32>(100, 100),
        0, 0,
        irr::core::dimension2d<irr::f32>(0, 0),
        irr::core::dimension2d<irr::f32>(width, height)
    );
    irr::scene::IMeshSceneNode *node = _smgr->addMeshSceneNode(mesh);
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialTexture(0, _textures[GROUND_TEXTURE]);
    node->setPosition(irr::core::vector3df(15*50-50, -50, -15*50+50));
}

void indie::Graphical::EntityToBlock(ENTITY &entity, std::vector<ENTITY> &entities)
{
    if (entity->get<CubeComponent>() && !entity->get<CubeComponent>()->getIsLoad() && (entity->get<TextureComponent>()) && (entity->get<PositionComponent>()) && (entity->get<ColliderComponent>())) {
        auto tex = entity->get<TextureComponent>();
        auto pos = entity->get<PositionComponent>();
        addTexture(tex);
        entity->get<CubeComponent>()->loadCube(_smgr, pos->x, pos->y);
        if (entity->get<ColliderComponent>()->isCollider != indie::ColliderStatus::Nothing) {
            for (size_t i = 0; i < entities.size(); i++) {
                if (entities[i]->get<MeshComponent>() && entities[i]->get<ActionComponent>())
                    indie::Collision::setCollision(entities[i]->get<MeshComponent>()->getNode(), entity->get<CubeComponent>()->getNode(), _smgr);
            }
        }
        if (tex != nullptr) {
            entity->get<CubeComponent>()->getNode()->setMaterialFlag(irr::video::EMF_LIGHTING, false);
            entity->get<CubeComponent>()->getNode()->setMaterialTexture(0, _textures[tex->getPath()]);
        }
    }
}

void indie::Graphical::renderMap(const std::unique_ptr<indie::Map> &map, std::vector<ENTITY> &entities)
{
    std::vector<std::shared_ptr<indie::Block>> blocks = map->getBlocks();

    try {
        std::for_each(begin(blocks), end(blocks),
            [&](std::shared_ptr<indie::Block> block) {
                ENTITY entity = block->getEntity();
                EntityToBlock(entity, entities);
        });
    } catch (const indie::TextureError &e) {
        throw indie::TextureError(e.where(), e.what());
    }
}

void indie::Graphical::renderTerrain(const std::unique_ptr<indie::Map> &map, std::vector<ENTITY> &entities)
{
    createGround(map->getWidth(), map->getHeight());
    renderMap(map, entities);
}

void indie::Graphical::addTexture(const std::shared_ptr<indie::TextureComponent> &tex)
{
    if (!tex)
        throw indie::TextureError("addTexture", "A texture is missing.");
    const std::string &path = tex->getPath();

    if (_textures.find(path) != end(_textures))
        return;
    _textures.emplace(path, _driver->getTexture(path.c_str()));
    if (_textures[path] == nullptr)
        throw indie::TextureError("addTexture", "Cannot add texture '" + path + "'.");
}

const bool indie::Graphical::isWindowActive()
{
    return (_device->isWindowActive());
}

void indie::Graphical::EntityToSprite(ENTITY &entity)
{
    if (entity->get<indie::TextureComponent>() && entity->get<indie::PositionComponent>() && entity->get<indie::SizeComponent>()) {
        _driver->draw2DImage(_textures.find(entity->get<indie::TextureComponent>()->getPath())->second,
            irr::core::position2d<irr::s32>(entity->get<indie::PositionComponent>()->x, entity->get<indie::PositionComponent>()->y),
            irr::core::rect<irr::s32>(0, 0, entity->get<indie::SizeComponent>()->sizeX, entity->get<indie::SizeComponent>()->sizeY),
            0,
            irr::video::SColor (255,255,255,255),
            true);
    }
}

handleInput *indie::Graphical::getReceiver()
{
    return _receiver;
}

void indie::Graphical::setPlayerNb(int val)
{
    _playerNb = val;
}

void indie::Graphical::pushParts(std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>> nw)
{
    _parts.push_back(nw);
}

int &indie::Graphical::getPlayerNb(void)
{
    return (_playerNb);
}

void indie::Graphical::deleteComponent(irr::scene::IMeshSceneNode *node)
{
    _smgr->addToDeletionQueue(node);
}

void indie::Graphical::pushbtimer(std::chrono::time_point<std::chrono::system_clock> nw)
{
    _btimer.push_back(nw);
}

void indie::Graphical::clearsmgr(void)
{
    _smgr->clear();
    _particuls.clear();
    _parts.clear();
    _btimer.clear();
    _smgr = _device->getSceneManager();
    createCamera();
}

void indie::Graphical::setWinner(int val)
{
    _winner = val;
}
int indie::Graphical::getWinner(void)
{
    return (_winner);
}