/*
** EPITECH PROJECT, 2020
** Particuls
** File description:
** Particuls
*/

#include "Particuls.hpp"

Particuls::Particuls(irr::scene::ISceneManager *smgr, irr::core::aabbox3d<irr::f32> coord, const irr::core::vector3df &dir, irr::video::IVideoDriver *driver, irr::video::ITexture *texture, irr::core::vector3df pos)
{
    _particuleSystem = smgr->addParticleSystemSceneNode(true);

    irr::scene::IParticleEmitter* emitter = _particuleSystem->createBoxEmitter(
        coord,
        dir,
        10, 20,
        irr::video::SColor(0, 234, 61, 10),
        irr::video::SColor(0, 250, 250, 250),
        400, 800,
        20,
        irr::core::dimension2df(40.0f, 40.0f),
        irr::core::dimension2df(50.0f, 50.0f)
    );

    _particuleSystem->setEmitter(emitter);
    emitter->drop();
    irr::scene::IParticleAffector* affector = _particuleSystem->createFadeOutParticleAffector();
    _particuleSystem->addAffector(affector);
    affector->drop();
    _particuleSystem->setPosition(pos);
    _particuleSystem->setScale(irr::core::vector3df(2,2,2));
    _particuleSystem->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    _particuleSystem->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
    _particuleSystem->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    _particuleSystem->setMaterialTexture(0, texture);
}

Particuls::~Particuls()
{}

irr::scene::IParticleSystemSceneNode *Particuls::getParticuls()
{
    return _particuleSystem;
}
