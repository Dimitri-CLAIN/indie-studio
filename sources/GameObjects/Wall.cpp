/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Wall.cpp
*/

#include "Wall.hpp"

indie::Wall::Wall(float x, float y)
: Block(x, y, indie::Unbreakable, "./files/textures/wall.png")
{}