/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Crate.cpp
*/

#include "Crate.hpp"

indie::Crate::Crate(float x, float y)
: Block(x, y, indie::Breakable, "./files/textures/crate.png")
{}