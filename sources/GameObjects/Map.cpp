/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Map.cpp
*/

#include "Map.hpp"
#include "Block.hpp"
#include "Wall.hpp"
#include "Crate.hpp"

#include <iostream>

indie::Map::Map(const indie::mapPlan_t &mapPlan)
{
    _width = mapPlan[0].size();
    _height = mapPlan.size();
    _grid = mapPlan;
    for (float y = 0; y < mapPlan.size(); y+=1.0) {
        for (float x = 0; x < mapPlan[y].size(); x+=1.0) {
            switch (mapPlan[y][x]) {
                case EMPTY:
                    break;
                case CRATE:
                    _blocks.emplace_back(std::make_shared<Crate>(x, y));
                    break;
                case WALL:
                    _blocks.emplace_back(std::make_shared<Wall>(x, y));
                    break;
            }
        }
    }
}

const unsigned int indie::Map::getWidth() const noexcept
{
    return _width;
}

const unsigned int indie::Map::getHeight() const noexcept
{
    return _height;
}

const std::vector<std::shared_ptr<indie::Block>> &indie::Map::getBlocks() const noexcept
{
    return _blocks;
}

std::vector<std::shared_ptr<indie::Block>> &indie::Map::retrieveBlocks() noexcept
{
    return _blocks;
}

std::shared_ptr<indie::Block> indie::Map::getBlockByPos(float posX, float posZ)
{
    for (auto it = _blocks.begin(); it < _blocks.end(); it++) {
        if (it->get()->getEntity()->get<PositionComponent>()->x == posX &&
            it->get()->getEntity()->get<PositionComponent>()->y == posZ)
            return *it;
    }
    return (nullptr);
}

indie::mapPlan_t &indie::Map::getGrid()
{
    return _grid;
}

indie::Map::Map(const indie::Map &map)
{
    _width = map._width;
    _height = map._height;
    _grid = map._grid;
    for (const auto & n : map._blocks)
        _blocks.push_back(NEW <indie::Block> (*n));
}

indie::Map &indie::Map::operator=(const indie::Map &map)
{
    _width = map._width;
    _height = map._height;
    _grid = map._grid;
    for (const auto & n : map._blocks)
        _blocks.push_back(NEW <Block> (*n));
    return (*this);
}