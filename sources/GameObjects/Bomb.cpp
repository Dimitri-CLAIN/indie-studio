/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Bomb
*/

#include "Bomb.hpp"

indie::Bomb::Bomb(float x, float y, int dmg, int range)
: Block(x, y, indie::ColliderStatus::Nothing, "./files/textures/bomb.png"),
_gPos((int)x, (int)y), _pos_y(y), _pos_x(x)
{
    _before = std::chrono::system_clock::now();
    _dmg = 1 + dmg;
    _range = 1 + range;
    _exploded = false;
    _before = std::chrono::system_clock::now();
}

void indie::Bomb::clock()
{
    std::chrono::time_point<std::chrono::system_clock> after = std::chrono::system_clock::now();
    if (std::chrono::duration_cast<std::chrono::seconds> (after - _before).count() >= 3)
        explode();
}

const indie::vector2<int> &indie::Bomb::getGridPos() const noexcept
{ return _gPos; }

void indie::Bomb::explode()
{
    getEntity()->get<PositionComponent>()->y = -100;
    _exploded = true;
}

std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>> indie::Bomb::getExplosion()
{
    float r = static_cast<int>(_range);

    irr::core::vector3df pos_c = {(_pos_x * 100), 0, ((_pos_y * 100) * -1)};
    irr::core::vector3df pos_u = {(_pos_x * 100), 0, (((_pos_y - 1) * 100) * -1)};
    irr::core::vector3df pos_d = {(_pos_x * 100), 0, (((_pos_y + 1) * 100) * -1)};
    irr::core::vector3df pos_l = {((_pos_x - 1) * 100), 0, ((_pos_y * 100) * -1)};
    irr::core::vector3df pos_r = {((_pos_x + 1) * 100), 0, ((_pos_y * 100) * -1)};
    irr::core::aabbox3d<irr::f32> mod_c = {-10, 10, 10, 10, 50, 20};
    irr::core::aabbox3d<irr::f32> mod_l = {(-10 + (-55 * (r - 1))), 10, -20, 10, 20, 20};
    irr::core::aabbox3d<irr::f32> mod_r = {(-10 + (55 * (r - 1))), 10, -20, 10, 20, 20};
    irr::core::aabbox3d<irr::f32> mod_u = {-10, 10, (55 * (r - 1)), 0, 20, 20};
    irr::core::aabbox3d<irr::f32> mod_d = {-10, 10, (-55 * (r - 1)), 0, 20, 20};
    std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>> center = {pos_c, mod_c};
    std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>> up = {pos_u, mod_u};
    std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>> down = {pos_d, mod_d};
    std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>> left = {pos_l, mod_l};
    std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>> rigth = {pos_r, mod_r};
    std::vector<std::pair<irr::core::vector3df, irr::core::aabbox3d<irr::f32>>> out;

    for (int i = 0; i != 10; i++) {
        out.push_back(center);
        out.push_back(up);
        out.push_back(down);
        out.push_back(left);
        out.push_back(rigth);
    }
    return (out);
}

bool indie::Bomb::amIExploded()
{
    return (_exploded);
}

std::chrono::time_point<std::chrono::system_clock> indie::Bomb::getbtimer()
{
    return (std::chrono::system_clock::now());
}