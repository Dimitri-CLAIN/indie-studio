/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Block.cpp
*/

#include <Factories/ECS/components/PositionComponent.hpp>
#include <Factories/ECS/components/ColliderComponent.hpp>
#include "Block.hpp"

indie::Block::Block(float x, float y, indie::ColliderStatus status, const std::string &texPath)
{
    std::cout << "Block crated (" << x << "; " << y << ")" << std::endl;
    _e = NEW <Entity>();
    _e->add(NEW <PositionComponent>(x, y));
    _e->add(NEW <ColliderComponent>(status));
    _e->add(NEW <TextureComponent>(texPath));
    _e->add(NEW <CubeComponent>());
}

std::shared_ptr<indie::PositionComponent> indie::Block::getPosition() noexcept
{
    return this->_e->get<PositionComponent>();
}

std::shared_ptr<indie::TextureComponent> indie::Block::getTexture() noexcept
{
    return this->_e->get<TextureComponent>();
}

ENTITY &indie::Block::getEntity()
{
    return _e;
}

indie::Block::Block(const indie::Block &block)
{
    _e = block._e;
}

indie::Block &indie::Block::operator=(const indie::Block &block)
{
    _e = block._e;
    return (*this);
}
