/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** IndieErrors.cpp
*/

#include "IndieError.hpp"

indie::IndieError::IndieError(const std::string &where, const std::string &what)
: _origin(where), _message(what.c_str())
{}

const char *indie::IndieError::what() const noexcept
{
    return _message;
}

const std::string &indie::IndieError::where() const noexcept
{
    return _origin;
}