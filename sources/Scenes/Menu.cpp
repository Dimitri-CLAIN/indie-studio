/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Menu.cpp
*/

#include "Menu.hpp"

indie::Menu::Menu(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox) : _entities(), _cursor(0),  _playerEvent(Nothing), _display(display), _jukebox(jukebox)
{
}

void indie::Menu::updateEntities()
{
    _entities.clear();
    this->createSprites(MENUBACK_TEXTURE, 0, 0, 600, 800);
    std::for_each(_buttons.begin(), _buttons.end(),
        [this](Button button) {
            _entities.push_back(button.getEntity());
    });
    for (int i = 0; i != _buttons.size(); i++) {
        if (i != _cursor)
            _buttons[i].refresh();
    }
}

void indie::Menu::createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY)
{
    ENTITY sprite = NEW <Entity>();

    sprite->add(NEW <PositionComponent>(posX, posY));
    sprite->add(NEW <SizeComponent>(sizeX, sizeY));
    sprite->add(NEW <TextureComponent>(texture));
    _entities.push_back(sprite);
}

indie::IScene::typeScene indie::Menu::inputAction(std::map<std::string, bool> inputs)
{
    if (inputs["p"])
        _jukebox.lock()->mute();
    if (inputs["\n"] == true) {
        _jukebox.lock()->playSound(JukeBox::Button);
        return _buttons[_cursor].getScene();
    } else if (inputs["UP"] == true && _cursor > 0)
        _cursor--;
    else if (inputs["DOWN"] == true && (_buttons.size() - 1 > _cursor))
        _cursor++;
    return indie::IScene::typeScene::Nothing;
}

indie::IScene::typeScene indie::Menu::launchLoop(void)
{
    indie::IScene::typeScene futurScene;
    std::shared_ptr<Graphical> display = _display.lock();

    _buttons[_cursor].changeState(indie::status::ON);
    updateEntities();
    display->displayScene(_entities);
    std::map<std::string, bool> inputs = display->handleInputs();

    if ((futurScene = inputAction(inputs)) != indie::IScene::typeScene::Nothing)
        return futurScene;
    if (inputs["esc"])
        return (IScene::Quit);
    return (IScene::Menu);
}

void indie::Menu::init()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();
    std::shared_ptr<Graphical> gr = _display.lock();
    Button play(indie::IScene::typeScene::SelectPlayer, MENUPBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 275);
    Button option(indie::IScene::typeScene::Options, MENUOBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 375);
    Button quit(indie::IScene::typeScene::Quit, MENUQBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 475);
    _buttons.push_back(play);
    _buttons.push_back(option);
    _buttons.push_back(quit);

    gr->getReceiver()->ResetList();
    if (jukebox->isMusic(IScene::Nothing) || jukebox->isMusic(IScene::Quit))
        jukebox->startMusic(IScene::Menu);
    else if (!jukebox->isMusic(IScene::Options) && !jukebox->isMusic(IScene::SelectSave) && !jukebox->isMusic(IScene::Menu) && !jukebox->isMusic(IScene::SelectPlayer) && !jukebox->isMusic(IScene::Title)) {
        std::cout << "WHY NOT" << std::endl;
        jukebox->endMusic();
        jukebox->changeMusic(IScene::Menu);
    }
}

void indie::Menu::dest()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    _entities.clear();
}
