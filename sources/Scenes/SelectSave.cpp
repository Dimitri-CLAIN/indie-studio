/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** SelectSave.cpp
*/

#include "SelectSave.hpp"

indie::SelectSave::SelectSave(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox) : _entities(), _playerEvent(Nothing), _display(display), _jukebox(jukebox)
{
}

void indie::SelectSave::updateEntities()
{
    std::shared_ptr<Graphical> display = _display.lock();
    int winner = display->getWinner();

    _entities.clear();
    this->createSprites(MENUBACK_TEXTURE, 0, 0, 600, 800);
    if (winner == 1)
        this->createSprites(PLAYERO_TEXTURE, ((W_WIDHT / 2) - (286 / 2)), 200, 68, 286);
    else if (winner == 2)
        this->createSprites(PLAYERT_TEXTURE, ((W_WIDHT / 2) - (286 / 2)), 200, 68, 286);
    else if (winner == 3)
        this->createSprites(PLAYERTH_TEXTURE, ((W_WIDHT / 2) - (286 / 2)), 200, 68, 286);
    else if (winner == 4)
        this->createSprites(PLAYERF_TEXTURE, ((W_WIDHT / 2) - (286 / 2)), 200, 68, 286);
    else if (winner == 5)
        this->createSprites(DRAW_TEXTURE, ((W_WIDHT / 2) - (286 / 2)), 200, 68, 286);
    std::for_each(_buttons.begin(), _buttons.end(),
        [this](Button button) {
            _entities.push_back(button.getEntity());
    });
    for (int i = 0; i != _buttons.size(); i++) {
        if (i != _cursor)
            _buttons[i].refresh();
    }
}

indie::IScene::typeScene indie::SelectSave::launchLoop(void)
{
    indie::IScene::typeScene futurScene;
    std::shared_ptr<Graphical> display = _display.lock();

    _buttons[_cursor].changeState(indie::status::ON);
    updateEntities();
    display->displayScene(_entities);
    std::map<std::string, bool> inputs = display->handleInputs();
    if ((futurScene = inputAction(inputs)) != indie::IScene::typeScene::Nothing)
        return futurScene;
    if (inputs["esc"])
        return (IScene::Quit);
    return (IScene::SelectSave);
}

void indie::SelectSave::init()
{
    std::shared_ptr<Graphical> display = _display.lock();
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();
    Button play(indie::IScene::typeScene::SelectPlayer, MENURBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 375);
    Button quit(indie::IScene::typeScene::Quit, MENUQBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 475);

    _cursor = 0;
    _buttons.push_back(play);
    _buttons.push_back(quit);
    display->manageEntities(_entities);
    if (jukebox->isMusic(IScene::Nothing) || jukebox->isMusic(IScene::Quit))
        jukebox->startMusic(IScene::SelectSave);
    else if (!jukebox->isMusic(IScene::Options) && !jukebox->isMusic(IScene::SelectSave) && !jukebox->isMusic(IScene::Menu) && !jukebox->isMusic(IScene::SelectPlayer) && !jukebox->isMusic(IScene::Title)) {
        jukebox->endMusic();
        jukebox->changeMusic(IScene::SelectSave);
    }
}

void indie::SelectSave::dest()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    _entities.clear();
}

indie::IScene::typeScene indie::SelectSave::inputAction(std::map<std::string, bool> inputs)
{
    if (inputs["p"])
        _jukebox.lock()->mute();
    if (inputs["\n"] == true)
        return _buttons[_cursor].getScene();
    else if (inputs["UP"] == true && _cursor > 0)
        _cursor--;
    else if (inputs["DOWN"] == true && (_buttons.size() - 1 > _cursor))
        _cursor++;
    return indie::IScene::typeScene::Nothing;
}

void indie::SelectSave::createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY)
{
    ENTITY sprite = NEW <Entity>();

    sprite->add(NEW <PositionComponent>(posX, posY));
    sprite->add(NEW <SizeComponent>(sizeX, sizeY));
    sprite->add(NEW <TextureComponent>(texture));
    _entities.push_back(sprite);
}