/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Options.cpp
*/

#include "Options.hpp"

indie::Options::Options(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox) : _entities(), _cursor(0), _playerEvent(Nothing), _display(display), _jukebox(jukebox)
{
}
void indie::Options::updateEntities()
{
    _entities.clear();
    this->createSprites(MENUBACK_TEXTURE, 0, 0, 600, 800);
    std::for_each(_buttons.begin(), _buttons.end(),
                  [this](Button button) {
                      _entities.push_back(button.getEntity());
                  });
    for (int i = 0; i != _buttons.size(); i++) {
        if (i != _cursor)
            _buttons[i].refresh();
    }
}

void indie::Options::createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY)
{
    ENTITY sprite = NEW <Entity>();

    sprite->add(NEW <PositionComponent>(posX, posY));
    sprite->add(NEW <SizeComponent>(sizeX, sizeY));
    sprite->add(NEW <TextureComponent>(texture));
    _entities.push_back(sprite);
}

indie::IScene::typeScene indie::Options::inputAction(std::map<std::string, bool> inputs)
{
    if (inputs["p"])
        _jukebox.lock()->mute();
    if (inputs["\n"]) {
        _jukebox.lock()->playSound(JukeBox::Button);
        return _buttons[_cursor].getScene();
    } else if (inputs["LEFT"]) {
        if (_cursor == 0) {
            _jukebox.lock()->changeMusicVolumeDown();
            _jukebox.lock()->playSound(JukeBox::Button);
        } else if (_cursor == 1) {
            _jukebox.lock()->changeSoundVolumeDown();
            _jukebox.lock()->playSound(JukeBox::Button);
        }
    } else if (inputs["RIGHT"]) {
        if (_cursor == 0) {
            _jukebox.lock()->changeMusicVolumeUp();
            _jukebox.lock()->playSound(JukeBox::Button);
        } else if (_cursor == 1) {
            _jukebox.lock()->changeSoundVolumeUp();
            _jukebox.lock()->playSound(JukeBox::Button);
        }
    } else if (inputs["UP"] && _cursor > 0)
        _cursor--;
    else if (inputs["DOWN"] && (_buttons.size() - 1 > _cursor))
        _cursor++;
    return indie::IScene::typeScene::Nothing;
}

indie::IScene::typeScene indie::Options::launchLoop(void)
{
    indie::IScene::typeScene futurScene;
    std::shared_ptr<Graphical> display = _display.lock();

    _buttons[_cursor].changeState(indie::status::ON);
    updateEntities();
    display->displayScene(_entities);
    std::map<std::string, bool> inputs = display->handleInputs();

    if ((futurScene = inputAction(inputs)) != indie::IScene::typeScene::Nothing)
        return futurScene;
    if (inputs["esc"])
        return (IScene::Quit);
    return (IScene::Options);
}

void indie::Options::init()
{
    std::shared_ptr<Graphical> gr = _display.lock();
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    Button option(indie::IScene::typeScene::Options, MUSICPBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 175);
    Button option2(indie::IScene::typeScene::Options, SOUNDPBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 275);
    Button menu(indie::IScene::typeScene::Menu, BACKBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 375);
    Button quit(indie::IScene::typeScene::Quit, MENUQBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 475);
    _buttons.push_back(option);
    _buttons.push_back(option2);
    _buttons.push_back(menu);
    _buttons.push_back(quit);

    gr->getReceiver()->ResetList();
    if (jukebox->isMusic(IScene::Nothing) || jukebox->isMusic(IScene::Quit))
        jukebox->startMusic(IScene::Options);
    else if (!jukebox->isMusic(IScene::Options) && !jukebox->isMusic(IScene::SelectSave) && !jukebox->isMusic(IScene::Menu) && !jukebox->isMusic(IScene::SelectPlayer) && !jukebox->isMusic(IScene::Title)) {
        std::cout << "WHY" << std::endl;
        jukebox->endMusic();
        jukebox->changeMusic(IScene::Options);
    }
}

void indie::Options::dest()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    _entities.clear();
}
