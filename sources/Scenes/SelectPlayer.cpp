/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** SelectPlayer.cpp
*/

#include "SelectPlayer.hpp"

indie::SelectPlayer::SelectPlayer(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox) : _entities(), _cursor(0) ,_playerEvent(Nothing), _display(display), _jukebox(jukebox)
{
}

indie::IScene::typeScene indie::SelectPlayer::launchLoop(void)
{
    indie::IScene::typeScene futurScene;
    std::shared_ptr<Graphical> display = _display.lock();

    _buttons[_cursor].changeState(indie::status::ON);
    updateEntities();
    display->displayScene(_entities);
    std::map<std::string, bool> inputs = display->handleInputs();
    if ((futurScene = inputAction(inputs)) != indie::IScene::typeScene::Nothing)
        return futurScene;
    if (inputs["esc"])
        return (IScene::Quit);
    return (IScene::SelectPlayer);
}

void indie::SelectPlayer::init()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();
    std::shared_ptr<Graphical> gr = _display.lock();
    Button twoP(indie::IScene::typeScene::GameScene, TWOPLAYER_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 200);
    Button threeP(indie::IScene::typeScene::GameScene, THREEPLAYER_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 280);
    Button fourP(indie::IScene::typeScene::GameScene, FOURPLAYER_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 360);
    Button quit(indie::IScene::typeScene::Menu, BACKBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 500);

    gr->getReceiver()->ResetList();
    _buttons.push_back(twoP);
    _buttons.push_back(threeP);
    _buttons.push_back(fourP);
    _buttons.push_back(quit);
    _cursor = 0;
    if (jukebox->isMusic(IScene::Nothing) || jukebox->isMusic(IScene::Quit))
        jukebox->startMusic(IScene::SelectPlayer);
    else if (!jukebox->isMusic(IScene::Options) && !jukebox->isMusic(IScene::SelectSave) && !jukebox->isMusic(IScene::Menu) && !jukebox->isMusic(IScene::SelectPlayer) && !jukebox->isMusic(IScene::Title)) {
        jukebox->endMusic();
        jukebox->changeMusic(IScene::SelectPlayer);
    }
}

void indie::SelectPlayer::dest()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    _entities.clear();
}

void indie::SelectPlayer::updateEntities()
{
    _entities.clear();
    this->createSprites(MENUBACK_TEXTURE, 0, 0, 600, 800);
    std::for_each(_buttons.begin(), _buttons.end(),
        [this](Button button) {
            _entities.push_back(button.getEntity());
    });
    for (int i = 0; i != _buttons.size(); i++) {
        if (i != _cursor)
            _buttons[i].refresh();
    }
}

void indie::SelectPlayer::createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY)
{
    ENTITY sprite = NEW <Entity>();

    sprite->add(NEW <PositionComponent>(posX, posY));
    sprite->add(NEW <SizeComponent>(sizeX, sizeY));
    sprite->add(NEW <TextureComponent>(texture));
    _entities.push_back(sprite);
}

void indie::SelectPlayer::resetGameScene()
{
    _cursor = 0;
}

indie::IScene::typeScene indie::SelectPlayer::inputAction(std::map<std::string, bool> inputs)
{
    std::shared_ptr<Graphical> gr = _display.lock();
    int test = 0;

    if (inputs["p"])
        _jukebox.lock()->mute();
    if (inputs["\n"] == true) {
        _jukebox.lock()->playSound(JukeBox::Button);
        return _buttons[_cursor].getScene();
    } else if (inputs["UP"] == true && _cursor > 0)
        _cursor--;
    else if (inputs["DOWN"] == true && (_buttons.size() - 1 > _cursor))
        _cursor++;
    if (_cursor != 3)
        gr->setPlayerNb(_cursor + 2);
    return indie::IScene::typeScene::Nothing;
}
