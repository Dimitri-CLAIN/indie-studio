/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** GameScene.cpp
*/

#include "GameScene.hpp"

indie::GameScene::GameScene(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox) :
    _entities(),
    _playerEvent(Nothing),
    _display(display),
    _map(),
    _players(),
    _jukebox(jukebox)
{}

void indie::GameScene::setPosition()
{
    std::shared_ptr<Graphical> display = _display.lock();

    for (auto &p : _players)
        p->getPosition();
}

void indie::GameScene::createPlayers()
{
    std::shared_ptr<Graphical> display = _display.lock();
    int playerNb = display->getPlayerNb();

    if (playerNb >= 1)
        _players.push_back(NEW <Perso>(100, 40, -100, "FirstPlayer"));
    if (playerNb >= 2)
        _players.push_back(NEW <Perso>(1280, 40, -1300, "SecondPlayer"));
    if (playerNb >= 3)
        _players.push_back(NEW <Perso>(100, 40, -1300, "ThirdPlayer"));
    if (playerNb >= 4)
        _players.push_back(NEW <Perso>(1280, 40, -100, "FourthPlayer"));
    for (int i = 0; i < playerNb; i++) {
        _entities.push_back(_players[i]->getEntity());
        _players[i]->assignInput(inputTables[i][0], inputTables[i][1], inputTables[i][2], inputTables[i][3], inputTables[i][4]);
    }
}

void indie::GameScene::init()
{
    std::shared_ptr<Graphical> display = _display.lock();
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();
    mapPlan_t mapPlan = MapFactory::createMapPlan();

    createPlayers();
    createSprites(GAMEBACK_TEXTURE, 0, 0, 600, 800);
    MapFactory::checkMap(mapPlan);
    _map = std::make_unique<indie::Map>(mapPlan);
    display->manageEntities(_entities);
    display->renderTerrain(_map, _entities);
    setPosition();
    if (jukebox->isMusic(IScene::Nothing) || jukebox->isMusic(IScene::Quit))
        jukebox->startMusic(IScene::GameScene);
    else if (!jukebox->isMusic(IScene::GameScene))
        jukebox->changeMusic(IScene::GameScene);
}

void indie::GameScene::dest()
{
    std::shared_ptr <JukeBox> jukebox = _jukebox.lock();

    _entities.clear();
    _players.clear();
    _map.reset();
}

bool indie::GameScene::checkEndOfTheGame()
{
    std::shared_ptr<Graphical> display = _display.lock();

    if (_players.size() == 1) {
        for (auto &p : _players) {
            if (p->getName() == "FirstPlayer" && p->getLife() >= 0)
                display->setWinner(1);
            if (p->getName() == "SecondPlayer" && p->getLife() >= 0)
                display->setWinner(2);
            if (p->getName() == "ThirdPlayer" && p->getLife() >= 0)
                display->setWinner(3);
            if (p->getName() == "FourthPlayer" && p->getLife() >= 0)
                display->setWinner(4);
        }
        return (true);
    }
    if (_players.size() == 0) {
        display->setWinner(5);
        return (true);
    }
    return (false);
}

indie::IScene::typeScene indie::GameScene::launchLoop()
{
    std::shared_ptr<Graphical> display = _display.lock();

    display->displayScene(_entities);
    std::map<std::string, bool> inputs = display->handleInputs();
    inputAction(inputs);
    manageBombs();

    for (unsigned i = 0; i < _players.size(); i++) {
        if (_players[i]->getLife() <= 0) {
            _players.erase(begin(_players) + i);
        }
    }

    if (inputs["esc"])
        return (IScene::Quit);
    if (checkEndOfTheGame() == true) {
        _entities.clear();
        return (IScene::SelectSave);
    }
    return (IScene::GameScene);
}

void indie::GameScene::destroyBlock(const vector2<int> &coords)
{
    auto &slot = _map->getGrid()[coords.y][coords.x];
    if (slot == CRATE) {
        auto &blocks = _map->retrieveBlocks();
        for (auto it = begin(blocks); it != end(blocks); it++) {
            auto pos = (*it)->getPosition();
            if (pos->x == coords.x && pos->y == coords.y) {
                (*it)->getEntity()->get<CubeComponent>()->getNode()->setPosition(irr::core::vector3df(0, -110, 0));
                blocks.erase(it);
                return;
            }
        }
    }
    if (slot == WALL)
        return;
    for (auto &p : _players) {
        auto &vb = p->getBombs();

        for (auto it = begin(vb); it != end(vb); it++) {
            if (coords == (*it)->getGridPos()) {
                if ((*it)->amIExploded() == false) {
                    (*it)->explode();
                }
            }
        }
    }
}

void indie::GameScene::destroyMap(const indie::vector2<int> &origin)
{
    std::shared_ptr<Graphical> display = _display.lock();

    for (auto it = begin(_players); it != end(_players); it++) {
        irr::core::vector3df pos = (*it)->getPosition();
        int persoX = static_cast<int>(pos.X / 100);
        int persoZ = static_cast<int>(pos.Z / -100);
        if (persoX != (pos.X + 10) / 100)
            persoX = (pos.X + 10) / 100;
        if (persoZ != (pos.Z - 10) / -100)
            persoZ = (pos.Z - 10) / -100;
        if ((persoX == origin.x - 1 && persoZ == origin.y) ||
            (persoX == origin.x + 1 && persoZ == origin.y) ||
            (persoX == origin.x && persoZ == origin.y-1) ||
            (persoX == origin.x && persoZ == origin.y+1) ||
            (persoX == origin.x && persoZ == origin.y)) {
            if ((*it)->takeDamage()) {
                _jukebox.lock()->playSound(JukeBox::Death);
            } else {
                _jukebox.lock()->playSound(JukeBox::Damage);
            }
        } else {
            _jukebox.lock()->playSound(JukeBox::Explosion);
        }
    }
    destroyBlock(vector2<int>(origin.x-1, origin.y));
    destroyBlock(vector2<int>(origin.x+1, origin.y));
    destroyBlock(vector2<int>(origin.x, origin.y-1));
    destroyBlock(vector2<int>(origin.x, origin.y+1));
}

void indie::GameScene::manageBombs()
{
    std::shared_ptr<Graphical> display = _display.lock();

    for (auto &p : _players) {
        auto &vBomb = p->getBombs();

        for (int it = vBomb.size()-1; it >= 0; it--) {
            vBomb[it]->clock();
            if (vBomb[it]->amIExploded()) {
                indie::vector2<int> gpos = vBomb[it]->getGridPos();
                display->pushbtimer(vBomb[it]->getbtimer());
                display->pushParts(vBomb[it]->getExplosion());
                destroyMap(gpos);
            }
        }
    }

    for (auto &p : _players) {
        auto &vBomb = p->getBombs();

        for (int i = vBomb.size()-1; i >= 0; i--) {
            if (vBomb[i]->amIExploded()) {
                removeEntityByID(vBomb[i]->getEntity()->getID(), begin(vBomb)+i);
                _entities.erase(std::find(begin(_entities), end(_entities), vBomb[i]->getEntity()));
                vBomb.erase(vBomb.begin() + i);
            }
        }
    }
}

void indie::GameScene::removeEntityByID(ID id, std::vector<std::unique_ptr<indie::Bomb>>::iterator n)
{
    for (int i = 0; i < _entities.size(); i++) {
        if (_entities[i]->getID() == id) {
            std::shared_ptr<Graphical> g = _display.lock();
            if (_entities[i]->get<CubeComponent>())
                g->deleteComponent(_entities[i]->get<CubeComponent>()->getNode());
        }
    }
}

void indie::GameScene::inputAction(std::map<std::string, bool> inputs)
{
    if (inputs["p"])
        _jukebox.lock()->mute();
    for (auto &p : _players) {
        if (p->actionInput(inputs) == ActionComponent::BOMB) {
            auto &vBomb = p->getBombs();
            _entities.push_back(vBomb[vBomb.size() - 1]->getEntity());
        }
    };
}

indie::GameScene::GameScene(const indie::GameScene &GameSceneCpy) : _entities(), _playerEvent(Nothing), _display(GameSceneCpy._display), _map(std::make_unique<Map> (*GameSceneCpy._map))
{
    std::shared_ptr<Graphical> display = _display.lock();
    int playerNb = display->getPlayerNb();

    _players = GameSceneCpy._players;
    for (int i = 0; i < playerNb; i++)
        _entities.push_back(_players[i]->getEntity());
}

indie::GameScene &indie::GameScene::operator=(const indie::GameScene &GameSceneCpy)
{
    std::shared_ptr<Graphical> display = _display.lock();
    int playerNb = display->getPlayerNb();

    _players = GameSceneCpy._players;
    _players.reserve(GameSceneCpy._players.size());
    for (const auto &e : GameSceneCpy._players)
        _players.push_back(e);

    for (int i = 0; i < playerNb; i++)
        _entities.push_back(_players[i]->getEntity());
    _playerEvent = Nothing;
    _display = GameSceneCpy._display;
    _map = std::make_unique<Map>(*_map);
    return (*this);
}

void indie::GameScene::createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY)
{
    ENTITY sprite = NEW <Entity>();

    sprite->add(NEW <PositionComponent>(posX, posY));
    sprite->add(NEW <SizeComponent>(sizeX, sizeY));
    sprite->add(NEW <TextureComponent>(texture));
    _entities.push_back(sprite);
}
