/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Title.cpp
*/

#include "Title.hpp"

indie::Title::Title(std::shared_ptr<Graphical> &display, std::shared_ptr<JukeBox> &jukebox) : _entities(), _playerEvent(Nothing), _display(display), _jukebox(jukebox), _cursor(0)
{
}

void indie::Title::updateEntities()
{
    _entities.clear();
    this->createSprites(TITLEBACK_TEXTURE, 0, 0, 600, 800);
    std::for_each(_buttons.begin(), _buttons.end(),
        [this](Button button) {
            _entities.push_back(button.getEntity());
    });
    for (int i = 0; i != _buttons.size(); i++) {
        if (i != _cursor)
            _buttons[i].refresh();
    }
}

indie::IScene::typeScene indie::Title::launchLoop(void)
{
    indie::IScene::typeScene futurScene;
    std::shared_ptr<Graphical> display = _display.lock();

    _buttons[_cursor].changeState(indie::status::ON);
    updateEntities();
    display->displayScene(_entities);
    std::map<std::string, bool> inputs = display->handleInputs();
    if ((futurScene = inputAction(inputs)) != indie::IScene::typeScene::Nothing)
        return futurScene;
    if (inputs["esc"])
        return (IScene::Quit);
    return (IScene::Title);
}

indie::IScene::typeScene indie::Title::inputAction(std::map<std::string, bool> inputs)
{
    if (inputs["p"])
        _jukebox.lock()->mute();
    if (inputs["\n"] == true) {
        _jukebox.lock()->playSound(JukeBox::Button);
        return _buttons[_cursor].getScene();
    } else if (inputs["UP"] == true && _cursor > 0)
        _cursor--;
    else if (inputs["DOWN"] == true && (_buttons.size() - 1 > _cursor))
        _cursor++;
    return indie::IScene::typeScene::Nothing;
}

void indie::Title::createSprites(std::string texture, float posX, float posY, float sizeX, float sizeY)
{
    ENTITY sprite = NEW <Entity>();

    sprite->add(NEW <PositionComponent>(posX, posY));
    sprite->add(NEW <SizeComponent>(sizeX, sizeY));
    sprite->add(NEW <TextureComponent>(texture));
    _entities.push_back(sprite);
}

void indie::Title::init()
{
    Button play(indie::IScene::typeScene::Menu, TITLEBT_TEXTURE, 286, 68, ((W_WIDHT / 2) - (286 / 2)), 475);
    _buttons.push_back(play);
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    if (jukebox->isMusic(IScene::Nothing) || jukebox->isMusic(IScene::Quit)) {
        jukebox->startMusic(IScene::Title);
    } else if (!jukebox->isMusic(IScene::Options) && !jukebox->isMusic(IScene::SelectSave) && !jukebox->isMusic(IScene::Menu) && !jukebox->isMusic(IScene::SelectPlayer) && !jukebox->isMusic(IScene::Title)) {
        jukebox->endMusic();
        jukebox->changeMusic(IScene::Title);
    }
}

void indie::Title::dest()
{
    std::shared_ptr<JukeBox> jukebox = _jukebox.lock();

    _entities.clear();
}

