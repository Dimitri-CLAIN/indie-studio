/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** MapGenerator.cpp
*/

#include "MapGenerator.hpp"

map_t indie::MapGenerator::create()
{
    std::fstream fs;
    map_t map;

    fs.open("./files/map1.txt", std::fstream::in);
    if (fs.is_open() == false)
        throw std::exception();
    while (fs.eof() == false) {
        std::string s;
        std::getline(fs, s);
        if (s.empty() == true)
            continue;
        std::vector<slot_t> o;
        std::for_each(begin(s), end(s), [&] (const char &c) {
            if (c == 'X')
                o.emplace_back(WALL);
            else {
                if (std::rand() % 4 == 0)
                    o.emplace_back(EMPTY);
                else
                    o.emplace_back(CRATE);
            }
        });
        map.emplace_back(o);
    }
    // Empty corners to make sure players can spawn and play
    unsigned width = map[1].size();
    unsigned height = map.size();
    map[1][1] = EMPTY;
    map[1][2] = EMPTY;
    map[2][1] = EMPTY;
    map[1][width-2] = EMPTY;
    map[1][width-3] = EMPTY;
    map[2][width-2] = EMPTY;
    map[height-2][1] = EMPTY;
    map[height-2][2] = EMPTY;
    map[height-3][1] = EMPTY;
    map[height-2][width-2] = EMPTY;
    map[height-3][width-2] = EMPTY;
    map[height-2][width-3] = EMPTY;

    fs.close();
    return map;
}

void indie::MapGenerator::displayMap(const map_t &map)
{
    std::for_each(begin(map), end(map), [] (const std::vector<slot_t> &row) {
        std::for_each(begin(row), end(row), [] (const slot_t &o) {
            std::cout << o;
        });
        std::cout << std::endl;
    });
}

std::ostream &operator<<(std::ostream &os, const slot_t &s)
{
    switch (s) {
        case WALL: os << "#"; break;
        case EMPTY: os << " "; break;
        case CRATE: os << "+"; break;
    }
    return os;
}
