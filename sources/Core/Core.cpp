/*
** EPITECH PROJECT, 2020
** OOP_indie_studio_2019
** File description:
** Core.cpp
*/

#include "Core.hpp"
#include "IScene.hpp"

indie::Core::Core() : _scenes(), _actualScene(IScene::Nothing), _display(NEW <Graphical>()), _jukebox(NEW <JukeBox>())
{
    _scenes.push_back(NEW <Title>(_display, _jukebox));
    _scenes.push_back(NEW <Menu>(_display, _jukebox));
    _scenes.push_back(NEW <Options>(_display, _jukebox));
    _scenes.push_back(NEW <SelectSave>(_display, _jukebox));
    _scenes.push_back(NEW <SelectPlayer>(_display, _jukebox));
    _scenes.push_back(NEW <GameScene>(_display, _jukebox));
}

bool indie::Core::clock()
{
    static std::chrono::time_point<std::chrono::system_clock> before = std::chrono::system_clock::now();
    std::chrono::time_point<std::chrono::system_clock> after = std::chrono::system_clock::now();

    if (std::chrono::duration_cast<std::chrono::milliseconds> (after - before).count() >= 30 && _actualScene == IScene::GameScene) {
        before = after;
        return (true);
    } else if (std::chrono::duration_cast<std::chrono::milliseconds> (after - before).count() >= 125) {
        before = after;
        return (true);
    }
    return (false);
}


void indie::Core::generalLoop(void)
{
    IScene::typeScene nextScene = IScene::Nothing;

    _actualScene = IScene::Title;
    _lastScene = IScene::Title;
    _display->initWindow();
    _scenes[_actualScene]->init();
    while (_actualScene != IScene::Quit && _display->getDevice()->run()) {
        if (clock()) {
            if (_actualScene == IScene::SelectPlayer) {
                _scenes.push_back(NEW <GameScene>(_display, _jukebox));
            }
            if (_actualScene == IScene::SelectSave && _lastScene == IScene::GameScene) {
                _display->clearsmgr();
                _scenes.erase(_scenes.end());
            }
            //if (_actualScene == IScene::SelectSave) {
            //     _display->clearsmgr();
            //     _scenes.erase(_scenes.end());
            //     _scenes.push_back((NEW <GameScene>(_display, _jukebox)));
            //     _actualScene = IScene::Menu;
            // }
            if (_actualScene != _lastScene) {
                _scenes[_lastScene]->dest();
                _scenes[_actualScene]->init();
            }
            nextScene = _scenes[_actualScene]->launchLoop();
            // if (nextScene == IScene::Menu && _lastScene == IScene::SelectSave &&
            //     _actualScene == IScene::SelectPlayer) {
            //     _actualScene = IScene::SelectSave;
            // } else {
            _lastScene = _actualScene;
            _actualScene = nextScene;
            //}
        }
    }
    if (!_jukebox->isMusic(IScene::Nothing) && !_jukebox->isMusic(IScene::Quit))
        _jukebox->endMusic();
    _scenes[_lastScene]->dest();
    if (_display->isWindowActive()) {
        _display->closeWindow();
    }
}
