/*
** map_gen
** File description:
** main.cpp
*/

#include <iostream>
#include "Render.hpp"
#include "MapFactory.hpp"
#include "IndieError.hpp"
#include "Perso.hpp"
#include "Graphical.hpp"
#include "Map.hpp"
#include "Core.hpp"

int main(void)
{
    indie::Core core;
    try {
        core.generalLoop();
    } catch (const indie::IndieError &e) {
        std::cerr << e.where() << " : " << e.what() << std::endl;
        return 84;
    } catch (...) {
        std::cerr << "Something wrong happened..." << std::endl;
    }
    return 0;
}
