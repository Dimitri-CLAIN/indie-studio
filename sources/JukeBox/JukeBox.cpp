/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** JukeBox.cpp
*/

#include "JukeBox.hpp"
#include <iostream>

indie::JukeBox::JukeBox() : _actualMusic(IScene::Nothing), _music(), _sound(), _musicVolume(50), _soundVolume(50)
{
    _music.setVolume(_musicVolume);
    _music.setLoop(true);
    _sound.setVolume(_soundVolume);
    _sound.setLoop(false);
}

indie::JukeBox::~JukeBox()
{
    if (_actualMusic != IScene::Quit) {
        endMusic();
    }
}

void indie::JukeBox::startMusic(indie::IScene::typeScene nextMusic)
{
    std::string file;
    if (nextMusic == IScene::Title || nextMusic == IScene::Menu || nextMusic == IScene::Options || nextMusic == IScene::SelectPlayer || nextMusic == IScene::SelectSave)
        file = "files/musics/MenusMusic.ogg";
    else if (nextMusic == IScene::GameScene)
        file = "files/musics/GameSceneMusic.ogg";
    else
        return;
    _music.openFromFile(file);
    _actualMusic = nextMusic;
    _music.play();
}

void indie::JukeBox::playSound(indie::JukeBox::typeSound typeOfSound)
{
    sf::SoundBuffer soundBuffer;

    if (typeOfSound == Damage) {
        _soundBuffer.loadFromFile("files/sounds/DamageSound.ogg");
        std::cout << "Damage" << std::endl;
    } else if (typeOfSound == Death) {
        _soundBuffer.loadFromFile("files/sounds/DeadSound.ogg");
        std::cout << "Death" << std::endl;
    } else if (typeOfSound == Explosion) {
        _soundBuffer.loadFromFile("files/sounds/ExplosionSound.ogg");
        std::cout << "Explosion" << std::endl;
    } else if (typeOfSound == Button) {
        _soundBuffer.loadFromFile("files/sounds/ButtonSound.ogg");
        std::cout << "Button" << std::endl;
    } else
        return;
    _sound.setBuffer(_soundBuffer);
    _sound.play();
}

void indie::JukeBox::endSound()
{
    _sound.stop();
}

void indie::JukeBox::changeMusic(IScene::typeScene nextMusic)
{
    endMusic();
    startMusic(nextMusic);
}

void indie::JukeBox::endMusic()
{
    if (_actualMusic != IScene::Quit || _actualMusic == IScene::Nothing)
        _music.stop();
    _actualMusic = IScene::Quit;
}

bool indie::JukeBox::isMusic(indie::IScene::typeScene lastMusic)
{
    return (lastMusic == _actualMusic);
}

void indie::JukeBox::mute()
{
    if (_music.getVolume() == 0)
        deMute();
    else {
        _music.setVolume(0);
        _sound.setVolume(0);
    }
}

void indie::JukeBox::changeMusicVolumeUp()
{
    if (_musicVolume < 100)
        _musicVolume += 5;
    _music.setVolume(_musicVolume);
}

void indie::JukeBox::changeMusicVolumeDown()
{
    if (_musicVolume > 0)
        _musicVolume -= 5;
    _music.setVolume(_musicVolume);
}

void indie::JukeBox::changeSoundVolumeUp()
{
    if (_soundVolume < 100)
        _soundVolume += 5;
    _sound.setVolume(_soundVolume);
}

void indie::JukeBox::changeSoundVolumeDown()
{
    if (_soundVolume >0)
        _soundVolume -= 5;
    _sound.setVolume(_soundVolume);
}

void indie::JukeBox::deMute()
{
    _music.setVolume(_musicVolume);
    _sound.setVolume(_soundVolume);
}