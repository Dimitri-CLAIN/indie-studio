/*
** EPITECH PROJECT, 2020
** map_gen
** File description:
** Render.cpp
*/

#include "Render.hpp"
#include "DeviceError.hpp"
#include "TextureError.hpp"

#include <iostream>
#include <algorithm>
#include <Factories/ECS/components/PositionComponent.hpp>
#include <GameObjects/Wall.hpp>
#include <GameObjects/Crate.hpp>

indie::Render::Render()
//try
{
    _device = irr::createDevice(irr::video::EDT_OPENGL);
    if (_device == 0)
        throw indie::DeviceError("Render Creation",
                                "could not create an Irrlicht Device.");
    _smgr = _device->getSceneManager();
    irr::video::IVideoDriver *driver = _device->getVideoDriver();

    driver->setTextureCreationFlag(irr::video::ETCF_ALWAYS_32_BIT, true);
    _device->getCursorControl()->setVisible(false); // set visible in menus

    _groundTileTex = driver->getTexture("./files/textures/groundTile.png");
    _wallTex = driver->getTexture("./files/textures/wall.png");
    _crateTex = driver->getTexture("./files/textures/crate.png");
    if (_groundTileTex == 0 || _wallTex == 0 || _crateTex == 0)
        throw TextureError("Render Creation", "Textures could not be loaded.");
    // TODO CHECK ERRORS
    createCamera();
}
//} catch (const indie::DeviceError &e) {
//    throw e;
//} catch (const indie::TextureError &e) {
//    throw e;
//}

indie::Render::~Render()
{
    _device->drop();
}

irr::IrrlichtDevice *indie::Render::getDevice() const noexcept
{
    return _device;
}

void indie::Render::createCamera()
{
    irr::core::vector3df cPosition = {635, 1130, -875};
    irr::core::vector3df cTarget = {635, -405, -710};
    _smgr->addCameraSceneNode(0, cPosition, cTarget);

    //movable
//    irr::scene::ICameraSceneNode *camera = smgr->addCameraSceneNodeFPS(0, 100.f, 1.2f);
//    camera->setPosition(irr::core::vector3df(635, 1130, -875));
//    camera->setTarget(irr::core::vector3df(635, -405, -810));
//    camera->setFarValue(21000.f);
}

void indie::Render::createGround(unsigned width, unsigned height)
{
    irr::scene::IAnimatedMesh *mesh = _smgr->addHillPlaneMesh("ground",
        irr::core::dimension2d<irr::f32>(width, height), // tile count
        irr::core::dimension2d<irr::u32>(100, 100), // tile size
        0, 0, // material, hill height
        irr::core::dimension2d<irr::f32>(0, 0),
        irr::core::dimension2d<irr::f32>(width, height)
    );
    irr::scene::ISceneNode *node = _smgr->addAnimatedMeshSceneNode(mesh);
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialTexture(0, _groundTileTex);
    node->setPosition(irr::core::vector3df(15*50-50, -50, -15*50+50));
}

void indie::Render::placeCube(irr::video::ITexture *tex, unsigned x, unsigned z)
{
    float gridSize = 100.f;

    irr::scene::ISceneNode *node = _smgr->addCubeSceneNode(gridSize, //size
0, -1, // parent node, id
irr::core::vector3df(x * gridSize, 0, -gridSize * z), // position
irr::core::vector3df(0, 0, 0), // rotation
irr::core::vector3df(1.f, 1.f, 1.f) // scale
    );
    if (tex != nullptr) {
        node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        node->setMaterialTexture(0, tex);
    }
}

void indie::Render::renderMap(const std::shared_ptr<Map> map)
{
    std::vector<std::shared_ptr<indie::Block>> blocks = map->getBlocks();
    std::for_each(begin(blocks), end(blocks), [&] (std::shared_ptr<indie::Block> block) {
        //temp
        std::shared_ptr<PositionComponent> pos = block->getPosition();
        if (static_cast<indie::Wall *>(block.get()))
            placeCube(_wallTex, pos->x, pos->y);
        else if (static_cast<indie::Crate *>(block.get()))
            placeCube(_crateTex, pos->x, -pos->y);
    });
}

void indie::Render::renderTerrain(const std::shared_ptr<Map> map)
{
    createGround(map->getWidth(), map->getHeight());
    renderMap(map);
}