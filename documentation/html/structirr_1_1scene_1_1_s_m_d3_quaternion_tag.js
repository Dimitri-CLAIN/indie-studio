var structirr_1_1scene_1_1_s_m_d3_quaternion_tag =
[
    [ "~SMD3QuaternionTag", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#a0ab86796066b86541d2dffbc04a4f79a", null ],
    [ "SMD3QuaternionTag", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#adb75318e6c21f985d84d879999286a13", null ],
    [ "SMD3QuaternionTag", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#a33148238abe559faaf881a496add6af7", null ],
    [ "SMD3QuaternionTag", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#ad90a0ca55378e00de7dd01ad1c797720", null ],
    [ "operator=", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#a87e569e5c217bcfcef35bc46fdabef1c", null ],
    [ "operator==", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#aa17617af2690e6e28f25a4287db98852", null ],
    [ "setto", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#a44c15a124ddd2cd26b360e5b683ff9cc", null ],
    [ "Name", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#a0a8f91e505cde43480a25c9a79d8c9d4", null ],
    [ "position", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#a9e1f323078961d54b0c3da00f5472445", null ],
    [ "rotation", "structirr_1_1scene_1_1_s_m_d3_quaternion_tag.html#ad9b8d9cc3b84c99b9bb372af1e3d80f1", null ]
];