var irr_allocator_8h =
[
    [ "irrAllocator", "classirr_1_1core_1_1irr_allocator.html", "classirr_1_1core_1_1irr_allocator" ],
    [ "irrAllocatorFast", "classirr_1_1core_1_1irr_allocator_fast.html", "classirr_1_1core_1_1irr_allocator_fast" ],
    [ "eAllocStrategy", "irr_allocator_8h.html#aa2e91971d5e6e84de235bfabe3c7adba", [
      [ "ALLOC_STRATEGY_SAFE", "irr_allocator_8h.html#aa2e91971d5e6e84de235bfabe3c7adbaabfc6be96075564e1e0d92477e657f746", null ],
      [ "ALLOC_STRATEGY_DOUBLE", "irr_allocator_8h.html#aa2e91971d5e6e84de235bfabe3c7adbaa1fa5aed132891361601560ed3e067fe8", null ],
      [ "ALLOC_STRATEGY_SQRT", "irr_allocator_8h.html#aa2e91971d5e6e84de235bfabe3c7adbaad8222ac85dd18e7c1380773bac278dae", null ]
    ] ]
];