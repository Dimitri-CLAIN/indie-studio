var classirr_1_1scene_1_1_i_billboard_scene_node =
[
    [ "IBillboardSceneNode", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a32225fb98ab8f9e472272ae9e83c3c88", null ],
    [ "getColor", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a0b2729cc4913b0890ae28cf0ef0ab949", null ],
    [ "getSize", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a466cfd24ccb0fb6c2216dbdc7228e3c0", null ],
    [ "getSize", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a79a636a0da637eaa9c061138f5ef3f68", null ],
    [ "setColor", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a82c1038a6dfcd255863baa96aaba4182", null ],
    [ "setColor", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a13efdfa73998706baf10cedcdb48d559", null ],
    [ "setSize", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a911415ac24440bd3ccfcde102583fd60", null ],
    [ "setSize", "classirr_1_1scene_1_1_i_billboard_scene_node.html#a9a5d47a00bb0160daab8fa53453a2ba4", null ]
];