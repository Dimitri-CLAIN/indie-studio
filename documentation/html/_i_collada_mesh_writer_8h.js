var _i_collada_mesh_writer_8h =
[
    [ "IColladaMeshWriterProperties", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties" ],
    [ "IColladaMeshWriterNames", "classirr_1_1scene_1_1_i_collada_mesh_writer_names.html", "classirr_1_1scene_1_1_i_collada_mesh_writer_names" ],
    [ "IColladaMeshWriter", "classirr_1_1scene_1_1_i_collada_mesh_writer.html", "classirr_1_1scene_1_1_i_collada_mesh_writer" ],
    [ "E_COLLADA_COLOR_SAMPLER", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8", [
      [ "ECCS_DIFFUSE", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8a316a6699bbd75d46aaa6c84b24bf3d1b", null ],
      [ "ECCS_AMBIENT", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8a307ed333fc9c8458252919d3e91cd049", null ],
      [ "ECCS_EMISSIVE", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8a8244896d6a79bb4a8f9fdff35a53f69e", null ],
      [ "ECCS_SPECULAR", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8a7783e491e21caa8574e7eb1b32e9eb31", null ],
      [ "ECCS_TRANSPARENT", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8a9106443e64c9141cb0bb7d003603b5f8", null ],
      [ "ECCS_REFLECTIVE", "_i_collada_mesh_writer_8h.html#a6204218341c6b449d879cd8367b2f8d8a5d9100bd3a502fe5662089fc013e5bd7", null ]
    ] ],
    [ "E_COLLADA_GEOMETRY_WRITING", "_i_collada_mesh_writer_8h.html#a179008e7c02889459edf81394dbd6959", [
      [ "ECGI_PER_MESH", "_i_collada_mesh_writer_8h.html#a179008e7c02889459edf81394dbd6959a48f7ed09f68367a679cc8f6d4fd63906", null ],
      [ "ECGI_PER_MESH_AND_MATERIAL", "_i_collada_mesh_writer_8h.html#a179008e7c02889459edf81394dbd6959af2b6f3248808f3dbf81a7e707e508f65", null ]
    ] ],
    [ "E_COLLADA_IRR_COLOR", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7e", [
      [ "ECIC_NONE", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7ea46a948a882be4d21af671d4dff939a12", null ],
      [ "ECIC_CUSTOM", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7eae1ff98b61069652418cc36e411de863c", null ],
      [ "ECIC_DIFFUSE", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7eac230a09c9fc579cc1776baea37731329", null ],
      [ "ECIC_AMBIENT", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7ea1508a71bf85cb6f1303df8ae240cde75", null ],
      [ "ECIC_EMISSIVE", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7ea212908c2b2af576861553e4b441ac7f3", null ],
      [ "ECIC_SPECULAR", "_i_collada_mesh_writer_8h.html#a61cba210038d6d843b81d9282f1cac7eae62f9539794fa526128ea1eed9840b7d", null ]
    ] ],
    [ "E_COLLADA_TECHNIQUE_FX", "_i_collada_mesh_writer_8h.html#a9ec31e84e05295892488296b0741e2b1", [
      [ "ECTF_BLINN", "_i_collada_mesh_writer_8h.html#a9ec31e84e05295892488296b0741e2b1a69a2ffe63b7956eaedd3862f25d94d51", null ],
      [ "ECTF_PHONG", "_i_collada_mesh_writer_8h.html#a9ec31e84e05295892488296b0741e2b1a48c8725416499c9909726e14f18c3fce", null ],
      [ "ECTF_LAMBERT", "_i_collada_mesh_writer_8h.html#a9ec31e84e05295892488296b0741e2b1aef79736ffe4a0d7653644c7082061726", null ],
      [ "ECTF_CONSTANT", "_i_collada_mesh_writer_8h.html#a9ec31e84e05295892488296b0741e2b1af21d6f4c2a62f9b3ae762c97808039aa", null ]
    ] ],
    [ "E_COLLADA_TRANSPARENT_FX", "_i_collada_mesh_writer_8h.html#af7dadd5b96b683cfe1800f343c4f6619", [
      [ "ECOF_A_ONE", "_i_collada_mesh_writer_8h.html#af7dadd5b96b683cfe1800f343c4f6619a93b8dbd819a9860581193a60ed6155c6", null ],
      [ "ECOF_RGB_ZERO", "_i_collada_mesh_writer_8h.html#af7dadd5b96b683cfe1800f343c4f6619aeeb0b34217f227d32be4876aeeaaf70f", null ]
    ] ]
];