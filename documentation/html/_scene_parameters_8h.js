var _scene_parameters_8h =
[
    [ "ALLOW_ZWRITE_ON_TRANSPARENT", "_scene_parameters_8h.html#ab585d23bc2a3d02cd368d8bfd0b1414a", null ],
    [ "B3D_LOADER_IGNORE_MIPMAP_FLAG", "_scene_parameters_8h.html#a0a190773ebdbed9f134b17d496fc526a", null ],
    [ "B3D_TEXTURE_PATH", "_scene_parameters_8h.html#acaad1f28c235751815637948cc845c15", null ],
    [ "COLLADA_CREATE_SCENE_INSTANCES", "_scene_parameters_8h.html#a157681b3ef101a801ce278e6f21de946", null ],
    [ "CSM_TEXTURE_PATH", "_scene_parameters_8h.html#aecf002b9e14bd101b455632e1c260a8d", null ],
    [ "DEBUG_NORMAL_COLOR", "_scene_parameters_8h.html#a767a12984dbf7a4f0917993d748d1350", null ],
    [ "DEBUG_NORMAL_LENGTH", "_scene_parameters_8h.html#a19153395855d08302b70dcfa9247eb51", null ],
    [ "DMF_ALPHA_CHANNEL_REF", "_scene_parameters_8h.html#afd6e025453e80983de80371dc56718ef", null ],
    [ "DMF_FLIP_ALPHA_TEXTURES", "_scene_parameters_8h.html#acdc1ddd0bfeb4118a5d8f3ea953717f6", null ],
    [ "DMF_IGNORE_MATERIALS_DIRS", "_scene_parameters_8h.html#ae996d826263cf504dd0260d0a096b0d0", null ],
    [ "DMF_TEXTURE_PATH", "_scene_parameters_8h.html#a2a6e8bd33eaec1815e3e16a59c269fb5", null ],
    [ "IRR_SCENE_MANAGER_IS_EDITOR", "_scene_parameters_8h.html#a7a3f4a75d85bd2b3f6bc4dd58f3ce585", null ],
    [ "LMTS_TEXTURE_PATH", "_scene_parameters_8h.html#a1d81a2ac8866dfa4a0ff3bdece327f75", null ],
    [ "MY3D_TEXTURE_PATH", "_scene_parameters_8h.html#a54eb9ea68ba13b4689444f8d34e338b9", null ],
    [ "OBJ_LOADER_IGNORE_GROUPS", "_scene_parameters_8h.html#afb0c389302c1e7ad39f83d558dbb2699", null ],
    [ "OBJ_LOADER_IGNORE_MATERIAL_FILES", "_scene_parameters_8h.html#ac7d5a31e2146062ddbde288115bb6c7b", null ],
    [ "OBJ_TEXTURE_PATH", "_scene_parameters_8h.html#a12d0b16f969fdaaf7c2161a0c7152b54", null ]
];