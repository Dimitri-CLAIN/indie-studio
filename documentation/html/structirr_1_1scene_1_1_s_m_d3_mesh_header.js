var structirr_1_1scene_1_1_s_m_d3_mesh_header =
[
    [ "meshID", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#a0d717379dc5276bc48bd8916ddb65cdf", null ],
    [ "meshName", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#ab1dff34f7841e8fe8a5454928622e84e", null ],
    [ "numFrames", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#a07b177f522c986dfe81ac99c63dd3d62", null ],
    [ "numShader", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#ac0cb990b6ce4c2696a9d0d0f63c770d2", null ],
    [ "numTriangles", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#a617ede5fc4ca36c234cdc751fcdc56e8", null ],
    [ "numVertices", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#a41b1639be12dab2aad95e4dc4dff34ca", null ],
    [ "offset_end", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#ab4ba8476c1007bc505b72a9e02e540d4", null ],
    [ "offset_shaders", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#ac2cae01881c76d24aa4f790e6030db14", null ],
    [ "offset_st", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#a6537b725617ffff8c196c68348d21c35", null ],
    [ "offset_triangles", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#aad42dc0b0d7d70823efe5d263825d5e3", null ],
    [ "vertexStart", "structirr_1_1scene_1_1_s_m_d3_mesh_header.html#ad0c2747bbf39b3f0f97eb3b070fc8e8e", null ]
];