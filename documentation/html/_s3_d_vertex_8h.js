var _s3_d_vertex_8h =
[
    [ "S3DVertex", "structirr_1_1video_1_1_s3_d_vertex.html", "structirr_1_1video_1_1_s3_d_vertex" ],
    [ "S3DVertex2TCoords", "structirr_1_1video_1_1_s3_d_vertex2_t_coords.html", "structirr_1_1video_1_1_s3_d_vertex2_t_coords" ],
    [ "S3DVertexTangents", "structirr_1_1video_1_1_s3_d_vertex_tangents.html", "structirr_1_1video_1_1_s3_d_vertex_tangents" ],
    [ "E_VERTEX_TYPE", "_s3_d_vertex_8h.html#a0e3b59e025e0d0db0ed2ee0ce904deac", [
      [ "EVT_STANDARD", "_s3_d_vertex_8h.html#a0e3b59e025e0d0db0ed2ee0ce904deaca98c8b791280bbf9252c4f4a37e91a416", null ],
      [ "EVT_2TCOORDS", "_s3_d_vertex_8h.html#a0e3b59e025e0d0db0ed2ee0ce904deaca7b5127a706ee33eb4385d702da007016", null ],
      [ "EVT_TANGENTS", "_s3_d_vertex_8h.html#a0e3b59e025e0d0db0ed2ee0ce904deaca8c50b5b2c88f08709d640fecc83dfb9b", null ]
    ] ],
    [ "getVertexPitchFromType", "_s3_d_vertex_8h.html#af7047c70ea264a502f3c0d7e7c16fbaa", null ],
    [ "sBuiltInVertexTypeNames", "_s3_d_vertex_8h.html#a0f4d6a80a8777457dc9fbe966588f6a6", null ]
];