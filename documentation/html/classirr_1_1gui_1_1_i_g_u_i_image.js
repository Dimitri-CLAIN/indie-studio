var classirr_1_1gui_1_1_i_g_u_i_image =
[
    [ "IGUIImage", "classirr_1_1gui_1_1_i_g_u_i_image.html#a079a30cd26c749a8a42b4a689678c4dc", null ],
    [ "getColor", "classirr_1_1gui_1_1_i_g_u_i_image.html#a46a6dc23dd045cb65247344bf71078e4", null ],
    [ "getImage", "classirr_1_1gui_1_1_i_g_u_i_image.html#a5ed7402ca9d9810d320ff11f1251e8c0", null ],
    [ "isAlphaChannelUsed", "classirr_1_1gui_1_1_i_g_u_i_image.html#a5be6faec4c156fc955c84a5b3f703d63", null ],
    [ "isImageScaled", "classirr_1_1gui_1_1_i_g_u_i_image.html#aca24045ee740242b9c8de73a8f609a27", null ],
    [ "setColor", "classirr_1_1gui_1_1_i_g_u_i_image.html#ac836018aafc61f6b9fe88acfa2267d8e", null ],
    [ "setImage", "classirr_1_1gui_1_1_i_g_u_i_image.html#a35a3af4957e42acb183f562d09a4ea63", null ],
    [ "setScaleImage", "classirr_1_1gui_1_1_i_g_u_i_image.html#a642c5683600be82efa0cc04b4236e34d", null ],
    [ "setUseAlphaChannel", "classirr_1_1gui_1_1_i_g_u_i_image.html#a9426b40769f4ef7614e6a94dcfa67455", null ]
];