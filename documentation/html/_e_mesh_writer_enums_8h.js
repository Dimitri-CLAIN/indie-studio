var _e_mesh_writer_enums_8h =
[
    [ "E_MESH_WRITER_FLAGS", "_e_mesh_writer_enums_8h.html#a9faae6cd9e415a0553cb4cdc190bbc1d", [
      [ "EMWF_NONE", "_e_mesh_writer_enums_8h.html#a9faae6cd9e415a0553cb4cdc190bbc1daf2dfebddfd0a2cd2b558e23cb6a87464", null ],
      [ "EMWF_WRITE_LIGHTMAPS", "_e_mesh_writer_enums_8h.html#a9faae6cd9e415a0553cb4cdc190bbc1daee23ec8ad339e67c03c424d5adb94a66", null ],
      [ "EMWF_WRITE_COMPRESSED", "_e_mesh_writer_enums_8h.html#a9faae6cd9e415a0553cb4cdc190bbc1dac7c70ee80dc1a33aac68d317cb9c2cb7", null ],
      [ "EMWF_WRITE_BINARY", "_e_mesh_writer_enums_8h.html#a9faae6cd9e415a0553cb4cdc190bbc1da06511a5df874b2b69a146e0bbcb70309", null ]
    ] ],
    [ "EMESH_WRITER_TYPE", "_e_mesh_writer_enums_8h.html#a431fa15741518ba15f6d5f2608b6cb4e", [
      [ "EMWT_IRR_MESH", "_e_mesh_writer_enums_8h.html#a431fa15741518ba15f6d5f2608b6cb4ea8f16d471fc5466a61e71c14158e7be3e", null ],
      [ "EMWT_COLLADA", "_e_mesh_writer_enums_8h.html#a431fa15741518ba15f6d5f2608b6cb4eab64eaf09eb12d2361e67066b86529ba8", null ],
      [ "EMWT_STL", "_e_mesh_writer_enums_8h.html#a431fa15741518ba15f6d5f2608b6cb4ea8608bd1e505d53e6cc2e7c477b31d8a1", null ],
      [ "EMWT_OBJ", "_e_mesh_writer_enums_8h.html#a431fa15741518ba15f6d5f2608b6cb4eae22b9ef8ea7befd1368d7b90bbe12992", null ],
      [ "EMWT_PLY", "_e_mesh_writer_enums_8h.html#a431fa15741518ba15f6d5f2608b6cb4ead00c87763ef520a5115e9920968c0108", null ]
    ] ]
];