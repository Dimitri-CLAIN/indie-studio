var classirr_1_1scene_1_1_i_collada_mesh_writer_properties =
[
    [ "~IColladaMeshWriterProperties", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#a98a4463140ed5695a07b718ec829300e", null ],
    [ "getColorMapping", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#ab347c50cc9b291625d051a919d8772ab", null ],
    [ "getCustomColor", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#a8028af2323dab63df4bdfeb292ec48cd", null ],
    [ "getIndexOfRefraction", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#ab7ec58f708ebebe941246e6c78b0691d", null ],
    [ "getMesh", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#ac6d9e1583642ac777471bd9225d72007", null ],
    [ "getReflectivity", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#ad880b5fc91114049b20347a31199b2a9", null ],
    [ "getTechniqueFx", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#abc0fb19092b53bdf48be8cc97a2af63b", null ],
    [ "getTextureIdx", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#a171287213537036be889a36ae4896c0e", null ],
    [ "getTransparency", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#ac547e1f89f4655751ecd570ad70d010b", null ],
    [ "getTransparentFx", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#a0d934ae86d3e587ae22f74d775bbfa36", null ],
    [ "isExportable", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#af24d1c12b3f4168407c078bd7fc3dc82", null ],
    [ "useNodeMaterial", "classirr_1_1scene_1_1_i_collada_mesh_writer_properties.html#a9c10df4dc3602efbba6a47b34e2f8f4b", null ]
];