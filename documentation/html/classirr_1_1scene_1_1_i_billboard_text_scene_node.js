var classirr_1_1scene_1_1_i_billboard_text_scene_node =
[
    [ "IBillboardTextSceneNode", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#a758a6a0ec2f76ee7623f19a55f1e7c4b", null ],
    [ "getColor", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#ac142a04e455811d5a3efa47ce2499d18", null ],
    [ "getSize", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#aead5178207d887357fb7f3fbddcc51d6", null ],
    [ "setColor", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#aaa65d10d3a49206728c47b148a64bb4a", null ],
    [ "setColor", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#ab3faa7c4238acd6bc3a2330cb5650da5", null ],
    [ "setSize", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#a506ca9b0ef160993fc44f4e0b5b97b63", null ],
    [ "setText", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#ab404347cd57f64bb559cca8bed8caa53", null ],
    [ "setTextColor", "classirr_1_1scene_1_1_i_billboard_text_scene_node.html#a05e1db5ef9af3ff0ab2750ba584583ef", null ]
];