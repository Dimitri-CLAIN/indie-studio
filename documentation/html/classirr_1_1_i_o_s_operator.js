var classirr_1_1_i_o_s_operator =
[
    [ "copyToClipboard", "classirr_1_1_i_o_s_operator.html#aba3b621ed3b64e5a421c42b06b49e0b6", null ],
    [ "getOperatingSystemVersion", "classirr_1_1_i_o_s_operator.html#acf89b7715468ad2af91807dea6d40bf8", null ],
    [ "getOperationSystemVersion", "classirr_1_1_i_o_s_operator.html#a8d634ee79439742b7397ca7ad7a3812a", null ],
    [ "getProcessorSpeedMHz", "classirr_1_1_i_o_s_operator.html#abc61b8016ec35125ef94adea42209ecd", null ],
    [ "getSystemMemory", "classirr_1_1_i_o_s_operator.html#af58b50cecf28f247e1f302ee0b9e8270", null ],
    [ "getTextFromClipboard", "classirr_1_1_i_o_s_operator.html#a9026ff9f28b48615971e18fcee6fff4c", null ]
];