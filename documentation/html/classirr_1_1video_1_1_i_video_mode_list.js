var classirr_1_1video_1_1_i_video_mode_list =
[
    [ "getDesktopDepth", "classirr_1_1video_1_1_i_video_mode_list.html#a15869ac6919f6f29d2a25fcf13b6dd45", null ],
    [ "getDesktopResolution", "classirr_1_1video_1_1_i_video_mode_list.html#ade99f99922307c1d539bdd35834361fa", null ],
    [ "getVideoModeCount", "classirr_1_1video_1_1_i_video_mode_list.html#a84326f1c6faa1cbbb6f90bdac66714dd", null ],
    [ "getVideoModeDepth", "classirr_1_1video_1_1_i_video_mode_list.html#ae3f4b101eacbebe78d6a2e1a3f2e713d", null ],
    [ "getVideoModeResolution", "classirr_1_1video_1_1_i_video_mode_list.html#aa06b5905ac9c04fb38a17bf798fccc9b", null ],
    [ "getVideoModeResolution", "classirr_1_1video_1_1_i_video_mode_list.html#af8409c756780c3566c94596cf7f94fc3", null ]
];