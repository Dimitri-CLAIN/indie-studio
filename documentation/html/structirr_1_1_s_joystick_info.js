var structirr_1_1_s_joystick_info =
[
    [ "POV_HAT_PRESENT", "structirr_1_1_s_joystick_info.html#a5ab1b9b6969289f70b321d3c3657d999af5061a1972f04f033d581db51f43f986", null ],
    [ "POV_HAT_ABSENT", "structirr_1_1_s_joystick_info.html#a5ab1b9b6969289f70b321d3c3657d999a89c1dae670b67183c5b4e675f9e58792", null ],
    [ "POV_HAT_UNKNOWN", "structirr_1_1_s_joystick_info.html#a5ab1b9b6969289f70b321d3c3657d999a5699138583766ab8eafac5c36340c0dc", null ],
    [ "Axes", "structirr_1_1_s_joystick_info.html#a0a09d1c3fc664207abaa610e5896b0c5", null ],
    [ "Buttons", "structirr_1_1_s_joystick_info.html#a31422460c315e69bc057367cb66e4d23", null ],
    [ "Joystick", "structirr_1_1_s_joystick_info.html#a691ed1bcdbf4ab3b30a4e9ed648c6d9d", null ],
    [ "Name", "structirr_1_1_s_joystick_info.html#a56d229ae1e1d9f18b252c2f6bf886815", null ],
    [ "PovHat", "structirr_1_1_s_joystick_info.html#a1b5da862d5259677cb343c7754a4943b", null ]
];