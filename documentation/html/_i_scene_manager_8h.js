var _i_scene_manager_8h =
[
    [ "ISceneManager", "classirr_1_1scene_1_1_i_scene_manager.html", "classirr_1_1scene_1_1_i_scene_manager" ],
    [ "E_SCENE_NODE_RENDER_PASS", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67f", [
      [ "ESNRP_NONE", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fa1f79a46e7a41716dcae5c8dfe8d310bb", null ],
      [ "ESNRP_CAMERA", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fa26f6af7911240e22003f327aef126053", null ],
      [ "ESNRP_LIGHT", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fa1390daf021e4354eb3dc8d5d46fb7dc0", null ],
      [ "ESNRP_SKY_BOX", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fac41f4cb4900e84b9e55462089d0e3cb8", null ],
      [ "ESNRP_AUTOMATIC", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fa5ceee6e4bc2fab42c663b32018e276e8", null ],
      [ "ESNRP_SOLID", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fad058b020ab42ad745cc03fe379148e1f", null ],
      [ "ESNRP_TRANSPARENT", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fad3a1300505d0ab06133e25256b893b2b", null ],
      [ "ESNRP_TRANSPARENT_EFFECT", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fa3d134d3f703e328ab5798e5ff4a5c186", null ],
      [ "ESNRP_SHADOW", "_i_scene_manager_8h.html#a7862269bd1abc123929d4dbb8200d67fadbc7353e3092974abaa4d063faa22421", null ]
    ] ]
];