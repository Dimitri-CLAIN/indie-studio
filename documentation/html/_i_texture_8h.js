var _i_texture_8h =
[
    [ "ITexture", "classirr_1_1video_1_1_i_texture.html", "classirr_1_1video_1_1_i_texture" ],
    [ "E_TEXTURE_CREATION_FLAG", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876f", [
      [ "ETCF_ALWAYS_16_BIT", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fa4fe1c1b0f4b44ef4b5da219ce66a0ae8", null ],
      [ "ETCF_ALWAYS_32_BIT", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fa20881e307a778c4a4fbb5327a60a93bb", null ],
      [ "ETCF_OPTIMIZED_FOR_QUALITY", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fadc5336eb55e0221ef0d7e53c1cf5a2b9", null ],
      [ "ETCF_OPTIMIZED_FOR_SPEED", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fa01693eaffcfeca7de6fcd7e826a909c3", null ],
      [ "ETCF_CREATE_MIP_MAPS", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fa288b302e9d4faaba80c7796c7bc1682c", null ],
      [ "ETCF_NO_ALPHA_CHANNEL", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fab9ce08dff03ebff9139b594610561609", null ],
      [ "ETCF_ALLOW_NON_POWER_2", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fa6a834e40d35e01205ab63f3ce6d71172", null ],
      [ "ETCF_FORCE_32_BIT_DO_NOT_USE", "_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876fa0081685b8fa033d771f9c6a5bded8253", null ]
    ] ],
    [ "E_TEXTURE_LOCK_MODE", "_i_texture_8h.html#a3916d259e8fe0d0d02e8ee0adc8af5bc", [
      [ "ETLM_READ_WRITE", "_i_texture_8h.html#a3916d259e8fe0d0d02e8ee0adc8af5bca41973750ecd380c9a17fa575262f5037", null ],
      [ "ETLM_READ_ONLY", "_i_texture_8h.html#a3916d259e8fe0d0d02e8ee0adc8af5bca2b06fa431c7bae8ea7f844f188beaf6b", null ],
      [ "ETLM_WRITE_ONLY", "_i_texture_8h.html#a3916d259e8fe0d0d02e8ee0adc8af5bca44730dab3af80ddd64d81c85df911ca2", null ]
    ] ]
];