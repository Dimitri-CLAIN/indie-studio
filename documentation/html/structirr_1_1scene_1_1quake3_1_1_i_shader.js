var structirr_1_1scene_1_1quake3_1_1_i_shader =
[
    [ "IShader", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#ac6f55608afaa395ac45e9963e3f72932", null ],
    [ "~IShader", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#ac2b09ca200765652543c5bdcc1a41da2", null ],
    [ "getGroup", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#aa547f1681b60ac94946548266d96f462", null ],
    [ "getGroupSize", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#ab77ef97a0df78d591d930070bab1b7ca", null ],
    [ "operator<", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#a3b0686a5e55a52535a9803079b0ff65e", null ],
    [ "operator=", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#af295696f6ee3b36cde6112e684041a4c", null ],
    [ "operator==", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#a194237479ff9eef9d24c7720caa4f9b1", null ],
    [ "ID", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#a5ab9a652ea9719bd94437876bf6c8bb4", null ],
    [ "name", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#a583a33b3a69ee2047c14d60dad147862", null ],
    [ "VarGroup", "structirr_1_1scene_1_1quake3_1_1_i_shader.html#a874bb594c148e7aed177b79ce5c85362", null ]
];