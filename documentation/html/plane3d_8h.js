var plane3d_8h =
[
    [ "plane3d", "classirr_1_1core_1_1plane3d.html", "classirr_1_1core_1_1plane3d" ],
    [ "plane3df", "plane3d_8h.html#ae7491b7985dcb74b840bfcd9c054b232", null ],
    [ "plane3di", "plane3d_8h.html#af977958e3f4578d8ef16178badb89f2d", null ],
    [ "EIntersectionRelation3D", "plane3d_8h.html#a8a9999eb0d151083f48afe5f7d17a96c", [
      [ "ISREL3D_FRONT", "plane3d_8h.html#a8a9999eb0d151083f48afe5f7d17a96ca30484977b2480b6d6621114512d0b703", null ],
      [ "ISREL3D_BACK", "plane3d_8h.html#a8a9999eb0d151083f48afe5f7d17a96cae3f42495b4c63c980c19a82a44b7746b", null ],
      [ "ISREL3D_PLANAR", "plane3d_8h.html#a8a9999eb0d151083f48afe5f7d17a96ca8a8605f10c124b4aca71783cc7735f0c", null ],
      [ "ISREL3D_SPANNING", "plane3d_8h.html#a8a9999eb0d151083f48afe5f7d17a96ca26444234d5d37ce75f55249fffe25a61", null ],
      [ "ISREL3D_CLIPPED", "plane3d_8h.html#a8a9999eb0d151083f48afe5f7d17a96ca4a33464e7b7175c8614c5a6c9f9a3751", null ]
    ] ]
];