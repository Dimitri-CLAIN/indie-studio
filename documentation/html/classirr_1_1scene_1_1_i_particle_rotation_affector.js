var classirr_1_1scene_1_1_i_particle_rotation_affector =
[
    [ "getPivotPoint", "classirr_1_1scene_1_1_i_particle_rotation_affector.html#ae920893af3df5c69bca9fe04d2ea51fc", null ],
    [ "getSpeed", "classirr_1_1scene_1_1_i_particle_rotation_affector.html#a49274165a72699096b84e99bbfc64470", null ],
    [ "getType", "classirr_1_1scene_1_1_i_particle_rotation_affector.html#a01055bd7d4720418bc2a3b16fce58571", null ],
    [ "setPivotPoint", "classirr_1_1scene_1_1_i_particle_rotation_affector.html#a631f5645a687b3683a3a0315e9844a0b", null ],
    [ "setSpeed", "classirr_1_1scene_1_1_i_particle_rotation_affector.html#ae612fdfb02132323af1004c714e2fe2b", null ]
];