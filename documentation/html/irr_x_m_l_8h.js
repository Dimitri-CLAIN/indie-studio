var irr_x_m_l_8h =
[
    [ "IFileReadCallBack", "classirr_1_1io_1_1_i_file_read_call_back.html", "classirr_1_1io_1_1_i_file_read_call_back" ],
    [ "IXMLBase", "classirr_1_1io_1_1_i_x_m_l_base.html", null ],
    [ "IIrrXMLReader", "classirr_1_1io_1_1_i_irr_x_m_l_reader.html", "classirr_1_1io_1_1_i_irr_x_m_l_reader" ],
    [ "xmlChar", "structirr_1_1io_1_1xml_char.html", "structirr_1_1io_1_1xml_char" ],
    [ "char16", "irr_x_m_l_8h.html#a9140fe380f1a4e2fb4e114463e2d2838", null ],
    [ "char32", "irr_x_m_l_8h.html#adfbb5748d02235670728f95ab89b69a4", null ],
    [ "IrrXMLReader", "irr_x_m_l_8h.html#a1628edbb9d5d53f18c82d2a92b0ad27e", null ],
    [ "IrrXMLReaderUTF16", "irr_x_m_l_8h.html#a5eb4094dfd0d509e0cd8a9d1dd30a5b9", null ],
    [ "IrrXMLReaderUTF32", "irr_x_m_l_8h.html#a70f411ff403636fb5c4e9becb090d5ec", null ],
    [ "ETEXT_FORMAT", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9d", [
      [ "ETF_ASCII", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9daa83b76584091bdef7401e2e7c27837bf", null ],
      [ "ETF_UTF8", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9dae9766ce8a50bc70e144a7b0cc96ba8db", null ],
      [ "ETF_UTF16_BE", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9da5c0685f21f600ba0acdd84bae458ee5c", null ],
      [ "ETF_UTF16_LE", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9da331a03a8d3459ad9a05d31dee38a8a73", null ],
      [ "ETF_UTF32_BE", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9daa550dbb5ed61125ff6a12a9a50e4e7b1", null ],
      [ "ETF_UTF32_LE", "irr_x_m_l_8h.html#ac7e51e5a6bd00451dec248f497b16a9da122cd8cec108b4e5a4040f1a9bcc6709", null ]
    ] ],
    [ "EXML_NODE", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33ed", [
      [ "EXN_NONE", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33edaa7f8e643a481d9c8b75a25499f40235c", null ],
      [ "EXN_ELEMENT", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33eda9df4f5baccc23a0ad1f6fa64d8de2fc0", null ],
      [ "EXN_ELEMENT_END", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33eda54ef1997279f08180634f4a897f771b8", null ],
      [ "EXN_TEXT", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33eda0edf973f8ca0f6097f69369539d432a4", null ],
      [ "EXN_COMMENT", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33edadc47ef6b25afabf76ff3acea8fea2680", null ],
      [ "EXN_CDATA", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33edaabb26cfe097fadf64c45db0f76523ac6", null ],
      [ "EXN_UNKNOWN", "irr_x_m_l_8h.html#a86a02676c9cbb822e04d60c81b4f33edaf80bf58165975de0e7a7c28882906865", null ]
    ] ],
    [ "createIrrXMLReader", "irr_x_m_l_8h.html#a581f4d4648398759c61266d63d7106b1", null ],
    [ "createIrrXMLReader", "irr_x_m_l_8h.html#a9c0ebca5a4addfcfd90f51b5131f7d56", null ],
    [ "createIrrXMLReader", "irr_x_m_l_8h.html#af853ea962be4432c2d9a50cc7d303fe5", null ],
    [ "createIrrXMLReaderUTF16", "irr_x_m_l_8h.html#a86473ef152c15b685af181a4c5461a5d", null ],
    [ "createIrrXMLReaderUTF16", "irr_x_m_l_8h.html#a7e7ecf8350b446da3a4080f3949bc0b1", null ],
    [ "createIrrXMLReaderUTF16", "irr_x_m_l_8h.html#a9248bcaf5d5f394d7926c28bfc479d6c", null ],
    [ "createIrrXMLReaderUTF32", "irr_x_m_l_8h.html#ae05bf7ee342431ea8c98fb98e75b974a", null ],
    [ "createIrrXMLReaderUTF32", "irr_x_m_l_8h.html#a4ea88dd2598272cff85357611a5e5938", null ],
    [ "createIrrXMLReaderUTF32", "irr_x_m_l_8h.html#a0f6ab4835641471a523c485c8d229eb2", null ]
];