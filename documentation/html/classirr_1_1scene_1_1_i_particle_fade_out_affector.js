var classirr_1_1scene_1_1_i_particle_fade_out_affector =
[
    [ "getFadeOutTime", "classirr_1_1scene_1_1_i_particle_fade_out_affector.html#a4c4195b7878fd93526506e1c592bdbec", null ],
    [ "getTargetColor", "classirr_1_1scene_1_1_i_particle_fade_out_affector.html#a85301f6c37546e881ffa354a67d673ed", null ],
    [ "getType", "classirr_1_1scene_1_1_i_particle_fade_out_affector.html#aaf584388aa7065bbfdb571253ba34b61", null ],
    [ "setFadeOutTime", "classirr_1_1scene_1_1_i_particle_fade_out_affector.html#a838758aef6c351a26c4d3190dc1b7645", null ],
    [ "setTargetColor", "classirr_1_1scene_1_1_i_particle_fade_out_affector.html#a600712e14e4a4a55da773cdc6f39cdf4", null ]
];