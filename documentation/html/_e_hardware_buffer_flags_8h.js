var _e_hardware_buffer_flags_8h =
[
    [ "E_BUFFER_TYPE", "_e_hardware_buffer_flags_8h.html#a8f59a89ffef0ad8e5b2c2cb874a93e8c", [
      [ "EBT_NONE", "_e_hardware_buffer_flags_8h.html#a8f59a89ffef0ad8e5b2c2cb874a93e8caee6d32ea2b56461fe8b0706c91b10d86", null ],
      [ "EBT_VERTEX", "_e_hardware_buffer_flags_8h.html#a8f59a89ffef0ad8e5b2c2cb874a93e8caee6af56d004d0171bdd3f94968be5c9d", null ],
      [ "EBT_INDEX", "_e_hardware_buffer_flags_8h.html#a8f59a89ffef0ad8e5b2c2cb874a93e8cac94c9bb193455a5a064f2757eb797bac", null ],
      [ "EBT_VERTEX_AND_INDEX", "_e_hardware_buffer_flags_8h.html#a8f59a89ffef0ad8e5b2c2cb874a93e8ca833624730c30cffccc121fe31aa0832c", null ]
    ] ],
    [ "E_HARDWARE_MAPPING", "_e_hardware_buffer_flags_8h.html#ac7d8ee8d77da75f2580bb9bb17231c27", [
      [ "EHM_NEVER", "_e_hardware_buffer_flags_8h.html#ac7d8ee8d77da75f2580bb9bb17231c27a6eaae9a4147dfc68ce11fa12b9ce3c0d", null ],
      [ "EHM_STATIC", "_e_hardware_buffer_flags_8h.html#ac7d8ee8d77da75f2580bb9bb17231c27a52a4dd2fb6fe682da24de33bb59a7cf6", null ],
      [ "EHM_DYNAMIC", "_e_hardware_buffer_flags_8h.html#ac7d8ee8d77da75f2580bb9bb17231c27a9217fb955cf965a1fd48db5e492921fe", null ],
      [ "EHM_STREAM", "_e_hardware_buffer_flags_8h.html#ac7d8ee8d77da75f2580bb9bb17231c27a7a85a790b8f5fa3432a96f39f5a31e36", null ]
    ] ]
];