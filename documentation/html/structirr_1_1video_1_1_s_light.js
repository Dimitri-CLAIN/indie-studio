var structirr_1_1video_1_1_s_light =
[
    [ "SLight", "structirr_1_1video_1_1_s_light.html#a61379791eafc4f05373461d02c8d3f43", null ],
    [ "AmbientColor", "structirr_1_1video_1_1_s_light.html#a120e9234a2141eb1fbe46cad23794e0e", null ],
    [ "Attenuation", "structirr_1_1video_1_1_s_light.html#a06c10ab01b8ad1ee554b956bd5baeacc", null ],
    [ "CastShadows", "structirr_1_1video_1_1_s_light.html#afbc66ada0baaf9f642d165fb8b11118b", null ],
    [ "DiffuseColor", "structirr_1_1video_1_1_s_light.html#a226622e93aa1e249fe876fdb9d0186fe", null ],
    [ "Direction", "structirr_1_1video_1_1_s_light.html#a226c488abec5696b995df6895ab42d41", null ],
    [ "Falloff", "structirr_1_1video_1_1_s_light.html#a38cea8db5e6951a9d98802f2f249129d", null ],
    [ "InnerCone", "structirr_1_1video_1_1_s_light.html#ab079c170fbab1e0a191e8801839d6c83", null ],
    [ "OuterCone", "structirr_1_1video_1_1_s_light.html#a38ee805b9145d4ab1b1a889bbded9adc", null ],
    [ "Position", "structirr_1_1video_1_1_s_light.html#ac1f0fda0cc1780b09a2597adcfc9c946", null ],
    [ "Radius", "structirr_1_1video_1_1_s_light.html#a9f3fbdf2869d9beca604ba1cca7499cb", null ],
    [ "SpecularColor", "structirr_1_1video_1_1_s_light.html#a2d66ad28850a8588b2ba727103710604", null ],
    [ "Type", "structirr_1_1video_1_1_s_light.html#ac06681bb78d4911775f50d33e21761b4", null ]
];