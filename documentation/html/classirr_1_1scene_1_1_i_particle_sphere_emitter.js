var classirr_1_1scene_1_1_i_particle_sphere_emitter =
[
    [ "getCenter", "classirr_1_1scene_1_1_i_particle_sphere_emitter.html#aed7e8a6a26b53d066b18596faf52353c", null ],
    [ "getRadius", "classirr_1_1scene_1_1_i_particle_sphere_emitter.html#aae898401589c9152990990800cb573ce", null ],
    [ "getType", "classirr_1_1scene_1_1_i_particle_sphere_emitter.html#aed807c85d4a31076ffff8f2bc3524124", null ],
    [ "setCenter", "classirr_1_1scene_1_1_i_particle_sphere_emitter.html#a801d2ea9d0ebfb877e4c0fdbbf277ecd", null ],
    [ "setRadius", "classirr_1_1scene_1_1_i_particle_sphere_emitter.html#ab693f10a9129706e76aac806ea4c4f2b", null ]
];