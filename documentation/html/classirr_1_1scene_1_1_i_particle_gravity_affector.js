var classirr_1_1scene_1_1_i_particle_gravity_affector =
[
    [ "getGravity", "classirr_1_1scene_1_1_i_particle_gravity_affector.html#ac6368c49867f62a4e873dfd6142abec9", null ],
    [ "getTimeForceLost", "classirr_1_1scene_1_1_i_particle_gravity_affector.html#a957d0e0d273e4c2fb4b2cc6017a67038", null ],
    [ "getType", "classirr_1_1scene_1_1_i_particle_gravity_affector.html#a60a002206e1fa758137f92b8dc390b7c", null ],
    [ "setGravity", "classirr_1_1scene_1_1_i_particle_gravity_affector.html#ad5c31751978d5b0415a36353e4279e77", null ],
    [ "setTimeForceLost", "classirr_1_1scene_1_1_i_particle_gravity_affector.html#a3910d872c740e678373cecbd5523ad38", null ]
];