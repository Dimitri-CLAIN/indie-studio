var _i_bone_scene_node_8h =
[
    [ "IBoneSceneNode", "classirr_1_1scene_1_1_i_bone_scene_node.html", "classirr_1_1scene_1_1_i_bone_scene_node" ],
    [ "E_BONE_ANIMATION_MODE", "_i_bone_scene_node_8h.html#a318162c0a3aad1cf228ed7daddd44801", [
      [ "EBAM_AUTOMATIC", "_i_bone_scene_node_8h.html#a318162c0a3aad1cf228ed7daddd44801ad26c52974d12b0f95256af5f9ba11bde", null ],
      [ "EBAM_ANIMATED", "_i_bone_scene_node_8h.html#a318162c0a3aad1cf228ed7daddd44801a49f9737e748ea0a6fe7e4a636edcdd1b", null ],
      [ "EBAM_UNANIMATED", "_i_bone_scene_node_8h.html#a318162c0a3aad1cf228ed7daddd44801a5cc55f24b347cbc72b22aff86eb3b18a", null ],
      [ "EBAM_COUNT", "_i_bone_scene_node_8h.html#a318162c0a3aad1cf228ed7daddd44801a44d54a6246d59a7ab11fc0c4c86ee7ab", null ]
    ] ],
    [ "E_BONE_SKINNING_SPACE", "_i_bone_scene_node_8h.html#a47bfc785c34c953f926c920cd13ba1fb", [
      [ "EBSS_LOCAL", "_i_bone_scene_node_8h.html#a47bfc785c34c953f926c920cd13ba1fba024fe7bb628e922486989ca66e294811", null ],
      [ "EBSS_GLOBAL", "_i_bone_scene_node_8h.html#a47bfc785c34c953f926c920cd13ba1fbad73d241e222b6402d5e2064625d4823b", null ],
      [ "EBSS_COUNT", "_i_bone_scene_node_8h.html#a47bfc785c34c953f926c920cd13ba1fbad92dab44c392e53ca99dfdd2f0ccf790", null ]
    ] ],
    [ "BoneAnimationModeNames", "_i_bone_scene_node_8h.html#a578945909729ca194b0d44852aab8021", null ]
];