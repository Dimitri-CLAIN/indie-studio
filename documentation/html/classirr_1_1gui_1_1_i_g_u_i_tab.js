var classirr_1_1gui_1_1_i_g_u_i_tab =
[
    [ "IGUITab", "classirr_1_1gui_1_1_i_g_u_i_tab.html#a27026e3a71397700290289ea206b0b86", null ],
    [ "getBackgroundColor", "classirr_1_1gui_1_1_i_g_u_i_tab.html#ab9c9bd6b40bf90482d429d9a87f7e06c", null ],
    [ "getNumber", "classirr_1_1gui_1_1_i_g_u_i_tab.html#aedc76f9d93782188741ace78b32634f0", null ],
    [ "getTextColor", "classirr_1_1gui_1_1_i_g_u_i_tab.html#a91cd857c91bbbbe9a216adadf9ea3b8f", null ],
    [ "isDrawingBackground", "classirr_1_1gui_1_1_i_g_u_i_tab.html#ab357a99f63a7ecbd3da5adbc24f43e58", null ],
    [ "setBackgroundColor", "classirr_1_1gui_1_1_i_g_u_i_tab.html#aed83e8e69af7637cfd86380f6917a7de", null ],
    [ "setDrawBackground", "classirr_1_1gui_1_1_i_g_u_i_tab.html#a15aa82c853f4e72c1da37f556cd3b097", null ],
    [ "setTextColor", "classirr_1_1gui_1_1_i_g_u_i_tab.html#a56f7e206931506705b3a54674d6b8603", null ]
];