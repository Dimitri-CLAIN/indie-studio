var classirr_1_1scene_1_1_i_particle_affector =
[
    [ "IParticleAffector", "classirr_1_1scene_1_1_i_particle_affector.html#a060c7080c8c619e9bb2d35c7ac981889", null ],
    [ "affect", "classirr_1_1scene_1_1_i_particle_affector.html#a87fb9116bb1a4d3ea075c59f3e2d1dbc", null ],
    [ "getEnabled", "classirr_1_1scene_1_1_i_particle_affector.html#a5665add84311a13c2dbfed3a77670cae", null ],
    [ "getType", "classirr_1_1scene_1_1_i_particle_affector.html#add5652cd7dabb91342497fed776e2557", null ],
    [ "setEnabled", "classirr_1_1scene_1_1_i_particle_affector.html#aa3715a5686640a180462ad7843aaff0b", null ],
    [ "Enabled", "classirr_1_1scene_1_1_i_particle_affector.html#a23798f130948c5d4bd045f69f4035398", null ]
];