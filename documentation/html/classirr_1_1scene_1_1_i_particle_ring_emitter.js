var classirr_1_1scene_1_1_i_particle_ring_emitter =
[
    [ "getCenter", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#a5d09aae6d192de5ff94fa04235edd7f4", null ],
    [ "getRadius", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#af2a3e9b7a946359159e75d298968b6d8", null ],
    [ "getRingThickness", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#a6148ae3c6186d32f3714f83bbb71034c", null ],
    [ "getType", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#adbf6ff1bd1d25ee40e97bb17b152136f", null ],
    [ "setCenter", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#abb0b2574e3ffd07fad37328483722997", null ],
    [ "setRadius", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#a1601e885f65f118f26dd79646cf817eb", null ],
    [ "setRingThickness", "classirr_1_1scene_1_1_i_particle_ring_emitter.html#a23c9c9b497f75ee0b9fe1f2241b7afff", null ]
];