var _s_color_8h =
[
    [ "SColor", "classirr_1_1video_1_1_s_color.html", "classirr_1_1video_1_1_s_color" ],
    [ "SColorf", "classirr_1_1video_1_1_s_colorf.html", "classirr_1_1video_1_1_s_colorf" ],
    [ "SColorHSL", "classirr_1_1video_1_1_s_color_h_s_l.html", "classirr_1_1video_1_1_s_color_h_s_l" ],
    [ "ECOLOR_FORMAT", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829ed", [
      [ "ECF_A1R5G5B5", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda1207765b5eab1701111b5dc37b68b4bf", null ],
      [ "ECF_R5G6B5", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda49b4fe9d0ba395456dcd7c76a1ae9775", null ],
      [ "ECF_R8G8B8", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda3f0380aafb1e1fd59f5419a95d630a6d", null ],
      [ "ECF_A8R8G8B8", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda55c57d63efff39efe33ee733fe962df0", null ],
      [ "ECF_R16F", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829edaaa87f3817817a302753b8dbb26fc72ec", null ],
      [ "ECF_G16R16F", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829edafc5bcc8f47e78d073130300229c22113", null ],
      [ "ECF_A16B16G16R16F", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda91a9f2eeac94b28d4e1421625b53ab5e", null ],
      [ "ECF_R32F", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829edad271018ac22c4ff6c76c8ebb870a6c96", null ],
      [ "ECF_G32R32F", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda92ab57e6c0b93f96b08db4dc5656aea2", null ],
      [ "ECF_A32B32G32R32F", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda22958f549a8322e81b5210edaa172157", null ],
      [ "ECF_UNKNOWN", "_s_color_8h.html#a1d5e487888c32b1674a8f75116d829eda209608685f06c86c6ae489896a246e77", null ]
    ] ],
    [ "A1R5G5B5toA8R8G8B8", "_s_color_8h.html#a8aa9e69faa25064379df5a91273b9fbe", null ],
    [ "A1R5G5B5toR5G6B5", "_s_color_8h.html#a4906b4425165ee533dab17506fe98ce8", null ],
    [ "A8R8G8B8toA1R5G5B5", "_s_color_8h.html#a929c6758a0edc772cf69b2c674e4f5cd", null ],
    [ "A8R8G8B8toR5G6B5", "_s_color_8h.html#a10636302b225d7f525fa46bbd3f747a4", null ],
    [ "getAlpha", "_s_color_8h.html#a23358b142a005c4a4c747d227a4d77ab", null ],
    [ "getAverage", "_s_color_8h.html#ab242fde6c6e19ae4100fe39b0dadd9d0", null ],
    [ "getBlue", "_s_color_8h.html#ae8dc9edd5a3eb01de47933ac2d7f679e", null ],
    [ "getGreen", "_s_color_8h.html#a3e64185c36326ed33a494ba14cac00a5", null ],
    [ "getRed", "_s_color_8h.html#aa98234702c65c82ce29115b12e89dafe", null ],
    [ "R5G6B5toA1R5G5B5", "_s_color_8h.html#aaeb39c8fc1b9c4e78b68e2da210f70ff", null ],
    [ "R5G6B5toA8R8G8B8", "_s_color_8h.html#a421971627afe81533a3c2b51907b72bd", null ],
    [ "RGB16", "_s_color_8h.html#a55f59b4905cab236a03cb420fd55830d", null ],
    [ "RGB16from16", "_s_color_8h.html#a1f8b2e9ef461d76c0723536f776c2e36", null ],
    [ "RGBA16", "_s_color_8h.html#a65997ac1b7b7544acc0f4757be7860d1", null ],
    [ "X8R8G8B8toA1R5G5B5", "_s_color_8h.html#a6824dfc9c71a7fd9d7a3e704b1267ddc", null ]
];