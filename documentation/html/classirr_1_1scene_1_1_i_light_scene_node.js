var classirr_1_1scene_1_1_i_light_scene_node =
[
    [ "ILightSceneNode", "classirr_1_1scene_1_1_i_light_scene_node.html#aa13c236d797d731c1035820a909ba961", null ],
    [ "enableCastShadow", "classirr_1_1scene_1_1_i_light_scene_node.html#a1520d051fe04bc8c5c8975fb3908161b", null ],
    [ "getCastShadow", "classirr_1_1scene_1_1_i_light_scene_node.html#a5ba3a03fc0cfbad7dd8ce2f338a1991a", null ],
    [ "getLightData", "classirr_1_1scene_1_1_i_light_scene_node.html#a687813feae9312a86882e12c2bd10194", null ],
    [ "getLightData", "classirr_1_1scene_1_1_i_light_scene_node.html#a20147e049be1a4790346fd72b150b30c", null ],
    [ "getLightType", "classirr_1_1scene_1_1_i_light_scene_node.html#a47e327388c75391ebc910369af7eedce", null ],
    [ "getRadius", "classirr_1_1scene_1_1_i_light_scene_node.html#a4ce3cd789ed3adabd381ff7f915861a0", null ],
    [ "setLightData", "classirr_1_1scene_1_1_i_light_scene_node.html#acf74ff3400a26ae31eb96b9c479e62d5", null ],
    [ "setLightType", "classirr_1_1scene_1_1_i_light_scene_node.html#a18b3c0ba831bdc9166db341a35701c9b", null ],
    [ "setRadius", "classirr_1_1scene_1_1_i_light_scene_node.html#a7da64c8c4776988a39927827f2c3f364", null ],
    [ "setVisible", "classirr_1_1scene_1_1_i_light_scene_node.html#a3a6a6681a665ec4c214cda8a84a29337", null ]
];