var structirr_1_1scene_1_1_s_m_d3_header =
[
    [ "fileName", "structirr_1_1scene_1_1_s_m_d3_header.html#ab4d7b0f81078c5a301687f346a12471e", null ],
    [ "fileSize", "structirr_1_1scene_1_1_s_m_d3_header.html#aee478ef9e90fdb9fb650d3f682b17963", null ],
    [ "frameStart", "structirr_1_1scene_1_1_s_m_d3_header.html#a453c612968e035f465f02474c37a61ac", null ],
    [ "headerID", "structirr_1_1scene_1_1_s_m_d3_header.html#ab6de3cb1d4214f671fee8726ea7e0373", null ],
    [ "numFrames", "structirr_1_1scene_1_1_s_m_d3_header.html#a1aacfa4b3b7f5c494d29b5e8b665d017", null ],
    [ "numMaxSkins", "structirr_1_1scene_1_1_s_m_d3_header.html#a6233476b5b71f3e5e8a707887fd6eeb8", null ],
    [ "numMeshes", "structirr_1_1scene_1_1_s_m_d3_header.html#a4a6a9d28d4035b6a95d5b2373d35c4a3", null ],
    [ "numTags", "structirr_1_1scene_1_1_s_m_d3_header.html#a406f70af59da8c05f1a59acb9cbbbd1a", null ],
    [ "tagEnd", "structirr_1_1scene_1_1_s_m_d3_header.html#a35ecf8fff318ab08772c72d573c9ac09", null ],
    [ "tagStart", "structirr_1_1scene_1_1_s_m_d3_header.html#a740c1d288c660b0e00402ad0939a321a", null ],
    [ "Version", "structirr_1_1scene_1_1_s_m_d3_header.html#a5d0a343ac618bc20808475d8f04b8cd7", null ]
];