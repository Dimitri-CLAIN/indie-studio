var _i_video_driver_8h =
[
    [ "SOverrideMaterial", "structirr_1_1video_1_1_s_override_material.html", "structirr_1_1video_1_1_s_override_material" ],
    [ "IRenderTarget", "structirr_1_1video_1_1_i_render_target.html", "structirr_1_1video_1_1_i_render_target" ],
    [ "IVideoDriver", "classirr_1_1video_1_1_i_video_driver.html", "classirr_1_1video_1_1_i_video_driver" ],
    [ "E_FOG_TYPE", "_i_video_driver_8h.html#adf41b1a85e067f5988ba1eb8bb50f44e", [
      [ "EFT_FOG_EXP", "_i_video_driver_8h.html#adf41b1a85e067f5988ba1eb8bb50f44ea2db1a88aadc1602dcd24175a44fd1216", null ],
      [ "EFT_FOG_LINEAR", "_i_video_driver_8h.html#adf41b1a85e067f5988ba1eb8bb50f44ea998abcfd4824aaf15a95678bb444ef65", null ],
      [ "EFT_FOG_EXP2", "_i_video_driver_8h.html#adf41b1a85e067f5988ba1eb8bb50f44eaeb1b77beb92098ee943c2139e9d6827d", null ]
    ] ],
    [ "E_LOST_RESOURCE", "_i_video_driver_8h.html#a5b423450f4c1775bfdc86b5998c3db72", [
      [ "ELR_DEVICE", "_i_video_driver_8h.html#a5b423450f4c1775bfdc86b5998c3db72adac2eae358f00a2b6b8d5da56bc6d150", null ],
      [ "ELR_TEXTURES", "_i_video_driver_8h.html#a5b423450f4c1775bfdc86b5998c3db72a23b58f43419e5a990655f0ee331497a7", null ],
      [ "ELR_RTTS", "_i_video_driver_8h.html#a5b423450f4c1775bfdc86b5998c3db72aeb70fa0500307facb6ebfd368aaa1f5e", null ],
      [ "ELR_HW_BUFFERS", "_i_video_driver_8h.html#a5b423450f4c1775bfdc86b5998c3db72aababdf6d3357bb40f416cd76d25c7bcc", null ]
    ] ],
    [ "E_RENDER_TARGET", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972", [
      [ "ERT_FRAME_BUFFER", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972a5745c3dbb79f4796906d8b15c42496b0", null ],
      [ "ERT_RENDER_TEXTURE", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972a305e36c92304d2e5092ecb7385133b17", null ],
      [ "ERT_MULTI_RENDER_TEXTURES", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972af254f96ed0ded501c3914e01d22632bd", null ],
      [ "ERT_STEREO_LEFT_BUFFER", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972a80a275c293de34ec438db13c4a032353", null ],
      [ "ERT_STEREO_RIGHT_BUFFER", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972ac695413f499e2deb765c3dac34ec60c6", null ],
      [ "ERT_STEREO_BOTH_BUFFERS", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972ac2405bfece5721ae62cda52066f6258b", null ],
      [ "ERT_AUX_BUFFER0", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972a41d4de665ac1ee756a099fa0caf952c6", null ],
      [ "ERT_AUX_BUFFER1", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972a0d261d4af133fddfffa71037f966788a", null ],
      [ "ERT_AUX_BUFFER2", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972afb44de542fd07246de65d87fee11c8a7", null ],
      [ "ERT_AUX_BUFFER3", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972acd99c14f40c09f7949b2281af227b151", null ],
      [ "ERT_AUX_BUFFER4", "_i_video_driver_8h.html#a5b61a3f2bd5d458f76f2eb20b0f40972a09d687a205a65180ec6a10dadea50f3e", null ]
    ] ],
    [ "E_TRANSFORMATION_STATE", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43d", [
      [ "ETS_VIEW", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43da152f4262d5874186e0288934c7d31e14", null ],
      [ "ETS_WORLD", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43da843cf42adb3fa9caf61c9e228cf14e85", null ],
      [ "ETS_PROJECTION", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43dae7ec186418508c67a7562af012d7b63f", null ],
      [ "ETS_TEXTURE_0", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43da71b039bebde6b4977cf6928a271d31e2", null ],
      [ "ETS_TEXTURE_1", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43da20d8e5db672d9fa527b32272cc6eecff", null ],
      [ "ETS_TEXTURE_2", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43da48b90ebe5accb411556e907145019e93", null ],
      [ "ETS_TEXTURE_3", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43da8fa70f6b7dfd7c81b4d39e2041373482", null ],
      [ "ETS_COUNT", "_i_video_driver_8h.html#a15b57657a320243be03ae6f66fcff43daa8a64580fae4ef3017f1fc9bd48123c2", null ]
    ] ],
    [ "FogTypeNames", "_i_video_driver_8h.html#a59ea4e8a1d66fc3247cec107f2da4fe0", null ]
];