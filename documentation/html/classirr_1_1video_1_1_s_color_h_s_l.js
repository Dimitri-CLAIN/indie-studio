var classirr_1_1video_1_1_s_color_h_s_l =
[
    [ "SColorHSL", "classirr_1_1video_1_1_s_color_h_s_l.html#ad75a96f226bcbb38a2237fb7be22f537", null ],
    [ "fromRGB", "classirr_1_1video_1_1_s_color_h_s_l.html#ac5806ad1f238083ac59d6f3d21673e94", null ],
    [ "toRGB", "classirr_1_1video_1_1_s_color_h_s_l.html#a87dfb7cd3c4dab7825fe0bdcac366d62", null ],
    [ "Hue", "classirr_1_1video_1_1_s_color_h_s_l.html#a76d317e46d5a30982ed3da58401d319f", null ],
    [ "Luminance", "classirr_1_1video_1_1_s_color_h_s_l.html#acc584a689cf0e4a312c07d58f2a9f2b4", null ],
    [ "Saturation", "classirr_1_1video_1_1_s_color_h_s_l.html#a61a3b30e3a7724ec9d091001e411987e", null ]
];