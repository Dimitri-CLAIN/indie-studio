var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghijklmnopqrstuvwx",
  2: "ips",
  3: "abcdefghijklmnopqrstuvw",
  4: "abcdefghijklmnopqrstuvwxyz~",
  5: "_abcdefghijklmnoprstuvwxyz",
  6: "abcdfgilmnprstuv",
  7: "abcefkmpstuvw",
  8: "abcdefghijklmnopqrstuvwxyz",
  9: "cdhilmoprstuw",
  10: "_abcdefghimnoprstw",
  11: "agnsw",
  12: "dirs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages"
};

