var searchData=
[
  ['ieee_5f1_5f0_10150',['IEEE_1_0',['../irr_math_8h.html#a3ec4980c07f6b510c61230356e014d24',1,'irrMath.h']]],
  ['ieee_5f255_5f0_10151',['IEEE_255_0',['../irr_math_8h.html#a2b311316554140ac181c31d86f4f3786',1,'irrMath.h']]],
  ['irr_5ftest_5fbroken_5fquaternion_5fuse_10152',['IRR_TEST_BROKEN_QUATERNION_USE',['../quaternion_8h.html#a8d461cd143e35079fac38317cee885e9',1,'quaternion.h']]],
  ['irrcallconv_10153',['IRRCALLCONV',['../_irr_compile_config_8h.html#a3f9bf2240c47d3f99b86f83f9123aa9d',1,'IrrCompileConfig.h']]],
  ['irrlicht_5fapi_10154',['IRRLICHT_API',['../_irr_compile_config_8h.html#aa93137544a73eaa2563931a1e665862d',1,'IrrCompileConfig.h']]],
  ['irrlicht_5fsdk_5fversion_10155',['IRRLICHT_SDK_VERSION',['../_irr_compile_config_8h.html#a8eee87830586d48bc5921ded2775c3a3',1,'IrrCompileConfig.h']]],
  ['irrlicht_5fversion_5fmajor_10156',['IRRLICHT_VERSION_MAJOR',['../_irr_compile_config_8h.html#a6151c23fff5ce8cab48350152c549ef3',1,'IrrCompileConfig.h']]],
  ['irrlicht_5fversion_5fminor_10157',['IRRLICHT_VERSION_MINOR',['../_irr_compile_config_8h.html#a525cde277e65e7457d04a8611b7f7f7f',1,'IrrCompileConfig.h']]],
  ['irrlicht_5fversion_5frevision_10158',['IRRLICHT_VERSION_REVISION',['../_irr_compile_config_8h.html#ad592993084de4a67d10b82e6ff498b30',1,'IrrCompileConfig.h']]]
];
