var _e_material_types_8h =
[
    [ "E_MATERIAL_TYPE", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1", [
      [ "EMT_SOLID", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a9bc471b9c18c9e2d20496004d2a2e803", null ],
      [ "EMT_SOLID_2_LAYER", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a3246cbdb780b42b819eb8a24c2ef40d9", null ],
      [ "EMT_LIGHTMAP", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a5dc90a3b4a8d82f10503ddf834a3143f", null ],
      [ "EMT_LIGHTMAP_ADD", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a7aa628bfc8b6ac56c30e93549b799855", null ],
      [ "EMT_LIGHTMAP_M2", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1abd740658d7ca152bab745c63107d7edb", null ],
      [ "EMT_LIGHTMAP_M4", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1ad61a64f074256bb1cc1b6c130e18ebdc", null ],
      [ "EMT_LIGHTMAP_LIGHTING", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1add532294454c4842964c79a14ac4af34", null ],
      [ "EMT_LIGHTMAP_LIGHTING_M2", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a9cd386cb5151eabccbdded029529dec0", null ],
      [ "EMT_LIGHTMAP_LIGHTING_M4", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a6b7a3c28cfc214c63df8674565e28f17", null ],
      [ "EMT_DETAIL_MAP", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a065af4f8daeb15f81bfe0417a3f231b1", null ],
      [ "EMT_SPHERE_MAP", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a42a8b6f5c933864ca104b3d46692c43b", null ],
      [ "EMT_REFLECTION_2_LAYER", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1ad8574343353ed8ade6e78bc04d64b6ae", null ],
      [ "EMT_TRANSPARENT_ADD_COLOR", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a1b5a814c4466aca2943ff056003a50d1", null ],
      [ "EMT_TRANSPARENT_ALPHA_CHANNEL", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1ac08aa3715ad41281472202107a81f736", null ],
      [ "EMT_TRANSPARENT_ALPHA_CHANNEL_REF", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a167b3eb9c4c09ee1f145d914f4ddb619", null ],
      [ "EMT_TRANSPARENT_VERTEX_ALPHA", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a26529b1cf18ec4d8073809f6bd15ebbb", null ],
      [ "EMT_TRANSPARENT_REFLECTION_2_LAYER", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a8b074c837c82178daa178a3a7321a32d", null ],
      [ "EMT_NORMAL_MAP_SOLID", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a89220ece17ea7d54a530de9756734c70", null ],
      [ "EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a12323a7408cc28c4e57c4ae52758086c", null ],
      [ "EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a0d6f6973795d52d137955699537565db", null ],
      [ "EMT_PARALLAX_MAP_SOLID", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a833aaad409476c3c4baf59e2d1096f4a", null ],
      [ "EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a84787f1dfcbdc1578ecd84f6de4a22a1", null ],
      [ "EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a4c3e6b604a6d0fc5dffec661f470c11a", null ],
      [ "EMT_ONETEXTURE_BLEND", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a09498eaf291e1f7abdc04db808cc15d9", null ],
      [ "EMT_FORCE_32BIT", "_e_material_types_8h.html#ac8e9b6c66f7cebabd1a6d30cbc5430f1a85962cdf5aab2ddd245cada9b1859e30", null ]
    ] ],
    [ "sBuiltInMaterialTypeNames", "_e_material_types_8h.html#a833460ba01098710b6df3ec5c281c873", null ]
];