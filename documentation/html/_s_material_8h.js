var _s_material_8h =
[
    [ "SMaterial", "classirr_1_1video_1_1_s_material.html", "classirr_1_1video_1_1_s_material" ],
    [ "E_ALPHA_SOURCE", "_s_material_8h.html#a08b237bb445ffc0f32932c8ed87c6e63", [
      [ "EAS_NONE", "_s_material_8h.html#a08b237bb445ffc0f32932c8ed87c6e63a3989589d4fc8dd35d634b3a05e3db936", null ],
      [ "EAS_VERTEX_COLOR", "_s_material_8h.html#a08b237bb445ffc0f32932c8ed87c6e63a83768b0c4d47856b13ed409b43822d1f", null ],
      [ "EAS_TEXTURE", "_s_material_8h.html#a08b237bb445ffc0f32932c8ed87c6e63ad57d5d09230cefbfc77681b6ec2c92fe", null ]
    ] ],
    [ "E_ANTI_ALIASING_MODE", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149d", [
      [ "EAAM_OFF", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149dab88de631372a6fc318762f20f1095433", null ],
      [ "EAAM_SIMPLE", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149da05900b0839b8484a7ff78dbdac7e5dd7", null ],
      [ "EAAM_QUALITY", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149da25f380a6b1ba75d0dc08ea624974ebd8", null ],
      [ "EAAM_LINE_SMOOTH", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149daa91855f39a1b96c5aadd788663b564d3", null ],
      [ "EAAM_POINT_SMOOTH", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149dab40cf8dfee5fa3a823f2ae34510e63c8", null ],
      [ "EAAM_FULL_BASIC", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149dade3c9a3d46cbedc5304cfa869ab593fd", null ],
      [ "EAAM_ALPHA_TO_COVERAGE", "_s_material_8h.html#aa8647c2a52bdd3bc15ee773e8f2b149dac887edb1977865349bf92db9118f45cd", null ]
    ] ],
    [ "E_BLEND_FACTOR", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bc", [
      [ "EBF_ZERO", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca540abc7dd9e36d4e9ab1bd800ff0cabb", null ],
      [ "EBF_ONE", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca1e0d8b9190c96495eab3a6e6967d687f", null ],
      [ "EBF_DST_COLOR", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca8f6471362a6f2c661601d1d1b2c9428d", null ],
      [ "EBF_ONE_MINUS_DST_COLOR", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca0a8fb8fd43ebe39be3b742e6aee81936", null ],
      [ "EBF_SRC_COLOR", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca2aeff4b6a46a6689bae3e5c71c4f3e95", null ],
      [ "EBF_ONE_MINUS_SRC_COLOR", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bcaecb612040083c2007d5dcd280892de94", null ],
      [ "EBF_SRC_ALPHA", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca1ac42d35dd7883099dfb5be8f6300a2b", null ],
      [ "EBF_ONE_MINUS_SRC_ALPHA", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca7e6c99e4803094ff5bb9fd85f77d02b9", null ],
      [ "EBF_DST_ALPHA", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca404186d241278bbea7d8c69e7aa190d8", null ],
      [ "EBF_ONE_MINUS_DST_ALPHA", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca2648632cc055728f76f78ae2c5b58071", null ],
      [ "EBF_SRC_ALPHA_SATURATE", "_s_material_8h.html#acae10401850a6cfd5fcf1548c6c884bca124e79631dfb3fca9d212aaf7bc9b802", null ]
    ] ],
    [ "E_BLEND_OPERATION", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2f", [
      [ "EBO_NONE", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fa0eae3da7a67fbce9190da69e23e1adce", null ],
      [ "EBO_ADD", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fac1bcb9f240765e34102c22b6bab0ffea", null ],
      [ "EBO_SUBTRACT", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fa90e4c0c0d003e1da0c27106835ca2f9f", null ],
      [ "EBO_REVSUBTRACT", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fa470edec17573e74840c296343c8311e1", null ],
      [ "EBO_MIN", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fa443b00c00065df1c21066b21726d5ecb", null ],
      [ "EBO_MAX", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fae681d878e276ef201946cf1dcebbd6a4", null ],
      [ "EBO_MIN_FACTOR", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fa4c04a9d2b8aeedd64bee6852d53eaf00", null ],
      [ "EBO_MAX_FACTOR", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fab29de3584befb42eb0366f9a252b017d", null ],
      [ "EBO_MIN_ALPHA", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2fae3a17242783b97991a0fe87a3834a602", null ],
      [ "EBO_MAX_ALPHA", "_s_material_8h.html#a6d78c1faed23a03e8ef7b7b623bbaf2faaff74cfa9463e02759bcd5606e2e0e63", null ]
    ] ],
    [ "E_COLOR_MATERIAL", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78", [
      [ "ECM_NONE", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78a1fbe9f2cc7932b308a8a3503a9409a2e", null ],
      [ "ECM_DIFFUSE", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78a93dd321bbb0c52003ba44960ccc07afc", null ],
      [ "ECM_AMBIENT", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78a955744174b188f9787b548c53905bc26", null ],
      [ "ECM_EMISSIVE", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78a77001813414a4f119f7760dfb2c962d1", null ],
      [ "ECM_SPECULAR", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78a8dbb170ec93f1bed738e77da8e5fd0a2", null ],
      [ "ECM_DIFFUSE_AND_AMBIENT", "_s_material_8h.html#a41af617fc9a691366e4b162cd1cdea78a8411bc2cba6cf0609958a5d02ccc7a3b", null ]
    ] ],
    [ "E_COLOR_PLANE", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21", [
      [ "ECP_NONE", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21aa5afe366b19c967f884d0bd4ca083df9", null ],
      [ "ECP_ALPHA", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21ad345f3072dec27d31bedb82f1f3011a7", null ],
      [ "ECP_RED", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21a36263a436337ff10be86fc9b5ae2f6d0", null ],
      [ "ECP_GREEN", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21a1e712fd8781a461da6f0a293a3e7180f", null ],
      [ "ECP_BLUE", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21a31e270dab7c29a6e642ac491b172d16d", null ],
      [ "ECP_RGB", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21a41d3738a2d0a4c67fa01148b05ee3fc6", null ],
      [ "ECP_ALL", "_s_material_8h.html#aa9f25191ae536c1a4b08ec5334866a21a674ec773096b1b504b38ed78213e1e38", null ]
    ] ],
    [ "E_COMPARISON_FUNC", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878", [
      [ "ECFN_NEVER", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878a506fee7d9b0132be08b01b28495ed63c", null ],
      [ "ECFN_LESSEQUAL", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878a7d2ba60c17f30a8cc461da4f5f955ea5", null ],
      [ "ECFN_EQUAL", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878aa5ae12d8040a20df7f4bd8c3a2aa9079", null ],
      [ "ECFN_LESS", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878a214d9e4f7275ac53bc5b9dd4384b0021", null ],
      [ "ECFN_NOTEQUAL", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878add191a828e770427149a699fdddb5a40", null ],
      [ "ECFN_GREATEREQUAL", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878a4f417c9c480573b16e05d75274c34360", null ],
      [ "ECFN_GREATER", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878a44207d136fffb644ef8743b739b71e99", null ],
      [ "ECFN_ALWAYS", "_s_material_8h.html#af5f3986e1d1be0f8d9aca55130ff5878a8d14ef9c1e84519f2b16a6d8c19ae56d", null ]
    ] ],
    [ "E_MODULATE_FUNC", "_s_material_8h.html#a1402e9045137ae232fafbdf385800843", [
      [ "EMFN_MODULATE_1X", "_s_material_8h.html#a1402e9045137ae232fafbdf385800843a58cc346f344fb488d403a3783675e5d7", null ],
      [ "EMFN_MODULATE_2X", "_s_material_8h.html#a1402e9045137ae232fafbdf385800843a55186f7a29487a3b75eda15274f6d6bd", null ],
      [ "EMFN_MODULATE_4X", "_s_material_8h.html#a1402e9045137ae232fafbdf385800843afa19322c677d65bf90ad20d873caeac8", null ]
    ] ],
    [ "E_POLYGON_OFFSET", "_s_material_8h.html#a812b866b910c946f5bc813f8eab31144", [
      [ "EPO_BACK", "_s_material_8h.html#a812b866b910c946f5bc813f8eab31144a73dcec6718742ff2631ab7ca25685b32", null ],
      [ "EPO_FRONT", "_s_material_8h.html#a812b866b910c946f5bc813f8eab31144ae39fa3f07418d3f6f5dd2d245122d2cb", null ]
    ] ],
    [ "pack_textureBlendFunc", "_s_material_8h.html#ad4d715752a69b052e582b06283513e49", null ],
    [ "textureBlendFunc_hasAlpha", "_s_material_8h.html#a5df0764d7c9e6742eecdba61f5d8a810", null ],
    [ "unpack_textureBlendFunc", "_s_material_8h.html#a6d0644626c746278881ceae7e33e75c0", null ],
    [ "IdentityMaterial", "_s_material_8h.html#a6c7a0046dcf64bb165eca2af94045a61", null ],
    [ "MATERIAL_MAX_TEXTURES", "_s_material_8h.html#ad41ca808200ca2e8e9d9326355020052", null ],
    [ "PolygonOffsetDirectionNames", "_s_material_8h.html#a4c81c2f4785908499fe7eb8214df879a", null ]
];