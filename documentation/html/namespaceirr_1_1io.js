var namespaceirr_1_1io =
[
    [ "IArchiveLoader", "classirr_1_1io_1_1_i_archive_loader.html", "classirr_1_1io_1_1_i_archive_loader" ],
    [ "IAttributeExchangingObject", "classirr_1_1io_1_1_i_attribute_exchanging_object.html", "classirr_1_1io_1_1_i_attribute_exchanging_object" ],
    [ "IAttributes", "classirr_1_1io_1_1_i_attributes.html", "classirr_1_1io_1_1_i_attributes" ],
    [ "IFileArchive", "classirr_1_1io_1_1_i_file_archive.html", "classirr_1_1io_1_1_i_file_archive" ],
    [ "IFileList", "classirr_1_1io_1_1_i_file_list.html", "classirr_1_1io_1_1_i_file_list" ],
    [ "IFileReadCallBack", "classirr_1_1io_1_1_i_file_read_call_back.html", "classirr_1_1io_1_1_i_file_read_call_back" ],
    [ "IFileSystem", "classirr_1_1io_1_1_i_file_system.html", "classirr_1_1io_1_1_i_file_system" ],
    [ "IIrrXMLReader", "classirr_1_1io_1_1_i_irr_x_m_l_reader.html", "classirr_1_1io_1_1_i_irr_x_m_l_reader" ],
    [ "IReadFile", "classirr_1_1io_1_1_i_read_file.html", "classirr_1_1io_1_1_i_read_file" ],
    [ "IWriteFile", "classirr_1_1io_1_1_i_write_file.html", "classirr_1_1io_1_1_i_write_file" ],
    [ "IXMLBase", "classirr_1_1io_1_1_i_x_m_l_base.html", null ],
    [ "IXMLWriter", "classirr_1_1io_1_1_i_x_m_l_writer.html", "classirr_1_1io_1_1_i_x_m_l_writer" ],
    [ "SAttributeReadWriteOptions", "structirr_1_1io_1_1_s_attribute_read_write_options.html", "structirr_1_1io_1_1_s_attribute_read_write_options" ],
    [ "SNamedPath", "structirr_1_1io_1_1_s_named_path.html", "structirr_1_1io_1_1_s_named_path" ],
    [ "xmlChar", "structirr_1_1io_1_1xml_char.html", "structirr_1_1io_1_1xml_char" ]
];