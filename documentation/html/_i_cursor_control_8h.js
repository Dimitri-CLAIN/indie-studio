var _i_cursor_control_8h =
[
    [ "SCursorSprite", "structirr_1_1gui_1_1_s_cursor_sprite.html", "structirr_1_1gui_1_1_s_cursor_sprite" ],
    [ "ICursorControl", "classirr_1_1gui_1_1_i_cursor_control.html", "classirr_1_1gui_1_1_i_cursor_control" ],
    [ "ECURSOR_ICON", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879b", [
      [ "ECI_NORMAL", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba035d2ef432b28541c4cae6af7350b724", null ],
      [ "ECI_CROSS", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba06230702d664ff50caa53174fcb8dd93", null ],
      [ "ECI_HAND", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba1f6f871650eaf988b74f1528e8729c3c", null ],
      [ "ECI_HELP", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba4d7bb4ab9874fdebdbadf4ecff4f36ef", null ],
      [ "ECI_IBEAM", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba0adaf77f6f429ed4e29dae84ced95e96", null ],
      [ "ECI_NO", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879baea1febebe0d9c605473ee755bfafa211", null ],
      [ "ECI_WAIT", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba21d4e894c80bda844b63e1fa41780440", null ],
      [ "ECI_SIZEALL", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879baaf52e3fa2104ba7e0ae9d0e047e75d61", null ],
      [ "ECI_SIZENESW", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba882eca63166fe5ad03178ddb354c25ca", null ],
      [ "ECI_SIZENWSE", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba58ef40d8866aeafff9ed0793db893bb9", null ],
      [ "ECI_SIZENS", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879bac940331c55aabae19fce31cea8115f9d", null ],
      [ "ECI_SIZEWE", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879bae94a0a825a0426f0a0aeda56576f24f8", null ],
      [ "ECI_UP", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba95458027fde2f320f3bf05b5bcd62694", null ],
      [ "ECI_COUNT", "_i_cursor_control_8h.html#aefee802dd632c5735703e40ef40f879ba4960196a815443cf827e58ca48232583", null ]
    ] ],
    [ "ECURSOR_PLATFORM_BEHAVIOR", "_i_cursor_control_8h.html#abbd186f9cfba2f805d98248df226acef", [
      [ "ECPB_NONE", "_i_cursor_control_8h.html#abbd186f9cfba2f805d98248df226acefaea42899f7236957b21660fd74dda283d", null ],
      [ "ECPB_X11_CACHE_UPDATES", "_i_cursor_control_8h.html#abbd186f9cfba2f805d98248df226acefa462d7a82478e4b89a31c15fdb20bd16e", null ]
    ] ],
    [ "GUICursorIconNames", "_i_cursor_control_8h.html#a33bd57d04dbf92750f64a244df85cd51", null ]
];