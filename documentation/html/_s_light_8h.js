var _s_light_8h =
[
    [ "SLight", "structirr_1_1video_1_1_s_light.html", "structirr_1_1video_1_1_s_light" ],
    [ "E_LIGHT_TYPE", "_s_light_8h.html#aaf0e02f6f83cc35cf9e764bf18400d39", [
      [ "ELT_POINT", "_s_light_8h.html#aaf0e02f6f83cc35cf9e764bf18400d39a21113dc8282eeb64fa1d7ba0ec9ee26d", null ],
      [ "ELT_SPOT", "_s_light_8h.html#aaf0e02f6f83cc35cf9e764bf18400d39a613d955141b78edba7a1e2688b11448a", null ],
      [ "ELT_DIRECTIONAL", "_s_light_8h.html#aaf0e02f6f83cc35cf9e764bf18400d39a46a64f42740c097ee15d5c3a87961788", null ],
      [ "ELT_COUNT", "_s_light_8h.html#aaf0e02f6f83cc35cf9e764bf18400d39aa5fe0f7e9cf66fabb752c9b344c8b276", null ]
    ] ],
    [ "LightTypeNames", "_s_light_8h.html#a345c3fd9c805bbe508a49fa367846bc8", null ]
];