var _i_file_archive_8h =
[
    [ "IFileArchive", "classirr_1_1io_1_1_i_file_archive.html", "classirr_1_1io_1_1_i_file_archive" ],
    [ "IArchiveLoader", "classirr_1_1io_1_1_i_archive_loader.html", "classirr_1_1io_1_1_i_archive_loader" ],
    [ "E_FILE_ARCHIVE_TYPE", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14f", [
      [ "EFAT_ZIP", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fa63010a52f2efb42f3c85b91ecf077004", null ],
      [ "EFAT_GZIP", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fa9d37ff1b229bb63f52d29ef65a35b6ce", null ],
      [ "EFAT_FOLDER", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fa1d1f159095f087da5647835b47cd85d4", null ],
      [ "EFAT_PAK", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14faae9e1788acfc49c30a77f80f5ced7cf5", null ],
      [ "EFAT_NPK", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fafcb0e1222939f010af78f15556f4fe07", null ],
      [ "EFAT_TAR", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fa0f36ce25d47b45f41a58e32b4c029cb6", null ],
      [ "EFAT_WAD", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fa58895648160db47291c12b7ade47f68c", null ],
      [ "EFAT_UNKNOWN", "_i_file_archive_8h.html#adb3e3c445ec8e608ed1f0f93306da14fa2c2aea9bc955ae4e0d29071ba66ff8dc", null ]
    ] ],
    [ "EFileSystemType", "_i_file_archive_8h.html#a22364f1caf06442a70f6198025af3fe9", [
      [ "FILESYSTEM_NATIVE", "_i_file_archive_8h.html#a22364f1caf06442a70f6198025af3fe9acfbc9e5e8773ec885f08eafd6c8d3c6f", null ],
      [ "FILESYSTEM_VIRTUAL", "_i_file_archive_8h.html#a22364f1caf06442a70f6198025af3fe9a94a9818df56a8fe16b1c7c6f44e8f9f3", null ]
    ] ]
];