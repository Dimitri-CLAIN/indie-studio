var structindie_1_1_size_component =
[
    [ "SizeComponent", "structindie_1_1_size_component.html#a20983c1fdd8afc8bf0d00a6153737263", null ],
    [ "~SizeComponent", "structindie_1_1_size_component.html#a63df38eb8730e477ac3190b97eb53638", null ],
    [ "incX", "structindie_1_1_size_component.html#aae99ccab715d3d8e21e0b6890eb296fa", null ],
    [ "incY", "structindie_1_1_size_component.html#a04d6c4ddedb1894238354e7c49b208a3", null ],
    [ "setX", "structindie_1_1_size_component.html#abedf81561d1802cd16a27990661a1874", null ],
    [ "setY", "structindie_1_1_size_component.html#a67708197cf630d1b653bbd51b25c7bee", null ],
    [ "sizeX", "structindie_1_1_size_component.html#a435db37557a202452ddb677721aa7861", null ],
    [ "sizeY", "structindie_1_1_size_component.html#a6b2215afc1116604a730db0573d59b56", null ]
];