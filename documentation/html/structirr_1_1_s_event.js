var structirr_1_1_s_event =
[
    [ "SGUIEvent", "structirr_1_1_s_event_1_1_s_g_u_i_event.html", "structirr_1_1_s_event_1_1_s_g_u_i_event" ],
    [ "SJoystickEvent", "structirr_1_1_s_event_1_1_s_joystick_event.html", "structirr_1_1_s_event_1_1_s_joystick_event" ],
    [ "SKeyInput", "structirr_1_1_s_event_1_1_s_key_input.html", "structirr_1_1_s_event_1_1_s_key_input" ],
    [ "SLogEvent", "structirr_1_1_s_event_1_1_s_log_event.html", "structirr_1_1_s_event_1_1_s_log_event" ],
    [ "SMouseInput", "structirr_1_1_s_event_1_1_s_mouse_input.html", "structirr_1_1_s_event_1_1_s_mouse_input" ],
    [ "SUserEvent", "structirr_1_1_s_event_1_1_s_user_event.html", "structirr_1_1_s_event_1_1_s_user_event" ],
    [ "EventType", "structirr_1_1_s_event.html#a8b48c016d5c20a9b0967b1ce0fb3ef15", null ],
    [ "GUIEvent", "structirr_1_1_s_event.html#a8efed75c2ae2e39c0f5208bffbe5e009", null ],
    [ "JoystickEvent", "structirr_1_1_s_event.html#a5fa8b0afcd0d4e24996b74d5a4fd0a6f", null ],
    [ "KeyInput", "structirr_1_1_s_event.html#abe216d2ccbae64f509cca162072c9d7e", null ],
    [ "LogEvent", "structirr_1_1_s_event.html#a9a730ec16e0d73345d542a305aa580db", null ],
    [ "MouseInput", "structirr_1_1_s_event.html#a6b170acaaba47761639a789ace5dfbc8", null ],
    [ "UserEvent", "structirr_1_1_s_event.html#a0434051e36546e4c87c28a6c9689b6fc", null ]
];