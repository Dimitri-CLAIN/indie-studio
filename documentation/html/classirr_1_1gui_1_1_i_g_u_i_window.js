var classirr_1_1gui_1_1_i_g_u_i_window =
[
    [ "IGUIWindow", "classirr_1_1gui_1_1_i_g_u_i_window.html#a964282be17981b8287b0a85d65a4008d", null ],
    [ "getClientRect", "classirr_1_1gui_1_1_i_g_u_i_window.html#aa6d240eb9d5b9b44c0e45fcef47e6216", null ],
    [ "getCloseButton", "classirr_1_1gui_1_1_i_g_u_i_window.html#ae5b6abf3c9d8d0af5539adf3fea8db70", null ],
    [ "getDrawBackground", "classirr_1_1gui_1_1_i_g_u_i_window.html#a6a7720d558220d29125bc494961f4463", null ],
    [ "getDrawTitlebar", "classirr_1_1gui_1_1_i_g_u_i_window.html#a634a224a31577fb7eeef578c9a351b69", null ],
    [ "getMaximizeButton", "classirr_1_1gui_1_1_i_g_u_i_window.html#a7b5976907664ed8603f3a603079d3b15", null ],
    [ "getMinimizeButton", "classirr_1_1gui_1_1_i_g_u_i_window.html#a9c9b5060ca57c46bfece1339ef30facb", null ],
    [ "isDraggable", "classirr_1_1gui_1_1_i_g_u_i_window.html#a466621df62d0dd97eb008efd3d31d4d9", null ],
    [ "setDraggable", "classirr_1_1gui_1_1_i_g_u_i_window.html#aeec78bc610444411cafd5925ab7650a7", null ],
    [ "setDrawBackground", "classirr_1_1gui_1_1_i_g_u_i_window.html#a76f7d790554c6bd60b601a64a4127169", null ],
    [ "setDrawTitlebar", "classirr_1_1gui_1_1_i_g_u_i_window.html#ae8fa25bf5eec28b8fd663f2e3856200b", null ]
];