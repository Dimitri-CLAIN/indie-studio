var classirr_1_1gui_1_1_i_g_u_i_spin_box =
[
    [ "IGUISpinBox", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a9e9c785f73b8fadbaff330f1cbb61adf", null ],
    [ "getEditBox", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a04bd863724a7b37b97e7f9368fda77ef", null ],
    [ "getMax", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#af332d184560dee8f273b50d1beca45cc", null ],
    [ "getMin", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a22f2211e612a7cfe7847d249724cadc1", null ],
    [ "getStepSize", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a5c66183b25860667488ff6c97df241bd", null ],
    [ "getValue", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#acfb0ebe7ab30b58e9d34b7925e785bb8", null ],
    [ "setDecimalPlaces", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a8a335d32cbdb7f43ca814422f8cee098", null ],
    [ "setRange", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#af7bc07a7be30d16a6ff27750782aaa80", null ],
    [ "setStepSize", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a64296e93b52129fcf9068279baf0697d", null ],
    [ "setValue", "classirr_1_1gui_1_1_i_g_u_i_spin_box.html#a28e388e3767d4257f712d899c744bc20", null ]
];