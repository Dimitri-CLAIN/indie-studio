var _e_culling_types_8h =
[
    [ "E_CULLING_TYPE", "_e_culling_types_8h.html#acabb2772476aa3706e65a7dc77fd9cce", [
      [ "EAC_OFF", "_e_culling_types_8h.html#acabb2772476aa3706e65a7dc77fd9cceacf8b41fd7d45781cfb2eb7039ba6b09a", null ],
      [ "EAC_BOX", "_e_culling_types_8h.html#acabb2772476aa3706e65a7dc77fd9ccea28580fa38fb38096fdf29555d2488ff6", null ],
      [ "EAC_FRUSTUM_BOX", "_e_culling_types_8h.html#acabb2772476aa3706e65a7dc77fd9ccea21aafa18a2103c249f1260ea2ba13774", null ],
      [ "EAC_FRUSTUM_SPHERE", "_e_culling_types_8h.html#acabb2772476aa3706e65a7dc77fd9cceaa43d4737db36d4c34225155ec628e170", null ],
      [ "EAC_OCC_QUERY", "_e_culling_types_8h.html#acabb2772476aa3706e65a7dc77fd9ccea8752dd77ac821c27fb25420ffd9ba76c", null ]
    ] ],
    [ "AutomaticCullingNames", "_e_culling_types_8h.html#afb3e3fc1668bd602189bab446801aa12", null ]
];