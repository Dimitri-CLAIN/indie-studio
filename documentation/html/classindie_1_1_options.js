var classindie_1_1_options =
[
    [ "event", "classindie_1_1_options.html#aa2290b9b83c1a70426ead857bc2deb8c", [
      [ "Nothing", "classindie_1_1_options.html#aa2290b9b83c1a70426ead857bc2deb8ca9e757b4095d6d868689e1400cd221ba7", null ],
      [ "Back", "classindie_1_1_options.html#aa2290b9b83c1a70426ead857bc2deb8ca34b6915fa1c5479964f1ec175f43c5b4", null ]
    ] ],
    [ "Options", "classindie_1_1_options.html#a785894c2309748076c422dd94a591630", null ],
    [ "Options", "classindie_1_1_options.html#a5119b68d741afd7f24c07c10df90f178", null ],
    [ "Options", "classindie_1_1_options.html#a2e6aa4153376e2ed5a3097dbe9e64b91", null ],
    [ "~Options", "classindie_1_1_options.html#a82fdabf2b777a1fc6617a041c853b604", null ],
    [ "dest", "classindie_1_1_options.html#adb8103a3ce5ef0fa02adc81b1c16046b", null ],
    [ "init", "classindie_1_1_options.html#a0bf8b1f900fe73743c41328cfbe4d752", null ],
    [ "launchLoop", "classindie_1_1_options.html#ac31a5811a189c83a12126f0e48610445", null ],
    [ "operator=", "classindie_1_1_options.html#a405ecc7457d8c7b7be5516b2f4e8937e", null ]
];