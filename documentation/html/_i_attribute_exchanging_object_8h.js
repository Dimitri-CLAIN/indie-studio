var _i_attribute_exchanging_object_8h =
[
    [ "SAttributeReadWriteOptions", "structirr_1_1io_1_1_s_attribute_read_write_options.html", "structirr_1_1io_1_1_s_attribute_read_write_options" ],
    [ "IAttributeExchangingObject", "classirr_1_1io_1_1_i_attribute_exchanging_object.html", "classirr_1_1io_1_1_i_attribute_exchanging_object" ],
    [ "E_ATTRIBUTE_READ_WRITE_FLAGS", "_i_attribute_exchanging_object_8h.html#a84923cf86af38e49c6ec5ee36903d782", [
      [ "EARWF_FOR_FILE", "_i_attribute_exchanging_object_8h.html#a84923cf86af38e49c6ec5ee36903d782aaa5edcb75733f65299b9cdd64e42513b", null ],
      [ "EARWF_FOR_EDITOR", "_i_attribute_exchanging_object_8h.html#a84923cf86af38e49c6ec5ee36903d782a9475477129f567bd0f284620fd779334", null ],
      [ "EARWF_USE_RELATIVE_PATHS", "_i_attribute_exchanging_object_8h.html#a84923cf86af38e49c6ec5ee36903d782ace3aad4e68fc4d12473c1e4ef39d62de", null ]
    ] ]
];