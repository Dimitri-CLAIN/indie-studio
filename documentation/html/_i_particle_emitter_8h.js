var _i_particle_emitter_8h =
[
    [ "IParticleEmitter", "classirr_1_1scene_1_1_i_particle_emitter.html", "classirr_1_1scene_1_1_i_particle_emitter" ],
    [ "IParticlePointEmitter", "_i_particle_emitter_8h.html#a719d36e28f832373a9aa9596bde0da89", null ],
    [ "E_PARTICLE_EMITTER_TYPE", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272b", [
      [ "EPET_POINT", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272bafc201018448a1f3d89151f22c33c53c3", null ],
      [ "EPET_ANIMATED_MESH", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272ba448cfefed583b0d3248414c8fb5a2e80", null ],
      [ "EPET_BOX", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272baa97953394f59cabeea2c2dda9938d745", null ],
      [ "EPET_CYLINDER", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272baa466f9d78fa14ace831342ebb76ad9e6", null ],
      [ "EPET_MESH", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272ba5b78b795480a2f86f3ebe176e39da883", null ],
      [ "EPET_RING", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272bae71cfa584d0b54671ba70bfd8dff8256", null ],
      [ "EPET_SPHERE", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272ba0d46a5e3b07f6c8a4ebbdd6e9022dbf9", null ],
      [ "EPET_COUNT", "_i_particle_emitter_8h.html#a3e251a881c886884a78adea2e546272bac8b95f0033334e772ff03434b099fb38", null ]
    ] ],
    [ "ParticleEmitterTypeNames", "_i_particle_emitter_8h.html#adf760b7abdec907704ec6190dfc0caba", null ]
];