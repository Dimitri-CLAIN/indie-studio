var classirr_1_1scene_1_1_i_volume_light_scene_node =
[
    [ "IVolumeLightSceneNode", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#a4bbb5ce233b9f8807ca62cbc0d48e5a9", null ],
    [ "getFootColor", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#a564c49d77b974aee4ff0441637b64f8c", null ],
    [ "getSubDivideU", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#a77a950374e4f15acc23094e3fa53b484", null ],
    [ "getSubDivideV", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#af11ccdd1f067d70e6b6ea1973516415f", null ],
    [ "getTailColor", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#aa4c589c6b9438619893984d48b56971b", null ],
    [ "getType", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#add658e48428640577ff9b0a1bda11b1a", null ],
    [ "setFootColor", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#aa44a1ade6dfc5383a51c6c88550d7ae0", null ],
    [ "setSubDivideU", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#a38d9dfc64c70660882c3bed4bfb65aea", null ],
    [ "setSubDivideV", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#a768059900d05389fd7cd832290d085fc", null ],
    [ "setTailColor", "classirr_1_1scene_1_1_i_volume_light_scene_node.html#a4934b1e6a0855d171d8c588f7cc2e845", null ]
];