var _s_key_map_8h =
[
    [ "SKeyMap", "structirr_1_1_s_key_map.html", "structirr_1_1_s_key_map" ],
    [ "EKEY_ACTION", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168", [
      [ "EKA_MOVE_FORWARD", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168af9ff845aa49060787ef81773eeaf1016", null ],
      [ "EKA_MOVE_BACKWARD", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168af4c724e0a0923e497c7f8a48884e36d9", null ],
      [ "EKA_STRAFE_LEFT", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168a91cad59a86b88c0e6ab5aa75b936d9c7", null ],
      [ "EKA_STRAFE_RIGHT", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168a57c4bd21c235bca312e547c6d1061df2", null ],
      [ "EKA_JUMP_UP", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168a574c0935be6337a2e6bc97e8304a7400", null ],
      [ "EKA_CROUCH", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168aace30d0d2eef7a48f5f863a99dabfb84", null ],
      [ "EKA_COUNT", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168abc7984adc60507c061aef78e857244c9", null ],
      [ "EKA_FORCE_32BIT", "_s_key_map_8h.html#aa9946ac9f3142f9e790ce52d59fd6168a27e7067f9bc0bc5143b0bac183ca6c6b", null ]
    ] ]
];