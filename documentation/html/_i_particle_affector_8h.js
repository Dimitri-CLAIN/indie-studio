var _i_particle_affector_8h =
[
    [ "IParticleAffector", "classirr_1_1scene_1_1_i_particle_affector.html", "classirr_1_1scene_1_1_i_particle_affector" ],
    [ "E_PARTICLE_AFFECTOR_TYPE", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010b", [
      [ "EPAT_NONE", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010bac5e1a70b4a2cecc5bf29d35f1e38aac1", null ],
      [ "EPAT_ATTRACT", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010ba90fb4c0dd7e58e7373b76596060402e1", null ],
      [ "EPAT_FADE_OUT", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010baa6b5c24be100f9beeb20a096105149f4", null ],
      [ "EPAT_GRAVITY", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010ba091fb685410e8b2134f7a81ae689ec5e", null ],
      [ "EPAT_ROTATE", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010ba25ace022a9e33b02eb1faeb6fa526575", null ],
      [ "EPAT_SCALE", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010ba9241becabb2dc35e6c1a612a454433de", null ],
      [ "EPAT_COUNT", "_i_particle_affector_8h.html#a34c0f9475cfcbda8b50ad816a046010ba3decef25f1986898147a12b259ed8573", null ]
    ] ],
    [ "ParticleAffectorTypeNames", "_i_particle_affector_8h.html#a1ce0e47158d68b401d23d833bc70bb86", null ]
];