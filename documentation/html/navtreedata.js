/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "OOP_indie_studio_2019", "index.html", [
    [ "Irrlicht Engine 1.8 API documentation", "index.html", [
      [ "Introduction", "index.html#intro", null ],
      [ "Links", "index.html#links", null ],
      [ "Short example", "index.html#irrexample", null ]
    ] ],
    [ "SFML", "md_cmake__s_f_m_l-2_85_81_license.html", null ],
    [ "readme", "md_cmake__s_f_m_l-2_85_81_readme.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", "namespacemembers_eval" ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_action_component_8cpp.html",
"_e_driver_types_8h.html",
"_ftp_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4",
"_i_event_receiver_8h.html#a2dbf2a247aa17a9eeefbbf36ebd5739fab24cbd2197b888022531799561c12b49",
"_i_g_u_i_tab_control_8h.html",
"_i_texture_8h.html#acaf6f7414534f7d62bff18c5bf11876f",
"_keycodes_8h.html#a54da2a0e231901735e3da1b0edf72eb3a6383bc1bfa470bf66e9011160ad5f2a0",
"_s_color_8h.html#aa98234702c65c82ce29115b12e89dafe",
"_t_c_p_8cpp_source.html",
"classindie_1_1_crate.html#abcd7838d80122fc81911ad3542a28399",
"classirr_1_1_i_logger.html#acbbc214a06cd968409000f55aa76c82f",
"classirr_1_1core_1_1dimension2d.html#a7bc8e7a51170f1e0758476d5fac4ff1b",
"classirr_1_1core_1_1quaternion.html#a76b7f0569737c2143ca6dae47abd8fdd",
"classirr_1_1gui_1_1_i_cursor_control.html",
"classirr_1_1gui_1_1_i_g_u_i_environment.html#acdfcdf6330e7475e3fdfd42f43c5f6df",
"classirr_1_1gui_1_1_i_g_u_i_tree_view_node.html#a2da4071958b479dbd5799d3834a0db77",
"classirr_1_1io_1_1_i_file_system.html#afe6641c7f88a8fea0205c113b8379730",
"classirr_1_1scene_1_1_i_collada_mesh_writer.html#a25d2e1ff0bf04375c822800b0b3a4b01",
"classirr_1_1scene_1_1_i_particle_animated_mesh_scene_node_emitter.html#afe672788259a71157ab26ff334d68c52",
"classirr_1_1scene_1_1_i_scene_node.html#a1f44d8cf753b2e4c17c90d4fc2ed05b2",
"classirr_1_1scene_1_1_s_vertex_position_scale_along_normals_manipulator.html",
"classirr_1_1video_1_1_s_color.html#adf45f90ff13a1eb56b5784a3fba20247",
"classsf_1_1_ftp_1_1_response.html#af300fffd4862774102f978eb22f85d9b",
"classsf_1_1_keyboard.html#acb4cacd7cc5802dec45724cf3314a142ac3fe5df11d15b57317c053a2ae13d9a9",
"classsf_1_1_shader.html#aca5c55c4a3b23d21e33dbdaab7990755",
"classsf_1_1_text.html#a084c275eb4bca835696af5f8f9c80ab3",
"classsf_1_1_vertex_buffer.html#a326a5c89f1ba01b51b323535494434e8",
"functions_func_v.html",
"irr_x_m_l_8h.html#a70f411ff403636fb5c4e9becb090d5ec",
"structirr_1_1_i_timer_1_1_real_time_date.html#a20c5475e8e9a65e410f218f192ca6d28",
"structirr_1_1scene_1_1_s_m_d3_quaternion_tag_list.html#a97809d2950f0740e774d2e40da626fe7",
"structirr_1_1video_1_1_s3_d_vertex.html#ad40e34b3828b1b58283090e20b280e47"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';