var _i_logger_8h =
[
    [ "ILogger", "classirr_1_1_i_logger.html", "classirr_1_1_i_logger" ],
    [ "ELOG_LEVEL", "_i_logger_8h.html#aa2d1cac68606a25ed24cfffccfa30a92", [
      [ "ELL_DEBUG", "_i_logger_8h.html#aa2d1cac68606a25ed24cfffccfa30a92a58d2a62ce004018e8bb6a29c732c70e3", null ],
      [ "ELL_INFORMATION", "_i_logger_8h.html#aa2d1cac68606a25ed24cfffccfa30a92a9d74de15737e326a91aec6f38c23f9cf", null ],
      [ "ELL_WARNING", "_i_logger_8h.html#aa2d1cac68606a25ed24cfffccfa30a92ad3d7f6bdd2f842cfc17561ba4be95001", null ],
      [ "ELL_ERROR", "_i_logger_8h.html#aa2d1cac68606a25ed24cfffccfa30a92a1c16e615e15fc3f75bacb50faaeb73ff", null ],
      [ "ELL_NONE", "_i_logger_8h.html#aa2d1cac68606a25ed24cfffccfa30a92a558fe286a76cbba090c8301f7b1f7dcb", null ]
    ] ]
];