var classirr_1_1gui_1_1_i_cursor_control =
[
    [ "addIcon", "classirr_1_1gui_1_1_i_cursor_control.html#a102ff455c70595886281e636ef063d3b", null ],
    [ "changeIcon", "classirr_1_1gui_1_1_i_cursor_control.html#a3e7c8cb1f03e1ccc31fcc3c30f717762", null ],
    [ "getActiveIcon", "classirr_1_1gui_1_1_i_cursor_control.html#aeffb0659049614ced7a56b607177c3bb", null ],
    [ "getPlatformBehavior", "classirr_1_1gui_1_1_i_cursor_control.html#aad19b5b02de0b8bc476c66b152b745c4", null ],
    [ "getPosition", "classirr_1_1gui_1_1_i_cursor_control.html#a65d9f6e734baa02be69b7e9f5fbdd565", null ],
    [ "getRelativePosition", "classirr_1_1gui_1_1_i_cursor_control.html#a8ba1cb0ff11edc5fb32cdadddece09f8", null ],
    [ "getSupportedIconSize", "classirr_1_1gui_1_1_i_cursor_control.html#a0710b659cbab1474e3486f7dd0e8c35e", null ],
    [ "isVisible", "classirr_1_1gui_1_1_i_cursor_control.html#ae1d1ca4c1c3042388881fabda4e53a42", null ],
    [ "setActiveIcon", "classirr_1_1gui_1_1_i_cursor_control.html#af394700d5279b13cc0f2bcdad679469c", null ],
    [ "setPlatformBehavior", "classirr_1_1gui_1_1_i_cursor_control.html#ad7688bb200945f15877a598e8be53878", null ],
    [ "setPosition", "classirr_1_1gui_1_1_i_cursor_control.html#a951b5afe97fa21d98ce5360d96314306", null ],
    [ "setPosition", "classirr_1_1gui_1_1_i_cursor_control.html#a421c770ffc494f8f6082a16bef0feed2", null ],
    [ "setPosition", "classirr_1_1gui_1_1_i_cursor_control.html#adca41054684f73435c9b045520f7c83b", null ],
    [ "setPosition", "classirr_1_1gui_1_1_i_cursor_control.html#a3b0a59608d1d0810079349acfa01a79b", null ],
    [ "setReferenceRect", "classirr_1_1gui_1_1_i_cursor_control.html#a2a7428ef716a60f8f4b86361a69b8770", null ],
    [ "setVisible", "classirr_1_1gui_1_1_i_cursor_control.html#aceb41d68494e2b2076fbc6949b254c74", null ]
];