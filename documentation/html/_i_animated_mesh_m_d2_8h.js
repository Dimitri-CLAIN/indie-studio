var _i_animated_mesh_m_d2_8h =
[
    [ "IAnimatedMeshMD2", "classirr_1_1scene_1_1_i_animated_mesh_m_d2.html", "classirr_1_1scene_1_1_i_animated_mesh_m_d2" ],
    [ "EMD2_ANIMATION_TYPE", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19", [
      [ "EMAT_STAND", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a35893ae58423c41dace23ad13e262e2c", null ],
      [ "EMAT_RUN", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19af240ef944e6e257ec369f87053c756e8", null ],
      [ "EMAT_ATTACK", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a2ff847d13f50b9ade089808df381bc42", null ],
      [ "EMAT_PAIN_A", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a7ef3070606abf1d6529512a0540a376d", null ],
      [ "EMAT_PAIN_B", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a17fe910179f2ceb045b6c865421add5e", null ],
      [ "EMAT_PAIN_C", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a5e80f8bb897b8d5001a9741d60a34ae5", null ],
      [ "EMAT_JUMP", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ae405ebd94a9595eb0e5444eab4157aee", null ],
      [ "EMAT_FLIP", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a42b8468a0cb84cc5f9a3433618ac85c2", null ],
      [ "EMAT_SALUTE", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ab193197927cc2408970e3039e6457309", null ],
      [ "EMAT_FALLBACK", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ab964cb1e2ac0ecdbd3ac30b58dfe717f", null ],
      [ "EMAT_WAVE", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a0de1308a8a0535299fa09df80ddc0338", null ],
      [ "EMAT_POINT", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19aac114a5c69f38d4f943145dd98929d95", null ],
      [ "EMAT_CROUCH_STAND", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a09269e097ca6a8ccd9052c06215d868c", null ],
      [ "EMAT_CROUCH_WALK", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a44aaa40b529734cc6e7025aed07be14b", null ],
      [ "EMAT_CROUCH_ATTACK", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19abfd3904fd08ebb548970b386fb075696", null ],
      [ "EMAT_CROUCH_PAIN", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a461dbf65f5adde2d661bd514a7fd5c5a", null ],
      [ "EMAT_CROUCH_DEATH", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a2161f76bef818fde2619b74030ec86f0", null ],
      [ "EMAT_DEATH_FALLBACK", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ac8c0d550c589fc801e9d24b87e3a489e", null ],
      [ "EMAT_DEATH_FALLFORWARD", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ab517f9cd7ef27068fa07c11cc6838949", null ],
      [ "EMAT_DEATH_FALLBACKSLOW", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ab25089817a141836f340f61ad1851b13", null ],
      [ "EMAT_BOOM", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19ab5a8ac059e08214f4a233afe0322da87", null ],
      [ "EMAT_COUNT", "_i_animated_mesh_m_d2_8h.html#a08d4a84966e1d2886d0d57e4acbb4f19a5fab9bcda993dba3607011f09f1b7c81", null ]
    ] ]
];