var namespaceirr_1_1video =
[
    [ "IGPUProgrammingServices", "classirr_1_1video_1_1_i_g_p_u_programming_services.html", "classirr_1_1video_1_1_i_g_p_u_programming_services" ],
    [ "IImage", "classirr_1_1video_1_1_i_image.html", "classirr_1_1video_1_1_i_image" ],
    [ "IImageLoader", "classirr_1_1video_1_1_i_image_loader.html", "classirr_1_1video_1_1_i_image_loader" ],
    [ "IImageWriter", "classirr_1_1video_1_1_i_image_writer.html", "classirr_1_1video_1_1_i_image_writer" ],
    [ "IMaterialRenderer", "classirr_1_1video_1_1_i_material_renderer.html", "classirr_1_1video_1_1_i_material_renderer" ],
    [ "IMaterialRendererServices", "classirr_1_1video_1_1_i_material_renderer_services.html", "classirr_1_1video_1_1_i_material_renderer_services" ],
    [ "IRenderTarget", "structirr_1_1video_1_1_i_render_target.html", "structirr_1_1video_1_1_i_render_target" ],
    [ "IShaderConstantSetCallBack", "classirr_1_1video_1_1_i_shader_constant_set_call_back.html", "classirr_1_1video_1_1_i_shader_constant_set_call_back" ],
    [ "ITexture", "classirr_1_1video_1_1_i_texture.html", "classirr_1_1video_1_1_i_texture" ],
    [ "IVideoDriver", "classirr_1_1video_1_1_i_video_driver.html", "classirr_1_1video_1_1_i_video_driver" ],
    [ "IVideoModeList", "classirr_1_1video_1_1_i_video_mode_list.html", "classirr_1_1video_1_1_i_video_mode_list" ],
    [ "S3DVertex", "structirr_1_1video_1_1_s3_d_vertex.html", "structirr_1_1video_1_1_s3_d_vertex" ],
    [ "S3DVertex2TCoords", "structirr_1_1video_1_1_s3_d_vertex2_t_coords.html", "structirr_1_1video_1_1_s3_d_vertex2_t_coords" ],
    [ "S3DVertexTangents", "structirr_1_1video_1_1_s3_d_vertex_tangents.html", "structirr_1_1video_1_1_s3_d_vertex_tangents" ],
    [ "SColor", "classirr_1_1video_1_1_s_color.html", "classirr_1_1video_1_1_s_color" ],
    [ "SColorf", "classirr_1_1video_1_1_s_colorf.html", "classirr_1_1video_1_1_s_colorf" ],
    [ "SColorHSL", "classirr_1_1video_1_1_s_color_h_s_l.html", "classirr_1_1video_1_1_s_color_h_s_l" ],
    [ "SExposedVideoData", "structirr_1_1video_1_1_s_exposed_video_data.html", "structirr_1_1video_1_1_s_exposed_video_data" ],
    [ "SLight", "structirr_1_1video_1_1_s_light.html", "structirr_1_1video_1_1_s_light" ],
    [ "SMaterial", "classirr_1_1video_1_1_s_material.html", "classirr_1_1video_1_1_s_material" ],
    [ "SMaterialLayer", "classirr_1_1video_1_1_s_material_layer.html", "classirr_1_1video_1_1_s_material_layer" ],
    [ "SOverrideMaterial", "structirr_1_1video_1_1_s_override_material.html", "structirr_1_1video_1_1_s_override_material" ]
];