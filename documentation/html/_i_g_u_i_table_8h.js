var _i_g_u_i_table_8h =
[
    [ "IGUITable", "classirr_1_1gui_1_1_i_g_u_i_table.html", "classirr_1_1gui_1_1_i_g_u_i_table" ],
    [ "EGUI_COLUMN_ORDERING", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9a", [
      [ "EGCO_NONE", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9aabb887e17695d3074a41d6a153c12ee95", null ],
      [ "EGCO_CUSTOM", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9aaad3913dabb6b4d4ec4c60addee30f499", null ],
      [ "EGCO_ASCENDING", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9aa92639cd2bfdefebfc0a2f6f1c99b75d6", null ],
      [ "EGCO_DESCENDING", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9aa2bae0078aa6d6fbd03c173f97df43c22", null ],
      [ "EGCO_FLIP_ASCENDING_DESCENDING", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9aad7a1f76de200a60bbcc6993973e01851", null ],
      [ "EGCO_COUNT", "_i_g_u_i_table_8h.html#a551e22458ae01a7eeaf87c0fbaaabf9aab445223e250935c860e50aba1895fc5d", null ]
    ] ],
    [ "EGUI_ORDERING_MODE", "_i_g_u_i_table_8h.html#a577bf3aa30c2e3bde9aa3eaa2e4f16d3", [
      [ "EGOM_NONE", "_i_g_u_i_table_8h.html#a577bf3aa30c2e3bde9aa3eaa2e4f16d3a4663b40ac164a811834112141af86961", null ],
      [ "EGOM_ASCENDING", "_i_g_u_i_table_8h.html#a577bf3aa30c2e3bde9aa3eaa2e4f16d3ae68aff56778cbbd1fb5b34e425b7c129", null ],
      [ "EGOM_DESCENDING", "_i_g_u_i_table_8h.html#a577bf3aa30c2e3bde9aa3eaa2e4f16d3ad1b031c00861395b5350ebcca36425b5", null ],
      [ "EGOM_COUNT", "_i_g_u_i_table_8h.html#a577bf3aa30c2e3bde9aa3eaa2e4f16d3a7bba14d09950eea8d3a5e411b0b6498f", null ]
    ] ],
    [ "EGUI_TABLE_DRAW_FLAGS", "_i_g_u_i_table_8h.html#a17b24a3d0d79def6279a9819d3d39ca2", [
      [ "EGTDF_ROWS", "_i_g_u_i_table_8h.html#a17b24a3d0d79def6279a9819d3d39ca2af5c265000877b948a13ad531e118cedc", null ],
      [ "EGTDF_COLUMNS", "_i_g_u_i_table_8h.html#a17b24a3d0d79def6279a9819d3d39ca2a6769323f79851e2df76c5a50d713ad4f", null ],
      [ "EGTDF_ACTIVE_ROW", "_i_g_u_i_table_8h.html#a17b24a3d0d79def6279a9819d3d39ca2a0493420fff5eccac3cc16230ba88711f", null ],
      [ "EGTDF_COUNT", "_i_g_u_i_table_8h.html#a17b24a3d0d79def6279a9819d3d39ca2ae31256f1e9ccb4593bd6b0042e3a2bbc", null ]
    ] ],
    [ "GUIColumnOrderingNames", "_i_g_u_i_table_8h.html#aeb6928fac43f31873c0d707afafa9f47", null ],
    [ "GUIOrderingModeNames", "_i_g_u_i_table_8h.html#a45688313a3589f97fd6fa516eed1aac8", null ]
];