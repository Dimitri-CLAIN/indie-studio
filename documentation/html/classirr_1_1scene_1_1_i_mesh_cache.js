var classirr_1_1scene_1_1_i_mesh_cache =
[
    [ "~IMeshCache", "classirr_1_1scene_1_1_i_mesh_cache.html#a6d1bfbd0bda8a559d85cfd6735bc0667", null ],
    [ "addMesh", "classirr_1_1scene_1_1_i_mesh_cache.html#a2959812a3a393817b1db42761766c49b", null ],
    [ "clear", "classirr_1_1scene_1_1_i_mesh_cache.html#ad92d924e558c3a7504f9154ee29b1569", null ],
    [ "clearUnusedMeshes", "classirr_1_1scene_1_1_i_mesh_cache.html#a9f3e20b8e0f66d59bc454a311d13bbee", null ],
    [ "getMeshByFilename", "classirr_1_1scene_1_1_i_mesh_cache.html#aa3946324e39cc074b1f73e01d57cae70", null ],
    [ "getMeshByIndex", "classirr_1_1scene_1_1_i_mesh_cache.html#a06e7755013445f9bc3d7339fbd009e31", null ],
    [ "getMeshByName", "classirr_1_1scene_1_1_i_mesh_cache.html#a4c93e736bdca8c84d478afc82540d6bb", null ],
    [ "getMeshCount", "classirr_1_1scene_1_1_i_mesh_cache.html#a9dc99e46309a6ef494ef7672c9b49853", null ],
    [ "getMeshFilename", "classirr_1_1scene_1_1_i_mesh_cache.html#adc17a943cd79a94710def8dd7d2de605", null ],
    [ "getMeshFilename", "classirr_1_1scene_1_1_i_mesh_cache.html#afda96c4fb8ab272f3d3688dcaef3abc3", null ],
    [ "getMeshIndex", "classirr_1_1scene_1_1_i_mesh_cache.html#a2b3512bd3ff11d0b290fa5d2d580eb54", null ],
    [ "getMeshName", "classirr_1_1scene_1_1_i_mesh_cache.html#a7271fa1247b8c1198c196dc947b5ede0", null ],
    [ "getMeshName", "classirr_1_1scene_1_1_i_mesh_cache.html#af06efb8fb21f6bba16e52d879b5d3ddd", null ],
    [ "isMeshLoaded", "classirr_1_1scene_1_1_i_mesh_cache.html#a42a13fab5b76ab7142a1d47dac80548b", null ],
    [ "removeMesh", "classirr_1_1scene_1_1_i_mesh_cache.html#aa82078b06fdcaa332b44a59e4027f921", null ],
    [ "renameMesh", "classirr_1_1scene_1_1_i_mesh_cache.html#a4533c81f4f3df112fa106d6fb1118f3b", null ],
    [ "renameMesh", "classirr_1_1scene_1_1_i_mesh_cache.html#a820743b703cdc4362a3dbe6664271bcb", null ],
    [ "setMeshFilename", "classirr_1_1scene_1_1_i_mesh_cache.html#a9b7770a23859ddd045b3c22dfbecbcaf", null ],
    [ "setMeshFilename", "classirr_1_1scene_1_1_i_mesh_cache.html#a5b87031dbfdb70a59c00a1b892b74c3d", null ]
];