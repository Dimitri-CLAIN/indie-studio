var _e_device_types_8h =
[
    [ "E_DEVICE_TYPE", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505", [
      [ "EIDT_WIN32", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505a20fb61ff76bfa2269a5f9e41d50018f1", null ],
      [ "EIDT_WINCE", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505a839d8602c1a118791425d3df0d31ced1", null ],
      [ "EIDT_X11", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505aa084c40bd6cc595378d28182dd74ff80", null ],
      [ "EIDT_OSX", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505a140f80a523981af1487cd774e9a2a656", null ],
      [ "EIDT_SDL", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505ab9cafb4f0108f3fd5da94e886b529979", null ],
      [ "EIDT_FRAMEBUFFER", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505a5516da97d0b6ef1708a3a13cdb157bee", null ],
      [ "EIDT_CONSOLE", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505a54387cdabc602203abce675b027a3ede", null ],
      [ "EIDT_BEST", "_e_device_types_8h.html#ac25d94cf2e1037c7ca18ee79b3bd4505ad00f870da762af833dbab2eacc96ae0b", null ]
    ] ]
];