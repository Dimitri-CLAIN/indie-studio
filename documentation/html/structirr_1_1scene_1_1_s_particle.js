var structirr_1_1scene_1_1_s_particle =
[
    [ "color", "structirr_1_1scene_1_1_s_particle.html#a62816a5bae441b16f9bd50dfba14abc0", null ],
    [ "endTime", "structirr_1_1scene_1_1_s_particle.html#ade0deb0f5ed021cb6aa4df9f906d4b36", null ],
    [ "pos", "structirr_1_1scene_1_1_s_particle.html#a5f8e6fe3a892c8f734389dfcaf01fb05", null ],
    [ "size", "structirr_1_1scene_1_1_s_particle.html#a548dc843e9db5db26e0005a2cb29a609", null ],
    [ "startColor", "structirr_1_1scene_1_1_s_particle.html#a23325221f845c68dd331edb38d90374f", null ],
    [ "startSize", "structirr_1_1scene_1_1_s_particle.html#a753df7de54e742cd7d50ff10154906ee", null ],
    [ "startTime", "structirr_1_1scene_1_1_s_particle.html#aa546b47d4a54889e568a4c067816765c", null ],
    [ "startVector", "structirr_1_1scene_1_1_s_particle.html#a5e8b1dcb8825e467b6ed2d4f48c321e2", null ],
    [ "vector", "structirr_1_1scene_1_1_s_particle.html#afaa96102f7effe71755e54c868e6eccd", null ]
];