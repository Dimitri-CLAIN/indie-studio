var classirr_1_1_i_reference_counted =
[
    [ "IReferenceCounted", "classirr_1_1_i_reference_counted.html#a8411682018e68a2752d4c82675c71040", null ],
    [ "~IReferenceCounted", "classirr_1_1_i_reference_counted.html#a78abc75801cbb13d9db0955b3c07251c", null ],
    [ "drop", "classirr_1_1_i_reference_counted.html#a03856a09355b89d178090c4a5f738543", null ],
    [ "getDebugName", "classirr_1_1_i_reference_counted.html#ad336c6e3c975e4c7911a606c27b894f0", null ],
    [ "getReferenceCount", "classirr_1_1_i_reference_counted.html#ae9836f102c33c68068e74292e0a01819", null ],
    [ "grab", "classirr_1_1_i_reference_counted.html#a396f9cdbe311ada278626477b3c6f0f5", null ],
    [ "setDebugName", "classirr_1_1_i_reference_counted.html#a704c5042d399fe8cd3bdd65a0559002a", null ]
];