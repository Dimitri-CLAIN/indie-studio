var struct_action_component =
[
    [ "action", "struct_action_component.html#ac2da86000264fc0564018956481a2753", [
      [ "NOTHING", "struct_action_component.html#ac2da86000264fc0564018956481a2753a4e5de02bdd955aca2a5a0a787c87f949", null ],
      [ "UP", "struct_action_component.html#ac2da86000264fc0564018956481a2753a13cb55af6b770954a7229e54082c2132", null ],
      [ "DOWN", "struct_action_component.html#ac2da86000264fc0564018956481a2753af6b7263b5e263454ef922ce21d2937d9", null ],
      [ "RIGHT", "struct_action_component.html#ac2da86000264fc0564018956481a2753a22ee78021663527a96d414e5885bdb70", null ],
      [ "LEFT", "struct_action_component.html#ac2da86000264fc0564018956481a2753a93da9b23a2693458a1d9cbe9bd0ac073", null ],
      [ "BOMB", "struct_action_component.html#ac2da86000264fc0564018956481a2753a7e3bbc089c66ffd4de21ad7b91c4d11d", null ]
    ] ],
    [ "ActionComponent", "struct_action_component.html#a2edcd5265ba7ed6a4d736592842102a9", null ],
    [ "~ActionComponent", "struct_action_component.html#a0f46e100e3e6d1d0182c80206690e616", null ],
    [ "changeDirection", "struct_action_component.html#a0c98ff5b62f0496a3e2b0f73a377ef9e", null ],
    [ "_rotation", "struct_action_component.html#a0dfd2f56d8a024c64c1fe5c9b7b34fb5", null ]
];