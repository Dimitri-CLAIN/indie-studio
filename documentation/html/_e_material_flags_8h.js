var _e_material_flags_8h =
[
    [ "E_MATERIAL_FLAG", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3", [
      [ "EMF_WIREFRAME", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3abc620823efed8d6bdbd46c8a0180893a", null ],
      [ "EMF_POINTCLOUD", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a3726bbddc57e6b37b05481b640eefb07", null ],
      [ "EMF_GOURAUD_SHADING", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a8c9d516b266f04179afb232f0b56e51c", null ],
      [ "EMF_LIGHTING", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3acea597a2692b8415486a464a7f954d34", null ],
      [ "EMF_ZBUFFER", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a493bb44efafebb48adab96e31eb029e5", null ],
      [ "EMF_ZWRITE_ENABLE", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a4bc03b7b9dd19e577bf909313ea62510", null ],
      [ "EMF_BACK_FACE_CULLING", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3ae1d176d0ce05ccc5df9e43ce854393bb", null ],
      [ "EMF_FRONT_FACE_CULLING", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a1b3b367405da4f85738c8dbe7647842d", null ],
      [ "EMF_BILINEAR_FILTER", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3afbf2b289d416e70466e4ab05e97b4934", null ],
      [ "EMF_TRILINEAR_FILTER", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a7a2ed21d879b182fbc767a4c20d72eef", null ],
      [ "EMF_ANISOTROPIC_FILTER", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a941c0756b9dc3f987a183a401c6fd4ad", null ],
      [ "EMF_FOG_ENABLE", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a5b898e76a9f5e5cfb9c27bee1fbc38be", null ],
      [ "EMF_NORMALIZE_NORMALS", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a3efe2d4921909a842adfc44dacc74520", null ],
      [ "EMF_TEXTURE_WRAP", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a824f828adf0a1e28c6dcb2f5875371b4", null ],
      [ "EMF_ANTI_ALIASING", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a9f512ba36b9ff14e1743150fb68196b0", null ],
      [ "EMF_COLOR_MASK", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a72ede4598946d81f12aa407fb680fc40", null ],
      [ "EMF_COLOR_MATERIAL", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a8623708e0a7188b1ae04592b7627eb98", null ],
      [ "EMF_USE_MIP_MAPS", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3a932f4ecd30738a527cfadfaa2b693ccf", null ],
      [ "EMF_BLEND_OPERATION", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3ab9cbb5be402278cf0276da84acd3da14", null ],
      [ "EMF_POLYGON_OFFSET", "_e_material_flags_8h.html#a8a3bc00ae8137535b9fbc5f40add70d3abfa9451318b91a44eae6a20b5d91a4f8", null ]
    ] ]
];