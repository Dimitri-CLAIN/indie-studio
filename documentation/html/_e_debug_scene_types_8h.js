var _e_debug_scene_types_8h =
[
    [ "E_DEBUG_SCENE_TYPE", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbe", [
      [ "EDS_OFF", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea25111b15f03bee9a99498737286916dc", null ],
      [ "EDS_BBOX", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea19e56bb3d3b18134fa63e0529629b427", null ],
      [ "EDS_NORMALS", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea2713e470ee18ec9bfe40fdfb502f8b05", null ],
      [ "EDS_SKELETON", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbeaa7664e189b8641ac54cf27f70f6d8144", null ],
      [ "EDS_MESH_WIRE_OVERLAY", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea349b086537ac770f09935af4e31d3f3e", null ],
      [ "EDS_HALF_TRANSPARENCY", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea1def9e1b7d86e286b07a4b7179e6ed85", null ],
      [ "EDS_BBOX_BUFFERS", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea0179a3df80ac09143dfffed0bd9e99d1", null ],
      [ "EDS_BBOX_ALL", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea80f38e42f1b8cf169e83f44092367bfe", null ],
      [ "EDS_FULL", "_e_debug_scene_types_8h.html#a52b664c4c988113735042b168fc32dbea24ffe5e6e99d589b3c80181e7c7dd4e2", null ]
    ] ]
];