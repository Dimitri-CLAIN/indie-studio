var _e_g_u_i_alignment_8h =
[
    [ "EGUI_ALIGNMENT", "_e_g_u_i_alignment_8h.html#a19eb5fb40e67f108cb16aba922ddaa2d", [
      [ "EGUIA_UPPERLEFT", "_e_g_u_i_alignment_8h.html#a19eb5fb40e67f108cb16aba922ddaa2da4bb8a01452727274e18047a872da1809", null ],
      [ "EGUIA_LOWERRIGHT", "_e_g_u_i_alignment_8h.html#a19eb5fb40e67f108cb16aba922ddaa2da48b4d042b2d6cd63b876cef62c9cfb97", null ],
      [ "EGUIA_CENTER", "_e_g_u_i_alignment_8h.html#a19eb5fb40e67f108cb16aba922ddaa2da9da0fb4bcb85d509bdfe018b720e4606", null ],
      [ "EGUIA_SCALE", "_e_g_u_i_alignment_8h.html#a19eb5fb40e67f108cb16aba922ddaa2da0fef10c3356089b2c18ba4e6b3f91246", null ]
    ] ],
    [ "GUIAlignmentNames", "_e_g_u_i_alignment_8h.html#a356f87c7a79af136d7128f4cf2a3ad9a", null ]
];