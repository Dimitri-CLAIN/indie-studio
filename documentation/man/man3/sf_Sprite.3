.TH "sf::Sprite" 3 "Sun Jun 14 2020" "Version v0.00" "OOP_indie_studio_2019" \" -*- nroff -*-
.ad l
.nh
.SH NAME
sf::Sprite \- \fBDrawable\fP representation of a texture, with its own transformations, color, etc\&.  

.SH SYNOPSIS
.br
.PP
.PP
\fC#include <Sprite\&.hpp>\fP
.PP
Inherits \fBsf::Drawable\fP, and \fBsf::Transformable\fP\&.
.SS "Public Member Functions"

.in +1c
.ti -1c
.RI "\fBSprite\fP ()"
.br
.RI "Default constructor\&. "
.ti -1c
.RI "\fBSprite\fP (const \fBTexture\fP &texture)"
.br
.RI "Construct the sprite from a source texture\&. "
.ti -1c
.RI "\fBSprite\fP (const \fBTexture\fP &texture, const \fBIntRect\fP &rectangle)"
.br
.RI "Construct the sprite from a sub-rectangle of a source texture\&. "
.ti -1c
.RI "void \fBsetTexture\fP (const \fBTexture\fP &texture, bool resetRect=false)"
.br
.RI "Change the source texture of the sprite\&. "
.ti -1c
.RI "void \fBsetTextureRect\fP (const \fBIntRect\fP &rectangle)"
.br
.RI "Set the sub-rectangle of the texture that the sprite will display\&. "
.ti -1c
.RI "void \fBsetColor\fP (const \fBColor\fP &color)"
.br
.RI "Set the global color of the sprite\&. "
.ti -1c
.RI "const \fBTexture\fP * \fBgetTexture\fP () const"
.br
.RI "Get the source texture of the sprite\&. "
.ti -1c
.RI "const \fBIntRect\fP & \fBgetTextureRect\fP () const"
.br
.RI "Get the sub-rectangle of the texture displayed by the sprite\&. "
.ti -1c
.RI "const \fBColor\fP & \fBgetColor\fP () const"
.br
.RI "Get the global color of the sprite\&. "
.ti -1c
.RI "\fBFloatRect\fP \fBgetLocalBounds\fP () const"
.br
.RI "Get the local bounding rectangle of the entity\&. "
.ti -1c
.RI "\fBFloatRect\fP \fBgetGlobalBounds\fP () const"
.br
.RI "Get the global bounding rectangle of the entity\&. "
.in -1c
.SS "Additional Inherited Members"
.SH "Detailed Description"
.PP 
\fBDrawable\fP representation of a texture, with its own transformations, color, etc\&. 

\fBsf::Sprite\fP is a drawable class that allows to easily display a texture (or a part of it) on a render target\&.
.PP
It inherits all the functions from \fBsf::Transformable\fP: position, rotation, scale, origin\&. It also adds sprite-specific properties such as the texture to use, the part of it to display, and some convenience functions to change the overall color of the sprite, or to get its bounding rectangle\&.
.PP
\fBsf::Sprite\fP works in combination with the \fBsf::Texture\fP class, which loads and provides the pixel data of a given texture\&.
.PP
The separation of \fBsf::Sprite\fP and \fBsf::Texture\fP allows more flexibility and better performances: indeed a \fBsf::Texture\fP is a heavy resource, and any operation on it is slow (often too slow for real-time applications)\&. On the other side, a \fBsf::Sprite\fP is a lightweight object which can use the pixel data of a \fBsf::Texture\fP and draw it with its own transformation/color/blending attributes\&.
.PP
It is important to note that the \fBsf::Sprite\fP instance doesn't copy the texture that it uses, it only keeps a reference to it\&. Thus, a \fBsf::Texture\fP must not be destroyed while it is used by a \fBsf::Sprite\fP (i\&.e\&. never write a function that uses a local \fBsf::Texture\fP instance for creating a sprite)\&.
.PP
See also the note on coordinates and undistorted rendering in \fBsf::Transformable\fP\&.
.PP
Usage example: 
.PP
.nf
// Declare and load a texture
sf::Texture texture;
texture\&.loadFromFile("texture\&.png");

// Create a sprite
sf::Sprite sprite;
sprite\&.setTexture(texture);
sprite\&.setTextureRect(sf::IntRect(10, 10, 50, 30));
sprite\&.setColor(sf::Color(255, 255, 255, 200));
sprite\&.setPosition(100, 25);

// Draw it
window\&.draw(sprite);

.fi
.PP
.PP
\fBSee also\fP
.RS 4
\fBsf::Texture\fP, \fBsf::Transformable\fP 
.RE
.PP

.PP
Definition at line 47 of file Sprite\&.hpp\&.
.SH "Constructor & Destructor Documentation"
.PP 
.SS "sf::Sprite::Sprite ()"

.PP
Default constructor\&. Creates an empty sprite with no source texture\&. 
.SS "sf::Sprite::Sprite (const \fBTexture\fP & texture)\fC [explicit]\fP"

.PP
Construct the sprite from a source texture\&. 
.PP
\fBParameters\fP
.RS 4
\fItexture\fP Source texture
.RE
.PP
\fBSee also\fP
.RS 4
\fBsetTexture\fP 
.RE
.PP

.SS "sf::Sprite::Sprite (const \fBTexture\fP & texture, const \fBIntRect\fP & rectangle)"

.PP
Construct the sprite from a sub-rectangle of a source texture\&. 
.PP
\fBParameters\fP
.RS 4
\fItexture\fP Source texture 
.br
\fIrectangle\fP Sub-rectangle of the texture to assign to the sprite
.RE
.PP
\fBSee also\fP
.RS 4
\fBsetTexture\fP, \fBsetTextureRect\fP 
.RE
.PP

.SH "Member Function Documentation"
.PP 
.SS "const \fBColor\fP& sf::Sprite::getColor () const"

.PP
Get the global color of the sprite\&. 
.PP
\fBReturns\fP
.RS 4
Global color of the sprite
.RE
.PP
\fBSee also\fP
.RS 4
\fBsetColor\fP 
.RE
.PP

.SS "\fBFloatRect\fP sf::Sprite::getGlobalBounds () const"

.PP
Get the global bounding rectangle of the entity\&. The returned rectangle is in global coordinates, which means that it takes into account the transformations (translation, rotation, scale, \&.\&.\&.) that are applied to the entity\&. In other words, this function returns the bounds of the sprite in the global 2D world's coordinate system\&.
.PP
\fBReturns\fP
.RS 4
Global bounding rectangle of the entity 
.RE
.PP

.SS "\fBFloatRect\fP sf::Sprite::getLocalBounds () const"

.PP
Get the local bounding rectangle of the entity\&. The returned rectangle is in local coordinates, which means that it ignores the transformations (translation, rotation, scale, \&.\&.\&.) that are applied to the entity\&. In other words, this function returns the bounds of the entity in the entity's coordinate system\&.
.PP
\fBReturns\fP
.RS 4
Local bounding rectangle of the entity 
.RE
.PP

.SS "const \fBTexture\fP* sf::Sprite::getTexture () const"

.PP
Get the source texture of the sprite\&. If the sprite has no source texture, a NULL pointer is returned\&. The returned pointer is const, which means that you can't modify the texture when you retrieve it with this function\&.
.PP
\fBReturns\fP
.RS 4
Pointer to the sprite's texture
.RE
.PP
\fBSee also\fP
.RS 4
\fBsetTexture\fP 
.RE
.PP

.SS "const \fBIntRect\fP& sf::Sprite::getTextureRect () const"

.PP
Get the sub-rectangle of the texture displayed by the sprite\&. 
.PP
\fBReturns\fP
.RS 4
\fBTexture\fP rectangle of the sprite
.RE
.PP
\fBSee also\fP
.RS 4
\fBsetTextureRect\fP 
.RE
.PP

.SS "void sf::Sprite::setColor (const \fBColor\fP & color)"

.PP
Set the global color of the sprite\&. This color is modulated (multiplied) with the sprite's texture\&. It can be used to colorize the sprite, or change its global opacity\&. By default, the sprite's color is opaque white\&.
.PP
\fBParameters\fP
.RS 4
\fIcolor\fP New color of the sprite
.RE
.PP
\fBSee also\fP
.RS 4
\fBgetColor\fP 
.RE
.PP

.SS "void sf::Sprite::setTexture (const \fBTexture\fP & texture, bool resetRect = \fCfalse\fP)"

.PP
Change the source texture of the sprite\&. The \fItexture\fP argument refers to a texture that must exist as long as the sprite uses it\&. Indeed, the sprite doesn't store its own copy of the texture, but rather keeps a pointer to the one that you passed to this function\&. If the source texture is destroyed and the sprite tries to use it, the behavior is undefined\&. If \fIresetRect\fP is true, the TextureRect property of the sprite is automatically adjusted to the size of the new texture\&. If it is false, the texture rect is left unchanged\&.
.PP
\fBParameters\fP
.RS 4
\fItexture\fP New texture 
.br
\fIresetRect\fP Should the texture rect be reset to the size of the new texture?
.RE
.PP
\fBSee also\fP
.RS 4
\fBgetTexture\fP, \fBsetTextureRect\fP 
.RE
.PP

.SS "void sf::Sprite::setTextureRect (const \fBIntRect\fP & rectangle)"

.PP
Set the sub-rectangle of the texture that the sprite will display\&. The texture rect is useful when you don't want to display the whole texture, but rather a part of it\&. By default, the texture rect covers the entire texture\&.
.PP
\fBParameters\fP
.RS 4
\fIrectangle\fP Rectangle defining the region of the texture to display
.RE
.PP
\fBSee also\fP
.RS 4
\fBgetTextureRect\fP, \fBsetTexture\fP 
.RE
.PP


.SH "Author"
.PP 
Generated automatically by Doxygen for OOP_indie_studio_2019 from the source code\&.
