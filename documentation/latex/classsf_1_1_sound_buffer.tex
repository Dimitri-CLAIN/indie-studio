\doxysection{sf\+::Sound\+Buffer Class Reference}
\label{classsf_1_1_sound_buffer}\index{sf::SoundBuffer@{sf::SoundBuffer}}


Storage for audio samples defining a sound.  




{\ttfamily \#include $<$Sound\+Buffer.\+hpp$>$}



Inheritance diagram for sf\+::Sound\+Buffer\+:
% FIG 0


Collaboration diagram for sf\+::Sound\+Buffer\+:
% FIG 1
\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Sound\+Buffer} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
\textbf{ Sound\+Buffer} (const \textbf{ Sound\+Buffer} \&copy)
\begin{DoxyCompactList}\small\item\em Copy constructor. \end{DoxyCompactList}\item 
\textbf{ $\sim$\+Sound\+Buffer} ()
\begin{DoxyCompactList}\small\item\em Destructor. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+File} (const std\+::string \&filename)
\begin{DoxyCompactList}\small\item\em Load the sound buffer from a file. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+Memory} (const void $\ast$data, std\+::size\+\_\+t size\+In\+Bytes)
\begin{DoxyCompactList}\small\item\em Load the sound buffer from a file in memory. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+Stream} (\textbf{ Input\+Stream} \&stream)
\begin{DoxyCompactList}\small\item\em Load the sound buffer from a custom stream. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+Samples} (const \textbf{ Int16} $\ast$samples, \textbf{ Uint64} sample\+Count, unsigned int channel\+Count, unsigned int sample\+Rate)
\begin{DoxyCompactList}\small\item\em Load the sound buffer from an array of audio samples. \end{DoxyCompactList}\item 
bool \textbf{ save\+To\+File} (const std\+::string \&filename) const
\begin{DoxyCompactList}\small\item\em Save the sound buffer to an audio file. \end{DoxyCompactList}\item 
const \textbf{ Int16} $\ast$ \textbf{ get\+Samples} () const
\begin{DoxyCompactList}\small\item\em Get the array of audio samples stored in the buffer. \end{DoxyCompactList}\item 
\textbf{ Uint64} \textbf{ get\+Sample\+Count} () const
\begin{DoxyCompactList}\small\item\em Get the number of samples stored in the buffer. \end{DoxyCompactList}\item 
unsigned int \textbf{ get\+Sample\+Rate} () const
\begin{DoxyCompactList}\small\item\em Get the sample rate of the sound. \end{DoxyCompactList}\item 
unsigned int \textbf{ get\+Channel\+Count} () const
\begin{DoxyCompactList}\small\item\em Get the number of channels used by the sound. \end{DoxyCompactList}\item 
\textbf{ Time} \textbf{ get\+Duration} () const
\begin{DoxyCompactList}\small\item\em Get the total duration of the sound. \end{DoxyCompactList}\item 
\textbf{ Sound\+Buffer} \& \textbf{ operator=} (const \textbf{ Sound\+Buffer} \&right)
\begin{DoxyCompactList}\small\item\em Overload of assignment operator. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Friends}
\begin{DoxyCompactItemize}
\item 
class \textbf{ Sound}
\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
Storage for audio samples defining a sound. 

A sound buffer holds the data of a sound, which is an array of audio samples. A sample is a 16 bits signed integer that defines the amplitude of the sound at a given time. The sound is then reconstituted by playing these samples at a high rate (for example, 44100 samples per second is the standard rate used for playing C\+Ds). In short, audio samples are like texture pixels, and a \doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer} is similar to a \doxyref{sf\+::\+Texture}{p.}{classsf_1_1_texture}.

A sound buffer can be loaded from a file (see \doxyref{load\+From\+File()}{p.}{classsf_1_1_sound_buffer_a2be6a8025c97eb622a7dff6cf2594394} for the complete list of supported formats), from memory, from a custom stream (see \doxyref{sf\+::\+Input\+Stream}{p.}{classsf_1_1_input_stream}) or directly from an array of samples. It can also be saved back to a file.

\doxyref{Sound}{p.}{classsf_1_1_sound} buffers alone are not very useful\+: they hold the audio data but cannot be played. To do so, you need to use the \doxyref{sf\+::\+Sound}{p.}{classsf_1_1_sound} class, which provides functions to play/pause/stop the sound as well as changing the way it is outputted (volume, pitch, 3D position, ...). This separation allows more flexibility and better performances\+: indeed a \doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer} is a heavy resource, and any operation on it is slow (often too slow for real-\/time applications). On the other side, a \doxyref{sf\+::\+Sound}{p.}{classsf_1_1_sound} is a lightweight object, which can use the audio data of a sound buffer and change the way it is played without actually modifying that data. Note that it is also possible to bind several \doxyref{sf\+::\+Sound}{p.}{classsf_1_1_sound} instances to the same \doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer}.

It is important to note that the \doxyref{sf\+::\+Sound}{p.}{classsf_1_1_sound} instance doesn\textquotesingle{}t copy the buffer that it uses, it only keeps a reference to it. Thus, a \doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer} must not be destructed while it is used by a \doxyref{sf\+::\+Sound}{p.}{classsf_1_1_sound} (i.\+e. never write a function that uses a local \doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer} instance for loading a sound).

Usage example\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{comment}{// Declare a new sound buffer}}
\DoxyCodeLine{sf::SoundBuffer buffer;}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Load it from a file}}
\DoxyCodeLine{\textcolor{keywordflow}{if} (!buffer.loadFromFile(\textcolor{stringliteral}{"sound.wav"}))}
\DoxyCodeLine{\{}
\DoxyCodeLine{    \textcolor{comment}{// error...}}
\DoxyCodeLine{\}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create a sound source and bind it to the buffer}}
\DoxyCodeLine{sf::Sound sound1;}
\DoxyCodeLine{sound1.setBuffer(buffer);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Play the sound}}
\DoxyCodeLine{sound1.play();}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create another sound source bound to the same buffer}}
\DoxyCodeLine{sf::Sound sound2;}
\DoxyCodeLine{sound2.setBuffer(buffer);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Play it with a higher pitch -\/-\/ the first sound remains unchanged}}
\DoxyCodeLine{sound2.setPitch(2);}
\DoxyCodeLine{sound2.play();}
\end{DoxyCode}


\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Sound}{p.}{classsf_1_1_sound}, \doxyref{sf\+::\+Sound\+Buffer\+Recorder}{p.}{classsf_1_1_sound_buffer_recorder} 
\end{DoxySeeAlso}


Definition at line 49 of file Sound\+Buffer.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_sound_buffer_a0cabfbfe19b831bf7d5c9592d92ef233}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!SoundBuffer@{SoundBuffer}}
\index{SoundBuffer@{SoundBuffer}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{SoundBuffer()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily sf\+::\+Sound\+Buffer\+::\+Sound\+Buffer (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Default constructor. 

\mbox{\label{classsf_1_1_sound_buffer_aaf000fc741ff27015907e8588263f4a6}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!SoundBuffer@{SoundBuffer}}
\index{SoundBuffer@{SoundBuffer}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{SoundBuffer()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily sf\+::\+Sound\+Buffer\+::\+Sound\+Buffer (\begin{DoxyParamCaption}\item[{const \textbf{ Sound\+Buffer} \&}]{copy }\end{DoxyParamCaption})}



Copy constructor. 


\begin{DoxyParams}{Parameters}
{\em copy} & Instance to copy \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_sound_buffer_aea240161724ffba74a0d6a9e277d3cd5}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!````~SoundBuffer@{$\sim$SoundBuffer}}
\index{````~SoundBuffer@{$\sim$SoundBuffer}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{$\sim$SoundBuffer()}
{\footnotesize\ttfamily sf\+::\+Sound\+Buffer\+::$\sim$\+Sound\+Buffer (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Destructor. 



\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_sound_buffer_a127707b831d875ed790eef1aa2b9fcc3}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!getChannelCount@{getChannelCount}}
\index{getChannelCount@{getChannelCount}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{getChannelCount()}
{\footnotesize\ttfamily unsigned int sf\+::\+Sound\+Buffer\+::get\+Channel\+Count (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the number of channels used by the sound. 

If the sound is mono then the number of channels will be 1, 2 for stereo, etc.

\begin{DoxyReturn}{Returns}
Number of channels
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Sample\+Rate}{p.}{classsf_1_1_sound_buffer_a2c2cf0078ce0549246ecc4a1646212b4}, \doxyref{get\+Duration}{p.}{classsf_1_1_sound_buffer_a280a581d9b360fd16121714c51fc8261} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 2
\mbox{\label{classsf_1_1_sound_buffer_a280a581d9b360fd16121714c51fc8261}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!getDuration@{getDuration}}
\index{getDuration@{getDuration}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{getDuration()}
{\footnotesize\ttfamily \textbf{ Time} sf\+::\+Sound\+Buffer\+::get\+Duration (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the total duration of the sound. 

\begin{DoxyReturn}{Returns}
\doxyref{Sound}{p.}{classsf_1_1_sound} duration
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Sample\+Rate}{p.}{classsf_1_1_sound_buffer_a2c2cf0078ce0549246ecc4a1646212b4}, \doxyref{get\+Channel\+Count}{p.}{classsf_1_1_sound_buffer_a127707b831d875ed790eef1aa2b9fcc3} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 3
\mbox{\label{classsf_1_1_sound_buffer_aebe2a4bdbfbd9249353748da3f6a4fa1}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!getSampleCount@{getSampleCount}}
\index{getSampleCount@{getSampleCount}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{getSampleCount()}
{\footnotesize\ttfamily \textbf{ Uint64} sf\+::\+Sound\+Buffer\+::get\+Sample\+Count (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the number of samples stored in the buffer. 

The array of samples can be accessed with the \doxyref{get\+Samples()}{p.}{classsf_1_1_sound_buffer_ab9b2525a8da64cb266ba728aff7adecb} function.

\begin{DoxyReturn}{Returns}
Number of samples
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Samples}{p.}{classsf_1_1_sound_buffer_ab9b2525a8da64cb266ba728aff7adecb} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_buffer_a2c2cf0078ce0549246ecc4a1646212b4}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!getSampleRate@{getSampleRate}}
\index{getSampleRate@{getSampleRate}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{getSampleRate()}
{\footnotesize\ttfamily unsigned int sf\+::\+Sound\+Buffer\+::get\+Sample\+Rate (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the sample rate of the sound. 

The sample rate is the number of samples played per second. The higher, the better the quality (for example, 44100 samples/s is CD quality).

\begin{DoxyReturn}{Returns}
Sample rate (number of samples per second)
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Channel\+Count}{p.}{classsf_1_1_sound_buffer_a127707b831d875ed790eef1aa2b9fcc3}, \doxyref{get\+Duration}{p.}{classsf_1_1_sound_buffer_a280a581d9b360fd16121714c51fc8261} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 4
\mbox{\label{classsf_1_1_sound_buffer_ab9b2525a8da64cb266ba728aff7adecb}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!getSamples@{getSamples}}
\index{getSamples@{getSamples}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{getSamples()}
{\footnotesize\ttfamily const \textbf{ Int16}$\ast$ sf\+::\+Sound\+Buffer\+::get\+Samples (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the array of audio samples stored in the buffer. 

The format of the returned samples is 16 bits signed integer (\doxyref{sf\+::\+Int16}{p.}{namespacesf_a3c8e10435e2a310a7741755e66b5c94e}). The total number of samples in this array is given by the \doxyref{get\+Sample\+Count()}{p.}{classsf_1_1_sound_buffer_aebe2a4bdbfbd9249353748da3f6a4fa1} function.

\begin{DoxyReturn}{Returns}
Read-\/only pointer to the array of sound samples
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Sample\+Count}{p.}{classsf_1_1_sound_buffer_aebe2a4bdbfbd9249353748da3f6a4fa1} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_buffer_a2be6a8025c97eb622a7dff6cf2594394}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!loadFromFile@{loadFromFile}}
\index{loadFromFile@{loadFromFile}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{loadFromFile()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Buffer\+::load\+From\+File (\begin{DoxyParamCaption}\item[{const std\+::string \&}]{filename }\end{DoxyParamCaption})}



Load the sound buffer from a file. 

See the documentation of \doxyref{sf\+::\+Input\+Sound\+File}{p.}{classsf_1_1_input_sound_file} for the list of supported formats.


\begin{DoxyParams}{Parameters}
{\em filename} & Path of the sound file to load\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading succeeded, false if it failed
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+Memory}{p.}{classsf_1_1_sound_buffer_af8cfa5599739a7edae69c5cba273d33f}, \doxyref{load\+From\+Stream}{p.}{classsf_1_1_sound_buffer_ad292156b1e01f6dabd4c0c277d5e079e}, \doxyref{load\+From\+Samples}{p.}{classsf_1_1_sound_buffer_a42d51ce4bb3b60c7ea06f63c273fd063}, \doxyref{save\+To\+File}{p.}{classsf_1_1_sound_buffer_aade64260c6375580a085314a30be007e} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 5
\mbox{\label{classsf_1_1_sound_buffer_af8cfa5599739a7edae69c5cba273d33f}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!loadFromMemory@{loadFromMemory}}
\index{loadFromMemory@{loadFromMemory}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{loadFromMemory()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Buffer\+::load\+From\+Memory (\begin{DoxyParamCaption}\item[{const void $\ast$}]{data,  }\item[{std\+::size\+\_\+t}]{size\+In\+Bytes }\end{DoxyParamCaption})}



Load the sound buffer from a file in memory. 

See the documentation of \doxyref{sf\+::\+Input\+Sound\+File}{p.}{classsf_1_1_input_sound_file} for the list of supported formats.


\begin{DoxyParams}{Parameters}
{\em data} & Pointer to the file data in memory \\
\hline
{\em size\+In\+Bytes} & Size of the data to load, in bytes\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading succeeded, false if it failed
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+File}{p.}{classsf_1_1_sound_buffer_a2be6a8025c97eb622a7dff6cf2594394}, \doxyref{load\+From\+Stream}{p.}{classsf_1_1_sound_buffer_ad292156b1e01f6dabd4c0c277d5e079e}, \doxyref{load\+From\+Samples}{p.}{classsf_1_1_sound_buffer_a42d51ce4bb3b60c7ea06f63c273fd063} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_buffer_a42d51ce4bb3b60c7ea06f63c273fd063}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!loadFromSamples@{loadFromSamples}}
\index{loadFromSamples@{loadFromSamples}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{loadFromSamples()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Buffer\+::load\+From\+Samples (\begin{DoxyParamCaption}\item[{const \textbf{ Int16} $\ast$}]{samples,  }\item[{\textbf{ Uint64}}]{sample\+Count,  }\item[{unsigned int}]{channel\+Count,  }\item[{unsigned int}]{sample\+Rate }\end{DoxyParamCaption})}



Load the sound buffer from an array of audio samples. 

The assumed format of the audio samples is 16 bits signed integer (\doxyref{sf\+::\+Int16}{p.}{namespacesf_a3c8e10435e2a310a7741755e66b5c94e}).


\begin{DoxyParams}{Parameters}
{\em samples} & Pointer to the array of samples in memory \\
\hline
{\em sample\+Count} & Number of samples in the array \\
\hline
{\em channel\+Count} & Number of channels (1 = mono, 2 = stereo, ...) \\
\hline
{\em sample\+Rate} & Sample rate (number of samples to play per second)\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading succeeded, false if it failed
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+File}{p.}{classsf_1_1_sound_buffer_a2be6a8025c97eb622a7dff6cf2594394}, \doxyref{load\+From\+Memory}{p.}{classsf_1_1_sound_buffer_af8cfa5599739a7edae69c5cba273d33f}, \doxyref{save\+To\+File}{p.}{classsf_1_1_sound_buffer_aade64260c6375580a085314a30be007e} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_buffer_ad292156b1e01f6dabd4c0c277d5e079e}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!loadFromStream@{loadFromStream}}
\index{loadFromStream@{loadFromStream}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{loadFromStream()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Buffer\+::load\+From\+Stream (\begin{DoxyParamCaption}\item[{\textbf{ Input\+Stream} \&}]{stream }\end{DoxyParamCaption})}



Load the sound buffer from a custom stream. 

See the documentation of \doxyref{sf\+::\+Input\+Sound\+File}{p.}{classsf_1_1_input_sound_file} for the list of supported formats.


\begin{DoxyParams}{Parameters}
{\em stream} & Source stream to read from\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading succeeded, false if it failed
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+File}{p.}{classsf_1_1_sound_buffer_a2be6a8025c97eb622a7dff6cf2594394}, \doxyref{load\+From\+Memory}{p.}{classsf_1_1_sound_buffer_af8cfa5599739a7edae69c5cba273d33f}, \doxyref{load\+From\+Samples}{p.}{classsf_1_1_sound_buffer_a42d51ce4bb3b60c7ea06f63c273fd063} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_buffer_adcc786b60bbd95be1551368fafd274a7}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!operator=@{operator=}}
\index{operator=@{operator=}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{operator=()}
{\footnotesize\ttfamily \textbf{ Sound\+Buffer}\& sf\+::\+Sound\+Buffer\+::operator= (\begin{DoxyParamCaption}\item[{const \textbf{ Sound\+Buffer} \&}]{right }\end{DoxyParamCaption})}



Overload of assignment operator. 


\begin{DoxyParams}{Parameters}
{\em right} & Instance to assign\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Reference to self 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_sound_buffer_aade64260c6375580a085314a30be007e}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!saveToFile@{saveToFile}}
\index{saveToFile@{saveToFile}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{saveToFile()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Buffer\+::save\+To\+File (\begin{DoxyParamCaption}\item[{const std\+::string \&}]{filename }\end{DoxyParamCaption}) const}



Save the sound buffer to an audio file. 

See the documentation of \doxyref{sf\+::\+Output\+Sound\+File}{p.}{classsf_1_1_output_sound_file} for the list of supported formats.


\begin{DoxyParams}{Parameters}
{\em filename} & Path of the sound file to write\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if saving succeeded, false if it failed
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+File}{p.}{classsf_1_1_sound_buffer_a2be6a8025c97eb622a7dff6cf2594394}, \doxyref{load\+From\+Memory}{p.}{classsf_1_1_sound_buffer_af8cfa5599739a7edae69c5cba273d33f}, \doxyref{load\+From\+Samples}{p.}{classsf_1_1_sound_buffer_a42d51ce4bb3b60c7ea06f63c273fd063} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 6


\doxysubsection{Friends And Related Function Documentation}
\mbox{\label{classsf_1_1_sound_buffer_a50914f77c7cf4fb97616c898c5291f4b}} 
\index{sf::SoundBuffer@{sf::SoundBuffer}!Sound@{Sound}}
\index{Sound@{Sound}!sf::SoundBuffer@{sf::SoundBuffer}}
\doxysubsubsection{Sound}
{\footnotesize\ttfamily friend class \textbf{ Sound}\hspace{0.3cm}{\ttfamily [friend]}}



Definition at line 228 of file Sound\+Buffer.\+hpp.



The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Audio/\textbf{ Sound\+Buffer.\+hpp}\end{DoxyCompactItemize}
