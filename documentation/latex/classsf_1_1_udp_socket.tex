\doxysection{sf\+::Udp\+Socket Class Reference}
\label{classsf_1_1_udp_socket}\index{sf::UdpSocket@{sf::UdpSocket}}


Specialized socket using the U\+DP protocol.  




{\ttfamily \#include $<$Udp\+Socket.\+hpp$>$}



Inheritance diagram for sf\+::Udp\+Socket\+:
% FIG 0


Collaboration diagram for sf\+::Udp\+Socket\+:
% FIG 1
\doxysubsection*{Public Types}
\begin{DoxyCompactItemize}
\item 
enum \{ \textbf{ Max\+Datagram\+Size} = 65507
 \}
\end{DoxyCompactItemize}
\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Udp\+Socket} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
unsigned short \textbf{ get\+Local\+Port} () const
\begin{DoxyCompactList}\small\item\em Get the port to which the socket is bound locally. \end{DoxyCompactList}\item 
\textbf{ Status} \textbf{ bind} (unsigned short port, const \textbf{ Ip\+Address} \&address=\textbf{ Ip\+Address\+::\+Any})
\begin{DoxyCompactList}\small\item\em Bind the socket to a specific port. \end{DoxyCompactList}\item 
void \textbf{ unbind} ()
\begin{DoxyCompactList}\small\item\em Unbind the socket from the local port to which it is bound. \end{DoxyCompactList}\item 
\textbf{ Status} \textbf{ send} (const void $\ast$data, std\+::size\+\_\+t size, const \textbf{ Ip\+Address} \&remote\+Address, unsigned short remote\+Port)
\begin{DoxyCompactList}\small\item\em Send raw data to a remote peer. \end{DoxyCompactList}\item 
\textbf{ Status} \textbf{ receive} (void $\ast$data, std\+::size\+\_\+t size, std\+::size\+\_\+t \&received, \textbf{ Ip\+Address} \&remote\+Address, unsigned short \&remote\+Port)
\begin{DoxyCompactList}\small\item\em Receive raw data from a remote peer. \end{DoxyCompactList}\item 
\textbf{ Status} \textbf{ send} (\textbf{ Packet} \&packet, const \textbf{ Ip\+Address} \&remote\+Address, unsigned short remote\+Port)
\begin{DoxyCompactList}\small\item\em Send a formatted packet of data to a remote peer. \end{DoxyCompactList}\item 
\textbf{ Status} \textbf{ receive} (\textbf{ Packet} \&packet, \textbf{ Ip\+Address} \&remote\+Address, unsigned short \&remote\+Port)
\begin{DoxyCompactList}\small\item\em Receive a formatted packet of data from a remote peer. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Additional Inherited Members}


\doxysubsection{Detailed Description}
Specialized socket using the U\+DP protocol. 

A U\+DP socket is a connectionless socket. Instead of connecting once to a remote host, like T\+CP sockets, it can send to and receive from any host at any time.

It is a datagram protocol\+: bounded blocks of data (datagrams) are transfered over the network rather than a continuous stream of data (T\+CP). Therefore, one call to send will always match one call to receive (if the datagram is not lost), with the same data that was sent.

The U\+DP protocol is lightweight but unreliable. Unreliable means that datagrams may be duplicated, be lost or arrive reordered. However, if a datagram arrives, its data is guaranteed to be valid.

U\+DP is generally used for real-\/time communication (audio or video streaming, real-\/time games, etc.) where speed is crucial and lost data doesn\textquotesingle{}t matter much.

Sending and receiving data can use either the low-\/level or the high-\/level functions. The low-\/level functions process a raw sequence of bytes, whereas the high-\/level interface uses packets (see \doxyref{sf\+::\+Packet}{p.}{classsf_1_1_packet}), which are easier to use and provide more safety regarding the data that is exchanged. You can look at the \doxyref{sf\+::\+Packet}{p.}{classsf_1_1_packet} class to get more details about how they work.

It is important to note that \doxyref{Udp\+Socket}{p.}{classsf_1_1_udp_socket} is unable to send datagrams bigger than Max\+Datagram\+Size. In this case, it returns an error and doesn\textquotesingle{}t send anything. This applies to both raw data and packets. Indeed, even packets are unable to split and recompose data, due to the unreliability of the protocol (dropped, mixed or duplicated datagrams may lead to a big mess when trying to recompose a packet).

If the socket is bound to a port, it is automatically unbound from it when the socket is destroyed. However, you can unbind the socket explicitly with the Unbind function if necessary, to stop receiving messages or make the port available for other sockets.

Usage example\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{comment}{// -\/-\/-\/-\/-\/ The client -\/-\/-\/-\/-\/}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create a socket and bind it to the port 55001}}
\DoxyCodeLine{sf::UdpSocket socket;}
\DoxyCodeLine{socket.bind(55001);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Send a message to 192.168.1.50 on port 55002}}
\DoxyCodeLine{std::string message = \textcolor{stringliteral}{"Hi, I am "} + sf::IpAddress::getLocalAddress().toString();}
\DoxyCodeLine{socket.send(message.c\_str(), message.size() + 1, \textcolor{stringliteral}{"192.168.1.50"}, 55002);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Receive an answer (most likely from 192.168.1.50, but could be anyone else)}}
\DoxyCodeLine{\textcolor{keywordtype}{char} buffer[1024];}
\DoxyCodeLine{std::size\_t received = 0;}
\DoxyCodeLine{sf::IpAddress sender;}
\DoxyCodeLine{\textcolor{keywordtype}{unsigned} \textcolor{keywordtype}{short} port;}
\DoxyCodeLine{socket.receive(buffer, \textcolor{keyword}{sizeof}(buffer), received, sender, port);}
\DoxyCodeLine{std::cout << sender.ToString() << \textcolor{stringliteral}{" said: "} << buffer << std::endl;}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// -\/-\/-\/-\/-\/ The server -\/-\/-\/-\/-\/}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create a socket and bind it to the port 55002}}
\DoxyCodeLine{sf::UdpSocket socket;}
\DoxyCodeLine{socket.bind(55002);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Receive a message from anyone}}
\DoxyCodeLine{\textcolor{keywordtype}{char} buffer[1024];}
\DoxyCodeLine{std::size\_t received = 0;}
\DoxyCodeLine{sf::IpAddress sender;}
\DoxyCodeLine{\textcolor{keywordtype}{unsigned} \textcolor{keywordtype}{short} port;}
\DoxyCodeLine{socket.receive(buffer, \textcolor{keyword}{sizeof}(buffer), received, sender, port);}
\DoxyCodeLine{std::cout << sender.ToString() << \textcolor{stringliteral}{" said: "} << buffer << std::endl;}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Send an answer}}
\DoxyCodeLine{std::string message = \textcolor{stringliteral}{"Welcome "} + sender.toString();}
\DoxyCodeLine{socket.send(message.c\_str(), message.size() + 1, sender, port);}
\end{DoxyCode}


\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Socket}{p.}{classsf_1_1_socket}, \doxyref{sf\+::\+Tcp\+Socket}{p.}{classsf_1_1_tcp_socket}, \doxyref{sf\+::\+Packet}{p.}{classsf_1_1_packet} 
\end{DoxySeeAlso}


Definition at line 45 of file Udp\+Socket.\+hpp.



\doxysubsection{Member Enumeration Documentation}
\mbox{\label{classsf_1_1_udp_socket_a919c4e3d9b027cb4021d39fd913956b2}} 
\doxysubsubsection{anonymous enum}
{\footnotesize\ttfamily anonymous enum}

\begin{DoxyEnumFields}{Enumerator}
\raisebox{\heightof{T}}[0pt][0pt]{\index{MaxDatagramSize@{MaxDatagramSize}!sf::UdpSocket@{sf::UdpSocket}}\index{sf::UdpSocket@{sf::UdpSocket}!MaxDatagramSize@{MaxDatagramSize}}}\mbox{\label{classsf_1_1_udp_socket_a919c4e3d9b027cb4021d39fd913956b2a728a7d33027bee0d65f70f964dd9c9eb}} 
Max\+Datagram\+Size&The maximum number of bytes that can be sent in a single U\+DP datagram. \\
\hline

\end{DoxyEnumFields}


Definition at line 52 of file Udp\+Socket.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_udp_socket_abb10725e26dee9d3a8165fe87ffb71bb}} 
\index{sf::UdpSocket@{sf::UdpSocket}!UdpSocket@{UdpSocket}}
\index{UdpSocket@{UdpSocket}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{UdpSocket()}
{\footnotesize\ttfamily sf\+::\+Udp\+Socket\+::\+Udp\+Socket (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Default constructor. 



\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_udp_socket_ad764c3d06d90b4714dcc97a0d1647bcc}} 
\index{sf::UdpSocket@{sf::UdpSocket}!bind@{bind}}
\index{bind@{bind}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{bind()}
{\footnotesize\ttfamily \textbf{ Status} sf\+::\+Udp\+Socket\+::bind (\begin{DoxyParamCaption}\item[{unsigned short}]{port,  }\item[{const \textbf{ Ip\+Address} \&}]{address = {\ttfamily \textbf{ Ip\+Address\+::\+Any}} }\end{DoxyParamCaption})}



Bind the socket to a specific port. 

Binding the socket to a port is necessary for being able to receive data on that port. You can use the special value \doxyref{Socket\+::\+Any\+Port}{p.}{classsf_1_1_socket_a6d14b53c60d28916ae2bcdb90fc57278a5a3c30fd128895403afc11076f461b19} to tell the system to automatically pick an available port, and then call get\+Local\+Port to retrieve the chosen port.

Since the socket can only be bound to a single port at any given moment, if it is already bound when this function is called, it will be unbound from the previous port before being bound to the new one.


\begin{DoxyParams}{Parameters}
{\em port} & Port to bind the socket to \\
\hline
{\em address} & Address of the interface to bind to\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Status code
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{unbind}{p.}{classsf_1_1_udp_socket_a2c4abb8102a1bd31f51fcfe7f15427a3}, \doxyref{get\+Local\+Port}{p.}{classsf_1_1_udp_socket_a5c03644b3da34bb763bce93e758c938e} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 2
\mbox{\label{classsf_1_1_udp_socket_a5c03644b3da34bb763bce93e758c938e}} 
\index{sf::UdpSocket@{sf::UdpSocket}!getLocalPort@{getLocalPort}}
\index{getLocalPort@{getLocalPort}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{getLocalPort()}
{\footnotesize\ttfamily unsigned short sf\+::\+Udp\+Socket\+::get\+Local\+Port (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the port to which the socket is bound locally. 

If the socket is not bound to a port, this function returns 0.

\begin{DoxyReturn}{Returns}
Port to which the socket is bound
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{bind}{p.}{classsf_1_1_udp_socket_ad764c3d06d90b4714dcc97a0d1647bcc} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_udp_socket_afdd5c655d00c96222d5b477fc057a22b}} 
\index{sf::UdpSocket@{sf::UdpSocket}!receive@{receive}}
\index{receive@{receive}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{receive()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily \textbf{ Status} sf\+::\+Udp\+Socket\+::receive (\begin{DoxyParamCaption}\item[{\textbf{ Packet} \&}]{packet,  }\item[{\textbf{ Ip\+Address} \&}]{remote\+Address,  }\item[{unsigned short \&}]{remote\+Port }\end{DoxyParamCaption})}



Receive a formatted packet of data from a remote peer. 

In blocking mode, this function will wait until the whole packet has been received.


\begin{DoxyParams}{Parameters}
{\em packet} & \doxyref{Packet}{p.}{classsf_1_1_packet} to fill with the received data \\
\hline
{\em remote\+Address} & Address of the peer that sent the data \\
\hline
{\em remote\+Port} & Port of the peer that sent the data\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Status code
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{send}{p.}{classsf_1_1_udp_socket_a664ab8f26f37c21cc4de1b847c2efcca} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_udp_socket_ade9ca0f7ed7919136917b0b997a9833a}} 
\index{sf::UdpSocket@{sf::UdpSocket}!receive@{receive}}
\index{receive@{receive}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{receive()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily \textbf{ Status} sf\+::\+Udp\+Socket\+::receive (\begin{DoxyParamCaption}\item[{void $\ast$}]{data,  }\item[{std\+::size\+\_\+t}]{size,  }\item[{std\+::size\+\_\+t \&}]{received,  }\item[{\textbf{ Ip\+Address} \&}]{remote\+Address,  }\item[{unsigned short \&}]{remote\+Port }\end{DoxyParamCaption})}



Receive raw data from a remote peer. 

In blocking mode, this function will wait until some bytes are actually received. Be careful to use a buffer which is large enough for the data that you intend to receive, if it is too small then an error will be returned and {\itshape all} the data will be lost.


\begin{DoxyParams}{Parameters}
{\em data} & Pointer to the array to fill with the received bytes \\
\hline
{\em size} & Maximum number of bytes that can be received \\
\hline
{\em received} & This variable is filled with the actual number of bytes received \\
\hline
{\em remote\+Address} & Address of the peer that sent the data \\
\hline
{\em remote\+Port} & Port of the peer that sent the data\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Status code
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{send}{p.}{classsf_1_1_udp_socket_a664ab8f26f37c21cc4de1b847c2efcca} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 3
\mbox{\label{classsf_1_1_udp_socket_a664ab8f26f37c21cc4de1b847c2efcca}} 
\index{sf::UdpSocket@{sf::UdpSocket}!send@{send}}
\index{send@{send}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{send()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily \textbf{ Status} sf\+::\+Udp\+Socket\+::send (\begin{DoxyParamCaption}\item[{const void $\ast$}]{data,  }\item[{std\+::size\+\_\+t}]{size,  }\item[{const \textbf{ Ip\+Address} \&}]{remote\+Address,  }\item[{unsigned short}]{remote\+Port }\end{DoxyParamCaption})}



Send raw data to a remote peer. 

Make sure that {\itshape size} is not greater than \doxyref{Udp\+Socket\+::\+Max\+Datagram\+Size}{p.}{classsf_1_1_udp_socket_a919c4e3d9b027cb4021d39fd913956b2a728a7d33027bee0d65f70f964dd9c9eb}, otherwise this function will fail and no data will be sent.


\begin{DoxyParams}{Parameters}
{\em data} & Pointer to the sequence of bytes to send \\
\hline
{\em size} & Number of bytes to send \\
\hline
{\em remote\+Address} & Address of the receiver \\
\hline
{\em remote\+Port} & Port of the receiver to send the data to\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Status code
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{receive}{p.}{classsf_1_1_udp_socket_ade9ca0f7ed7919136917b0b997a9833a} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 4
\mbox{\label{classsf_1_1_udp_socket_a48969a62c80d40fd74293a740798e435}} 
\index{sf::UdpSocket@{sf::UdpSocket}!send@{send}}
\index{send@{send}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{send()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily \textbf{ Status} sf\+::\+Udp\+Socket\+::send (\begin{DoxyParamCaption}\item[{\textbf{ Packet} \&}]{packet,  }\item[{const \textbf{ Ip\+Address} \&}]{remote\+Address,  }\item[{unsigned short}]{remote\+Port }\end{DoxyParamCaption})}



Send a formatted packet of data to a remote peer. 

Make sure that the packet size is not greater than \doxyref{Udp\+Socket\+::\+Max\+Datagram\+Size}{p.}{classsf_1_1_udp_socket_a919c4e3d9b027cb4021d39fd913956b2a728a7d33027bee0d65f70f964dd9c9eb}, otherwise this function will fail and no data will be sent.


\begin{DoxyParams}{Parameters}
{\em packet} & \doxyref{Packet}{p.}{classsf_1_1_packet} to send \\
\hline
{\em remote\+Address} & Address of the receiver \\
\hline
{\em remote\+Port} & Port of the receiver to send the data to\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Status code
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{receive}{p.}{classsf_1_1_udp_socket_ade9ca0f7ed7919136917b0b997a9833a} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_udp_socket_a2c4abb8102a1bd31f51fcfe7f15427a3}} 
\index{sf::UdpSocket@{sf::UdpSocket}!unbind@{unbind}}
\index{unbind@{unbind}!sf::UdpSocket@{sf::UdpSocket}}
\doxysubsubsection{unbind()}
{\footnotesize\ttfamily void sf\+::\+Udp\+Socket\+::unbind (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Unbind the socket from the local port to which it is bound. 

The port that the socket was previously bound to is immediately made available to the operating system after this function is called. This means that a subsequent call to \doxyref{bind()}{p.}{classsf_1_1_udp_socket_ad764c3d06d90b4714dcc97a0d1647bcc} will be able to re-\/bind the port if no other process has done so in the mean time. If the socket is not bound to a port, this function has no effect.

\begin{DoxySeeAlso}{See also}
\doxyref{bind}{p.}{classsf_1_1_udp_socket_ad764c3d06d90b4714dcc97a0d1647bcc} 
\end{DoxySeeAlso}


The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Network/\textbf{ Udp\+Socket.\+hpp}\end{DoxyCompactItemize}
