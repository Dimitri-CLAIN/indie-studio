\doxysection{sf\+::Transformable Class Reference}
\label{classsf_1_1_transformable}\index{sf::Transformable@{sf::Transformable}}


Decomposed transform defined by a position, a rotation and a scale.  




{\ttfamily \#include $<$Transformable.\+hpp$>$}



Inheritance diagram for sf\+::Transformable\+:
% FIG 0
\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Transformable} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
virtual \textbf{ $\sim$\+Transformable} ()
\begin{DoxyCompactList}\small\item\em Virtual destructor. \end{DoxyCompactList}\item 
void \textbf{ set\+Position} (float x, float y)
\begin{DoxyCompactList}\small\item\em set the position of the object \end{DoxyCompactList}\item 
void \textbf{ set\+Position} (const \textbf{ Vector2f} \&position)
\begin{DoxyCompactList}\small\item\em set the position of the object \end{DoxyCompactList}\item 
void \textbf{ set\+Rotation} (float angle)
\begin{DoxyCompactList}\small\item\em set the orientation of the object \end{DoxyCompactList}\item 
void \textbf{ set\+Scale} (float factorX, float factorY)
\begin{DoxyCompactList}\small\item\em set the scale factors of the object \end{DoxyCompactList}\item 
void \textbf{ set\+Scale} (const \textbf{ Vector2f} \&factors)
\begin{DoxyCompactList}\small\item\em set the scale factors of the object \end{DoxyCompactList}\item 
void \textbf{ set\+Origin} (float x, float y)
\begin{DoxyCompactList}\small\item\em set the local origin of the object \end{DoxyCompactList}\item 
void \textbf{ set\+Origin} (const \textbf{ Vector2f} \&origin)
\begin{DoxyCompactList}\small\item\em set the local origin of the object \end{DoxyCompactList}\item 
const \textbf{ Vector2f} \& \textbf{ get\+Position} () const
\begin{DoxyCompactList}\small\item\em get the position of the object \end{DoxyCompactList}\item 
float \textbf{ get\+Rotation} () const
\begin{DoxyCompactList}\small\item\em get the orientation of the object \end{DoxyCompactList}\item 
const \textbf{ Vector2f} \& \textbf{ get\+Scale} () const
\begin{DoxyCompactList}\small\item\em get the current scale of the object \end{DoxyCompactList}\item 
const \textbf{ Vector2f} \& \textbf{ get\+Origin} () const
\begin{DoxyCompactList}\small\item\em get the local origin of the object \end{DoxyCompactList}\item 
void \textbf{ move} (float offsetX, float offsetY)
\begin{DoxyCompactList}\small\item\em Move the object by a given offset. \end{DoxyCompactList}\item 
void \textbf{ move} (const \textbf{ Vector2f} \&offset)
\begin{DoxyCompactList}\small\item\em Move the object by a given offset. \end{DoxyCompactList}\item 
void \textbf{ rotate} (float angle)
\begin{DoxyCompactList}\small\item\em Rotate the object. \end{DoxyCompactList}\item 
void \textbf{ scale} (float factorX, float factorY)
\begin{DoxyCompactList}\small\item\em Scale the object. \end{DoxyCompactList}\item 
void \textbf{ scale} (const \textbf{ Vector2f} \&factor)
\begin{DoxyCompactList}\small\item\em Scale the object. \end{DoxyCompactList}\item 
const \textbf{ Transform} \& \textbf{ get\+Transform} () const
\begin{DoxyCompactList}\small\item\em get the combined transform of the object \end{DoxyCompactList}\item 
const \textbf{ Transform} \& \textbf{ get\+Inverse\+Transform} () const
\begin{DoxyCompactList}\small\item\em get the inverse of the combined transform of the object \end{DoxyCompactList}\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
Decomposed transform defined by a position, a rotation and a scale. 

This class is provided for convenience, on top of \doxyref{sf\+::\+Transform}{p.}{classsf_1_1_transform}.

\doxyref{sf\+::\+Transform}{p.}{classsf_1_1_transform}, as a low-\/level class, offers a great level of flexibility but it is not always convenient to manage. Indeed, one can easily combine any kind of operation, such as a translation followed by a rotation followed by a scaling, but once the result transform is built, there\textquotesingle{}s no way to go backward and, let\textquotesingle{}s say, change only the rotation without modifying the translation and scaling. The entire transform must be recomputed, which means that you need to retrieve the initial translation and scale factors as well, and combine them the same way you did before updating the rotation. This is a tedious operation, and it requires to store all the individual components of the final transform.

That\textquotesingle{}s exactly what \doxyref{sf\+::\+Transformable}{p.}{classsf_1_1_transformable} was written for\+: it hides these variables and the composed transform behind an easy to use interface. You can set or get any of the individual components without worrying about the others. It also provides the composed transform (as a \doxyref{sf\+::\+Transform}{p.}{classsf_1_1_transform}), and keeps it up-\/to-\/date.

In addition to the position, rotation and scale, \doxyref{sf\+::\+Transformable}{p.}{classsf_1_1_transformable} provides an \char`\"{}origin\char`\"{} component, which represents the local origin of the three other components. Let\textquotesingle{}s take an example with a 10x10 pixels sprite. By default, the sprite is positioned/rotated/scaled relatively to its top-\/left corner, because it is the local point (0, 0). But if we change the origin to be (5, 5), the sprite will be positioned/rotated/scaled around its center instead. And if we set the origin to (10, 10), it will be transformed around its bottom-\/right corner.

To keep the \doxyref{sf\+::\+Transformable}{p.}{classsf_1_1_transformable} class simple, there\textquotesingle{}s only one origin for all the components. You cannot position the sprite relatively to its top-\/left corner while rotating it around its center, for example. To do such things, use \doxyref{sf\+::\+Transform}{p.}{classsf_1_1_transform} directly.

\doxyref{sf\+::\+Transformable}{p.}{classsf_1_1_transformable} can be used as a base class. It is often combined with \doxyref{sf\+::\+Drawable}{p.}{classsf_1_1_drawable} -- that\textquotesingle{}s what S\+F\+ML\textquotesingle{}s sprites, texts and shapes do. 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{keyword}{class }MyEntity : \textcolor{keyword}{public} sf::Transformable, \textcolor{keyword}{public} sf::Drawable}
\DoxyCodeLine{\{}
\DoxyCodeLine{    \textcolor{keyword}{virtual} \textcolor{keywordtype}{void} draw(sf::RenderTarget\& target, sf::RenderStates states)\textcolor{keyword}{ const}}
\DoxyCodeLine{\textcolor{keyword}{    }\{}
\DoxyCodeLine{        states.transform *= getTransform();}
\DoxyCodeLine{        target.draw(..., states);}
\DoxyCodeLine{    \}}
\DoxyCodeLine{\};}
\DoxyCodeLine{}
\DoxyCodeLine{MyEntity entity;}
\DoxyCodeLine{entity.setPosition(10, 20);}
\DoxyCodeLine{entity.setRotation(45);}
\DoxyCodeLine{window.draw(entity);}
\end{DoxyCode}


It can also be used as a member, if you don\textquotesingle{}t want to use its A\+PI directly (because you don\textquotesingle{}t need all its functions, or you have different naming conventions for example). 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{keyword}{class }MyEntity}
\DoxyCodeLine{\{}
\DoxyCodeLine{\textcolor{keyword}{public}:}
\DoxyCodeLine{    \textcolor{keywordtype}{void} SetPosition(\textcolor{keyword}{const} MyVector\& v)}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        myTransform.setPosition(v.x(), v.y());}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keywordtype}{void} Draw(sf::RenderTarget\& target)\textcolor{keyword}{ const}}
\DoxyCodeLine{\textcolor{keyword}{    }\{}
\DoxyCodeLine{        target.draw(..., myTransform.getTransform());}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{keyword}{private}:}
\DoxyCodeLine{    sf::Transformable myTransform;}
\DoxyCodeLine{\};}
\end{DoxyCode}


A note on coordinates and undistorted rendering\+: ~\newline
By default, S\+F\+ML (or more exactly, Open\+GL) may interpolate drawable objects such as sprites or texts when rendering. While this allows transitions like slow movements or rotations to appear smoothly, it can lead to unwanted results in some cases, for example blurred or distorted objects. In order to render a \doxyref{sf\+::\+Drawable}{p.}{classsf_1_1_drawable} object pixel-\/perfectly, make sure the involved coordinates allow a 1\+:1 mapping of pixels in the window to texels (pixels in the texture). More specifically, this means\+:
\begin{DoxyItemize}
\item The object\textquotesingle{}s position, origin and scale have no fractional part
\item The object\textquotesingle{}s and the view\textquotesingle{}s rotation are a multiple of 90 degrees
\item The view\textquotesingle{}s center and size have no fractional part
\end{DoxyItemize}

\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Transform}{p.}{classsf_1_1_transform} 
\end{DoxySeeAlso}


Definition at line 41 of file Transformable.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_transformable_ae71710de0fef423121bab1c684954a2e}} 
\index{sf::Transformable@{sf::Transformable}!Transformable@{Transformable}}
\index{Transformable@{Transformable}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{Transformable()}
{\footnotesize\ttfamily sf\+::\+Transformable\+::\+Transformable (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Default constructor. 

\mbox{\label{classsf_1_1_transformable_a43253abcb863195a673c2a347a7425cc}} 
\index{sf::Transformable@{sf::Transformable}!````~Transformable@{$\sim$Transformable}}
\index{````~Transformable@{$\sim$Transformable}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{$\sim$Transformable()}
{\footnotesize\ttfamily virtual sf\+::\+Transformable\+::$\sim$\+Transformable (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [virtual]}}



Virtual destructor. 



\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_transformable_ab18b25f51263252ff3811465eb7e9fb1}} 
\index{sf::Transformable@{sf::Transformable}!getInverseTransform@{getInverseTransform}}
\index{getInverseTransform@{getInverseTransform}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{getInverseTransform()}
{\footnotesize\ttfamily const \textbf{ Transform}\& sf\+::\+Transformable\+::get\+Inverse\+Transform (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



get the inverse of the combined transform of the object 

\begin{DoxyReturn}{Returns}
Inverse of the combined transformations applied to the object
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Transform}{p.}{classsf_1_1_transformable_a7f7c3f0bab3f162b13613904fbdbb9ad} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a37ea3500afac234814a43ce809ef264e}} 
\index{sf::Transformable@{sf::Transformable}!getOrigin@{getOrigin}}
\index{getOrigin@{getOrigin}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{getOrigin()}
{\footnotesize\ttfamily const \textbf{ Vector2f}\& sf\+::\+Transformable\+::get\+Origin (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



get the local origin of the object 

\begin{DoxyReturn}{Returns}
Current origin
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Origin}{p.}{classsf_1_1_transformable_a56c67bd80aae8418d13fb96c034d25ec} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 1
\mbox{\label{classsf_1_1_transformable_a73f9739bc6e74db2cea154bc8e94ec46}} 
\index{sf::Transformable@{sf::Transformable}!getPosition@{getPosition}}
\index{getPosition@{getPosition}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{getPosition()}
{\footnotesize\ttfamily const \textbf{ Vector2f}\& sf\+::\+Transformable\+::get\+Position (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



get the position of the object 

\begin{DoxyReturn}{Returns}
Current position
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Position}{p.}{classsf_1_1_transformable_a4dbfb1a7c80688b0b4c477d706550208} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 2
\mbox{\label{classsf_1_1_transformable_aa00b5c5d4a06ac24a94dd72c56931d3a}} 
\index{sf::Transformable@{sf::Transformable}!getRotation@{getRotation}}
\index{getRotation@{getRotation}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{getRotation()}
{\footnotesize\ttfamily float sf\+::\+Transformable\+::get\+Rotation (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



get the orientation of the object 

The rotation is always in the range [0, 360].

\begin{DoxyReturn}{Returns}
Current rotation, in degrees
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Rotation}{p.}{classsf_1_1_transformable_a32baf2bf1a74699b03bf8c95030a38ed} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a73819fdea80ca8a06fad8a0067b4588c}} 
\index{sf::Transformable@{sf::Transformable}!getScale@{getScale}}
\index{getScale@{getScale}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{getScale()}
{\footnotesize\ttfamily const \textbf{ Vector2f}\& sf\+::\+Transformable\+::get\+Scale (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



get the current scale of the object 

\begin{DoxyReturn}{Returns}
Current scale factors
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Scale}{p.}{classsf_1_1_transformable_aaec50b46b3f41b054763304d1e727471} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a7f7c3f0bab3f162b13613904fbdbb9ad}} 
\index{sf::Transformable@{sf::Transformable}!getTransform@{getTransform}}
\index{getTransform@{getTransform}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{getTransform()}
{\footnotesize\ttfamily const \textbf{ Transform}\& sf\+::\+Transformable\+::get\+Transform (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



get the combined transform of the object 

\begin{DoxyReturn}{Returns}
\doxyref{Transform}{p.}{classsf_1_1_transform} combining the position/rotation/scale/origin of the object
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Inverse\+Transform}{p.}{classsf_1_1_transformable_ab18b25f51263252ff3811465eb7e9fb1} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_ab9ca691522f6ddc1a40406849b87c469}} 
\index{sf::Transformable@{sf::Transformable}!move@{move}}
\index{move@{move}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{move()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::move (\begin{DoxyParamCaption}\item[{const \textbf{ Vector2f} \&}]{offset }\end{DoxyParamCaption})}



Move the object by a given offset. 

This function adds to the current position of the object, unlike set\+Position which overwrites it. Thus, it is equivalent to the following code\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{keywordtype}{object}.setPosition(\textcolor{keywordtype}{object}.getPosition() + offset);}
\end{DoxyCode}



\begin{DoxyParams}{Parameters}
{\em offset} & Offset\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Position}{p.}{classsf_1_1_transformable_a4dbfb1a7c80688b0b4c477d706550208} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a86b461d6a941ad390c2ad8b6a4a20391}} 
\index{sf::Transformable@{sf::Transformable}!move@{move}}
\index{move@{move}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{move()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::move (\begin{DoxyParamCaption}\item[{float}]{offsetX,  }\item[{float}]{offsetY }\end{DoxyParamCaption})}



Move the object by a given offset. 

This function adds to the current position of the object, unlike set\+Position which overwrites it. Thus, it is equivalent to the following code\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{sf::Vector2f pos = \textcolor{keywordtype}{object}.getPosition();}
\DoxyCodeLine{\textcolor{keywordtype}{object}.setPosition(pos.x + offsetX, pos.y + offsetY);}
\end{DoxyCode}



\begin{DoxyParams}{Parameters}
{\em offsetX} & X offset \\
\hline
{\em offsetY} & Y offset\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Position}{p.}{classsf_1_1_transformable_a4dbfb1a7c80688b0b4c477d706550208} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 3
\mbox{\label{classsf_1_1_transformable_af8a5ffddc0d93f238fee3bf8efe1ebda}} 
\index{sf::Transformable@{sf::Transformable}!rotate@{rotate}}
\index{rotate@{rotate}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{rotate()}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::rotate (\begin{DoxyParamCaption}\item[{float}]{angle }\end{DoxyParamCaption})}



Rotate the object. 

This function adds to the current rotation of the object, unlike set\+Rotation which overwrites it. Thus, it is equivalent to the following code\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{keywordtype}{object}.setRotation(\textcolor{keywordtype}{object}.getRotation() + angle);}
\end{DoxyCode}



\begin{DoxyParams}{Parameters}
{\em angle} & Angle of rotation, in degrees \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_transformable_adecaa6c69b1f27dd5194b067d96bb694}} 
\index{sf::Transformable@{sf::Transformable}!scale@{scale}}
\index{scale@{scale}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{scale()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::scale (\begin{DoxyParamCaption}\item[{const \textbf{ Vector2f} \&}]{factor }\end{DoxyParamCaption})}



Scale the object. 

This function multiplies the current scale of the object, unlike set\+Scale which overwrites it. Thus, it is equivalent to the following code\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{sf::Vector2f scale = \textcolor{keywordtype}{object}.getScale();}
\DoxyCodeLine{\textcolor{keywordtype}{object}.setScale(scale.x * factor.x, scale.y * factor.y);}
\end{DoxyCode}



\begin{DoxyParams}{Parameters}
{\em factor} & Scale factors\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Scale}{p.}{classsf_1_1_transformable_aaec50b46b3f41b054763304d1e727471} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a3de0c6d8957f3cf318092f3f60656391}} 
\index{sf::Transformable@{sf::Transformable}!scale@{scale}}
\index{scale@{scale}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{scale()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::scale (\begin{DoxyParamCaption}\item[{float}]{factorX,  }\item[{float}]{factorY }\end{DoxyParamCaption})}



Scale the object. 

This function multiplies the current scale of the object, unlike set\+Scale which overwrites it. Thus, it is equivalent to the following code\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{sf::Vector2f scale = \textcolor{keywordtype}{object}.getScale();}
\DoxyCodeLine{\textcolor{keywordtype}{object}.setScale(scale.x * factorX, scale.y * factorY);}
\end{DoxyCode}



\begin{DoxyParams}{Parameters}
{\em factorX} & Horizontal scale factor \\
\hline
{\em factorY} & Vertical scale factor\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Scale}{p.}{classsf_1_1_transformable_aaec50b46b3f41b054763304d1e727471} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_aa93a835ffbf3bee2098dfbbc695a7f05}} 
\index{sf::Transformable@{sf::Transformable}!setOrigin@{setOrigin}}
\index{setOrigin@{setOrigin}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setOrigin()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Origin (\begin{DoxyParamCaption}\item[{const \textbf{ Vector2f} \&}]{origin }\end{DoxyParamCaption})}



set the local origin of the object 

The origin of an object defines the center point for all transformations (position, scale, rotation). The coordinates of this point must be relative to the top-\/left corner of the object, and ignore all transformations (position, scale, rotation). The default origin of a transformable object is (0, 0).


\begin{DoxyParams}{Parameters}
{\em origin} & New origin\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Origin}{p.}{classsf_1_1_transformable_a37ea3500afac234814a43ce809ef264e} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a56c67bd80aae8418d13fb96c034d25ec}} 
\index{sf::Transformable@{sf::Transformable}!setOrigin@{setOrigin}}
\index{setOrigin@{setOrigin}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setOrigin()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Origin (\begin{DoxyParamCaption}\item[{float}]{x,  }\item[{float}]{y }\end{DoxyParamCaption})}



set the local origin of the object 

The origin of an object defines the center point for all transformations (position, scale, rotation). The coordinates of this point must be relative to the top-\/left corner of the object, and ignore all transformations (position, scale, rotation). The default origin of a transformable object is (0, 0).


\begin{DoxyParams}{Parameters}
{\em x} & X coordinate of the new origin \\
\hline
{\em y} & Y coordinate of the new origin\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Origin}{p.}{classsf_1_1_transformable_a37ea3500afac234814a43ce809ef264e} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 4
\mbox{\label{classsf_1_1_transformable_af1a42209ce2b5d3f07b00f917bcd8015}} 
\index{sf::Transformable@{sf::Transformable}!setPosition@{setPosition}}
\index{setPosition@{setPosition}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setPosition()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Position (\begin{DoxyParamCaption}\item[{const \textbf{ Vector2f} \&}]{position }\end{DoxyParamCaption})}



set the position of the object 

This function completely overwrites the previous position. See the move function to apply an offset based on the previous position instead. The default position of a transformable object is (0, 0).


\begin{DoxyParams}{Parameters}
{\em position} & New position\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{move}{p.}{classsf_1_1_transformable_a86b461d6a941ad390c2ad8b6a4a20391}, \doxyref{get\+Position}{p.}{classsf_1_1_transformable_a73f9739bc6e74db2cea154bc8e94ec46} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_a4dbfb1a7c80688b0b4c477d706550208}} 
\index{sf::Transformable@{sf::Transformable}!setPosition@{setPosition}}
\index{setPosition@{setPosition}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setPosition()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Position (\begin{DoxyParamCaption}\item[{float}]{x,  }\item[{float}]{y }\end{DoxyParamCaption})}



set the position of the object 

This function completely overwrites the previous position. See the move function to apply an offset based on the previous position instead. The default position of a transformable object is (0, 0).


\begin{DoxyParams}{Parameters}
{\em x} & X coordinate of the new position \\
\hline
{\em y} & Y coordinate of the new position\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{move}{p.}{classsf_1_1_transformable_a86b461d6a941ad390c2ad8b6a4a20391}, \doxyref{get\+Position}{p.}{classsf_1_1_transformable_a73f9739bc6e74db2cea154bc8e94ec46} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 5
\mbox{\label{classsf_1_1_transformable_a32baf2bf1a74699b03bf8c95030a38ed}} 
\index{sf::Transformable@{sf::Transformable}!setRotation@{setRotation}}
\index{setRotation@{setRotation}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setRotation()}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Rotation (\begin{DoxyParamCaption}\item[{float}]{angle }\end{DoxyParamCaption})}



set the orientation of the object 

This function completely overwrites the previous rotation. See the rotate function to add an angle based on the previous rotation instead. The default rotation of a transformable object is 0.


\begin{DoxyParams}{Parameters}
{\em angle} & New rotation, in degrees\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{rotate}{p.}{classsf_1_1_transformable_af8a5ffddc0d93f238fee3bf8efe1ebda}, \doxyref{get\+Rotation}{p.}{classsf_1_1_transformable_aa00b5c5d4a06ac24a94dd72c56931d3a} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 6
\mbox{\label{classsf_1_1_transformable_a4c48a87f1626047e448f9c1a68ff167e}} 
\index{sf::Transformable@{sf::Transformable}!setScale@{setScale}}
\index{setScale@{setScale}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setScale()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Scale (\begin{DoxyParamCaption}\item[{const \textbf{ Vector2f} \&}]{factors }\end{DoxyParamCaption})}



set the scale factors of the object 

This function completely overwrites the previous scale. See the scale function to add a factor based on the previous scale instead. The default scale of a transformable object is (1, 1).


\begin{DoxyParams}{Parameters}
{\em factors} & New scale factors\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{scale}{p.}{classsf_1_1_transformable_a3de0c6d8957f3cf318092f3f60656391}, \doxyref{get\+Scale}{p.}{classsf_1_1_transformable_a73819fdea80ca8a06fad8a0067b4588c} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_transformable_aaec50b46b3f41b054763304d1e727471}} 
\index{sf::Transformable@{sf::Transformable}!setScale@{setScale}}
\index{setScale@{setScale}!sf::Transformable@{sf::Transformable}}
\doxysubsubsection{setScale()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily void sf\+::\+Transformable\+::set\+Scale (\begin{DoxyParamCaption}\item[{float}]{factorX,  }\item[{float}]{factorY }\end{DoxyParamCaption})}



set the scale factors of the object 

This function completely overwrites the previous scale. See the scale function to add a factor based on the previous scale instead. The default scale of a transformable object is (1, 1).


\begin{DoxyParams}{Parameters}
{\em factorX} & New horizontal scale factor \\
\hline
{\em factorY} & New vertical scale factor\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{scale}{p.}{classsf_1_1_transformable_a3de0c6d8957f3cf318092f3f60656391}, \doxyref{get\+Scale}{p.}{classsf_1_1_transformable_a73819fdea80ca8a06fad8a0067b4588c} 
\end{DoxySeeAlso}


The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Graphics/\textbf{ Transformable.\+hpp}\end{DoxyCompactItemize}
