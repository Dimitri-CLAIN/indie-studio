\doxysection{sf\+::Image Class Reference}
\label{classsf_1_1_image}\index{sf::Image@{sf::Image}}


Class for loading, manipulating and saving images.  




{\ttfamily \#include $<$Image.\+hpp$>$}

\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Image} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
\textbf{ $\sim$\+Image} ()
\begin{DoxyCompactList}\small\item\em Destructor. \end{DoxyCompactList}\item 
void \textbf{ create} (unsigned int width, unsigned int height, const \textbf{ Color} \&color=\textbf{ Color}(0, 0, 0))
\begin{DoxyCompactList}\small\item\em Create the image and fill it with a unique color. \end{DoxyCompactList}\item 
void \textbf{ create} (unsigned int width, unsigned int height, const \textbf{ Uint8} $\ast$pixels)
\begin{DoxyCompactList}\small\item\em Create the image from an array of pixels. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+File} (const std\+::string \&filename)
\begin{DoxyCompactList}\small\item\em Load the image from a file on disk. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+Memory} (const void $\ast$data, std\+::size\+\_\+t size)
\begin{DoxyCompactList}\small\item\em Load the image from a file in memory. \end{DoxyCompactList}\item 
bool \textbf{ load\+From\+Stream} (\textbf{ Input\+Stream} \&stream)
\begin{DoxyCompactList}\small\item\em Load the image from a custom stream. \end{DoxyCompactList}\item 
bool \textbf{ save\+To\+File} (const std\+::string \&filename) const
\begin{DoxyCompactList}\small\item\em Save the image to a file on disk. \end{DoxyCompactList}\item 
\textbf{ Vector2u} \textbf{ get\+Size} () const
\begin{DoxyCompactList}\small\item\em Return the size (width and height) of the image. \end{DoxyCompactList}\item 
void \textbf{ create\+Mask\+From\+Color} (const \textbf{ Color} \&color, \textbf{ Uint8} alpha=0)
\begin{DoxyCompactList}\small\item\em Create a transparency mask from a specified color-\/key. \end{DoxyCompactList}\item 
void \textbf{ copy} (const \textbf{ Image} \&source, unsigned int destX, unsigned int destY, const \textbf{ Int\+Rect} \&source\+Rect=\textbf{ Int\+Rect}(0, 0, 0, 0), bool apply\+Alpha=false)
\begin{DoxyCompactList}\small\item\em Copy pixels from another image onto this one. \end{DoxyCompactList}\item 
void \textbf{ set\+Pixel} (unsigned int x, unsigned int y, const \textbf{ Color} \&color)
\begin{DoxyCompactList}\small\item\em Change the color of a pixel. \end{DoxyCompactList}\item 
\textbf{ Color} \textbf{ get\+Pixel} (unsigned int x, unsigned int y) const
\begin{DoxyCompactList}\small\item\em Get the color of a pixel. \end{DoxyCompactList}\item 
const \textbf{ Uint8} $\ast$ \textbf{ get\+Pixels\+Ptr} () const
\begin{DoxyCompactList}\small\item\em Get a read-\/only pointer to the array of pixels. \end{DoxyCompactList}\item 
void \textbf{ flip\+Horizontally} ()
\begin{DoxyCompactList}\small\item\em Flip the image horizontally (left $<$-\/$>$ right) \end{DoxyCompactList}\item 
void \textbf{ flip\+Vertically} ()
\begin{DoxyCompactList}\small\item\em Flip the image vertically (top $<$-\/$>$ bottom) \end{DoxyCompactList}\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
Class for loading, manipulating and saving images. 

\doxyref{sf\+::\+Image}{p.}{classsf_1_1_image} is an abstraction to manipulate images as bidimensional arrays of pixels. The class provides functions to load, read, write and save pixels, as well as many other useful functions.

\doxyref{sf\+::\+Image}{p.}{classsf_1_1_image} can handle a unique internal representation of pixels, which is R\+G\+BA 32 bits. This means that a pixel must be composed of 8 bits red, green, blue and alpha channels -- just like a \doxyref{sf\+::\+Color}{p.}{classsf_1_1_color}. All the functions that return an array of pixels follow this rule, and all parameters that you pass to \doxyref{sf\+::\+Image}{p.}{classsf_1_1_image} functions (such as load\+From\+Memory) must use this representation as well.

A \doxyref{sf\+::\+Image}{p.}{classsf_1_1_image} can be copied, but it is a heavy resource and if possible you should always use [const] references to pass or return them to avoid useless copies.

Usage example\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{comment}{// Load an image file from a file}}
\DoxyCodeLine{sf::Image background;}
\DoxyCodeLine{\textcolor{keywordflow}{if} (!background.loadFromFile(\textcolor{stringliteral}{"background.jpg"}))}
\DoxyCodeLine{    \textcolor{keywordflow}{return} -\/1;}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create a 20x20 image filled with black color}}
\DoxyCodeLine{sf::Image image;}
\DoxyCodeLine{image.create(20, 20, sf::Color::Black);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Copy image1 on image2 at position (10, 10)}}
\DoxyCodeLine{image.copy(background, 10, 10);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Make the top-\/left pixel transparent}}
\DoxyCodeLine{sf::Color color = image.getPixel(0, 0);}
\DoxyCodeLine{color.a = 0;}
\DoxyCodeLine{image.setPixel(0, 0, color);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Save the image to a file}}
\DoxyCodeLine{\textcolor{keywordflow}{if} (!image.saveToFile(\textcolor{stringliteral}{"result.png"}))}
\DoxyCodeLine{    \textcolor{keywordflow}{return} -\/1;}
\end{DoxyCode}


\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Texture}{p.}{classsf_1_1_texture} 
\end{DoxySeeAlso}


Definition at line 46 of file Image.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_image_abb4caf3cb167b613345ebe36fc883f12}} 
\index{sf::Image@{sf::Image}!Image@{Image}}
\index{Image@{Image}!sf::Image@{sf::Image}}
\doxysubsubsection{Image()}
{\footnotesize\ttfamily sf\+::\+Image\+::\+Image (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Default constructor. 

Creates an empty image. \mbox{\label{classsf_1_1_image_a0ba22a38e6c96e3b37dd88198046de83}} 
\index{sf::Image@{sf::Image}!````~Image@{$\sim$Image}}
\index{````~Image@{$\sim$Image}!sf::Image@{sf::Image}}
\doxysubsubsection{$\sim$Image()}
{\footnotesize\ttfamily sf\+::\+Image\+::$\sim$\+Image (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Destructor. 



\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_image_ab2fa337c956f85f93377dcb52153a45a}} 
\index{sf::Image@{sf::Image}!copy@{copy}}
\index{copy@{copy}!sf::Image@{sf::Image}}
\doxysubsubsection{copy()}
{\footnotesize\ttfamily void sf\+::\+Image\+::copy (\begin{DoxyParamCaption}\item[{const \textbf{ Image} \&}]{source,  }\item[{unsigned int}]{destX,  }\item[{unsigned int}]{destY,  }\item[{const \textbf{ Int\+Rect} \&}]{source\+Rect = {\ttfamily \textbf{ Int\+Rect}(0,~0,~0,~0)},  }\item[{bool}]{apply\+Alpha = {\ttfamily false} }\end{DoxyParamCaption})}



Copy pixels from another image onto this one. 

This function does a slow pixel copy and should not be used intensively. It can be used to prepare a complex static image from several others, but if you need this kind of feature in real-\/time you\textquotesingle{}d better use \doxyref{sf\+::\+Render\+Texture}{p.}{classsf_1_1_render_texture}.

If {\itshape source\+Rect} is empty, the whole image is copied. If {\itshape apply\+Alpha} is set to true, the transparency of source pixels is applied. If it is false, the pixels are copied unchanged with their alpha value.


\begin{DoxyParams}{Parameters}
{\em source} & Source image to copy \\
\hline
{\em destX} & X coordinate of the destination position \\
\hline
{\em destY} & Y coordinate of the destination position \\
\hline
{\em source\+Rect} & Sub-\/rectangle of the source image to copy \\
\hline
{\em apply\+Alpha} & Should the copy take into account the source transparency? \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_image_a2a67930e2fd9ad97cf004e918cf5832b}} 
\index{sf::Image@{sf::Image}!create@{create}}
\index{create@{create}!sf::Image@{sf::Image}}
\doxysubsubsection{create()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily void sf\+::\+Image\+::create (\begin{DoxyParamCaption}\item[{unsigned int}]{width,  }\item[{unsigned int}]{height,  }\item[{const \textbf{ Color} \&}]{color = {\ttfamily \textbf{ Color}(0,~0,~0)} }\end{DoxyParamCaption})}



Create the image and fill it with a unique color. 


\begin{DoxyParams}{Parameters}
{\em width} & Width of the image \\
\hline
{\em height} & Height of the image \\
\hline
{\em color} & Fill color \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_image_a1c2b960ea12bdbb29e80934ce5268ebf}} 
\index{sf::Image@{sf::Image}!create@{create}}
\index{create@{create}!sf::Image@{sf::Image}}
\doxysubsubsection{create()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily void sf\+::\+Image\+::create (\begin{DoxyParamCaption}\item[{unsigned int}]{width,  }\item[{unsigned int}]{height,  }\item[{const \textbf{ Uint8} $\ast$}]{pixels }\end{DoxyParamCaption})}



Create the image from an array of pixels. 

The {\itshape pixel} array is assumed to contain 32-\/bits R\+G\+BA pixels, and have the given {\itshape width} and {\itshape height}. If not, this is an undefined behavior. If {\itshape pixels} is null, an empty image is created.


\begin{DoxyParams}{Parameters}
{\em width} & Width of the image \\
\hline
{\em height} & Height of the image \\
\hline
{\em pixels} & Array of pixels to copy to the image \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_image_a22f13f8c242a6b38eb73cc176b37ae34}} 
\index{sf::Image@{sf::Image}!createMaskFromColor@{createMaskFromColor}}
\index{createMaskFromColor@{createMaskFromColor}!sf::Image@{sf::Image}}
\doxysubsubsection{createMaskFromColor()}
{\footnotesize\ttfamily void sf\+::\+Image\+::create\+Mask\+From\+Color (\begin{DoxyParamCaption}\item[{const \textbf{ Color} \&}]{color,  }\item[{\textbf{ Uint8}}]{alpha = {\ttfamily 0} }\end{DoxyParamCaption})}



Create a transparency mask from a specified color-\/key. 

This function sets the alpha value of every pixel matching the given color to {\itshape alpha} (0 by default), so that they become transparent.


\begin{DoxyParams}{Parameters}
{\em color} & \doxyref{Color}{p.}{classsf_1_1_color} to make transparent \\
\hline
{\em alpha} & Alpha value to assign to transparent pixels \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_image_a57168e7bc29190e08bbd6c9c19f4bb2c}} 
\index{sf::Image@{sf::Image}!flipHorizontally@{flipHorizontally}}
\index{flipHorizontally@{flipHorizontally}!sf::Image@{sf::Image}}
\doxysubsubsection{flipHorizontally()}
{\footnotesize\ttfamily void sf\+::\+Image\+::flip\+Horizontally (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Flip the image horizontally (left $<$-\/$>$ right) 

\mbox{\label{classsf_1_1_image_a78a702a7e49d1de2dec9894da99d279c}} 
\index{sf::Image@{sf::Image}!flipVertically@{flipVertically}}
\index{flipVertically@{flipVertically}!sf::Image@{sf::Image}}
\doxysubsubsection{flipVertically()}
{\footnotesize\ttfamily void sf\+::\+Image\+::flip\+Vertically (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Flip the image vertically (top $<$-\/$>$ bottom) 

\mbox{\label{classsf_1_1_image_acf278760458433b2c3626a6980388a95}} 
\index{sf::Image@{sf::Image}!getPixel@{getPixel}}
\index{getPixel@{getPixel}!sf::Image@{sf::Image}}
\doxysubsubsection{getPixel()}
{\footnotesize\ttfamily \textbf{ Color} sf\+::\+Image\+::get\+Pixel (\begin{DoxyParamCaption}\item[{unsigned int}]{x,  }\item[{unsigned int}]{y }\end{DoxyParamCaption}) const}



Get the color of a pixel. 

This function doesn\textquotesingle{}t check the validity of the pixel coordinates, using out-\/of-\/range values will result in an undefined behavior.


\begin{DoxyParams}{Parameters}
{\em x} & X coordinate of pixel to get \\
\hline
{\em y} & Y coordinate of pixel to get\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
\doxyref{Color}{p.}{classsf_1_1_color} of the pixel at coordinates (x, y)
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Pixel}{p.}{classsf_1_1_image_a9fd329b8cd7d4439e07fb5d3bb2d9744} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_image_ad9562b126fc8d5efcf608166992865c7}} 
\index{sf::Image@{sf::Image}!getPixelsPtr@{getPixelsPtr}}
\index{getPixelsPtr@{getPixelsPtr}!sf::Image@{sf::Image}}
\doxysubsubsection{getPixelsPtr()}
{\footnotesize\ttfamily const \textbf{ Uint8}$\ast$ sf\+::\+Image\+::get\+Pixels\+Ptr (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get a read-\/only pointer to the array of pixels. 

The returned value points to an array of R\+G\+BA pixels made of 8 bits integers components. The size of the array is width $\ast$ height $\ast$ 4 (\doxyref{get\+Size()}{p.}{classsf_1_1_image_a85409951b05369813069ed64393391ce}.x $\ast$ \doxyref{get\+Size()}{p.}{classsf_1_1_image_a85409951b05369813069ed64393391ce}.y $\ast$ 4). Warning\+: the returned pointer may become invalid if you modify the image, so you should never store it for too long. If the image is empty, a null pointer is returned.

\begin{DoxyReturn}{Returns}
Read-\/only pointer to the array of pixels 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_image_a85409951b05369813069ed64393391ce}} 
\index{sf::Image@{sf::Image}!getSize@{getSize}}
\index{getSize@{getSize}!sf::Image@{sf::Image}}
\doxysubsubsection{getSize()}
{\footnotesize\ttfamily \textbf{ Vector2u} sf\+::\+Image\+::get\+Size (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Return the size (width and height) of the image. 

\begin{DoxyReturn}{Returns}
Size of the image, in pixels 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_image_a9e4f2aa8e36d0cabde5ed5a4ef80290b}} 
\index{sf::Image@{sf::Image}!loadFromFile@{loadFromFile}}
\index{loadFromFile@{loadFromFile}!sf::Image@{sf::Image}}
\doxysubsubsection{loadFromFile()}
{\footnotesize\ttfamily bool sf\+::\+Image\+::load\+From\+File (\begin{DoxyParamCaption}\item[{const std\+::string \&}]{filename }\end{DoxyParamCaption})}



Load the image from a file on disk. 

The supported image formats are bmp, png, tga, jpg, gif, psd, hdr and pic. Some format options are not supported, like progressive jpeg. If this function fails, the image is left unchanged.


\begin{DoxyParams}{Parameters}
{\em filename} & Path of the image file to load\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading was successful
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+Memory}{p.}{classsf_1_1_image_aaa6c7afa5851a51cec6ab438faa7354c}, \doxyref{load\+From\+Stream}{p.}{classsf_1_1_image_a21122ded0e8368bb06ed3b9acfbfb501}, \doxyref{save\+To\+File}{p.}{classsf_1_1_image_a51537fb667f47cbe80395cfd7f9e72a4} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_image_aaa6c7afa5851a51cec6ab438faa7354c}} 
\index{sf::Image@{sf::Image}!loadFromMemory@{loadFromMemory}}
\index{loadFromMemory@{loadFromMemory}!sf::Image@{sf::Image}}
\doxysubsubsection{loadFromMemory()}
{\footnotesize\ttfamily bool sf\+::\+Image\+::load\+From\+Memory (\begin{DoxyParamCaption}\item[{const void $\ast$}]{data,  }\item[{std\+::size\+\_\+t}]{size }\end{DoxyParamCaption})}



Load the image from a file in memory. 

The supported image formats are bmp, png, tga, jpg, gif, psd, hdr and pic. Some format options are not supported, like progressive jpeg. If this function fails, the image is left unchanged.


\begin{DoxyParams}{Parameters}
{\em data} & Pointer to the file data in memory \\
\hline
{\em size} & Size of the data to load, in bytes\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading was successful
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+File}{p.}{classsf_1_1_image_a9e4f2aa8e36d0cabde5ed5a4ef80290b}, \doxyref{load\+From\+Stream}{p.}{classsf_1_1_image_a21122ded0e8368bb06ed3b9acfbfb501} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_image_a21122ded0e8368bb06ed3b9acfbfb501}} 
\index{sf::Image@{sf::Image}!loadFromStream@{loadFromStream}}
\index{loadFromStream@{loadFromStream}!sf::Image@{sf::Image}}
\doxysubsubsection{loadFromStream()}
{\footnotesize\ttfamily bool sf\+::\+Image\+::load\+From\+Stream (\begin{DoxyParamCaption}\item[{\textbf{ Input\+Stream} \&}]{stream }\end{DoxyParamCaption})}



Load the image from a custom stream. 

The supported image formats are bmp, png, tga, jpg, gif, psd, hdr and pic. Some format options are not supported, like progressive jpeg. If this function fails, the image is left unchanged.


\begin{DoxyParams}{Parameters}
{\em stream} & Source stream to read from\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if loading was successful
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{load\+From\+File}{p.}{classsf_1_1_image_a9e4f2aa8e36d0cabde5ed5a4ef80290b}, \doxyref{load\+From\+Memory}{p.}{classsf_1_1_image_aaa6c7afa5851a51cec6ab438faa7354c} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_image_a51537fb667f47cbe80395cfd7f9e72a4}} 
\index{sf::Image@{sf::Image}!saveToFile@{saveToFile}}
\index{saveToFile@{saveToFile}!sf::Image@{sf::Image}}
\doxysubsubsection{saveToFile()}
{\footnotesize\ttfamily bool sf\+::\+Image\+::save\+To\+File (\begin{DoxyParamCaption}\item[{const std\+::string \&}]{filename }\end{DoxyParamCaption}) const}



Save the image to a file on disk. 

The format of the image is automatically deduced from the extension. The supported image formats are bmp, png, tga and jpg. The destination file is overwritten if it already exists. This function fails if the image is empty.


\begin{DoxyParams}{Parameters}
{\em filename} & Path of the file to save\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if saving was successful
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{create}{p.}{classsf_1_1_image_a2a67930e2fd9ad97cf004e918cf5832b}, \doxyref{load\+From\+File}{p.}{classsf_1_1_image_a9e4f2aa8e36d0cabde5ed5a4ef80290b}, \doxyref{load\+From\+Memory}{p.}{classsf_1_1_image_aaa6c7afa5851a51cec6ab438faa7354c} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_image_a9fd329b8cd7d4439e07fb5d3bb2d9744}} 
\index{sf::Image@{sf::Image}!setPixel@{setPixel}}
\index{setPixel@{setPixel}!sf::Image@{sf::Image}}
\doxysubsubsection{setPixel()}
{\footnotesize\ttfamily void sf\+::\+Image\+::set\+Pixel (\begin{DoxyParamCaption}\item[{unsigned int}]{x,  }\item[{unsigned int}]{y,  }\item[{const \textbf{ Color} \&}]{color }\end{DoxyParamCaption})}



Change the color of a pixel. 

This function doesn\textquotesingle{}t check the validity of the pixel coordinates, using out-\/of-\/range values will result in an undefined behavior.


\begin{DoxyParams}{Parameters}
{\em x} & X coordinate of pixel to change \\
\hline
{\em y} & Y coordinate of pixel to change \\
\hline
{\em color} & New color of the pixel\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Pixel}{p.}{classsf_1_1_image_acf278760458433b2c3626a6980388a95} 
\end{DoxySeeAlso}


The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Graphics/\textbf{ Image.\+hpp}\end{DoxyCompactItemize}
