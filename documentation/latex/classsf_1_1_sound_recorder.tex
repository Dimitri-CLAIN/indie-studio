\doxysection{sf\+::Sound\+Recorder Class Reference}
\label{classsf_1_1_sound_recorder}\index{sf::SoundRecorder@{sf::SoundRecorder}}


Abstract base class for capturing sound data.  




{\ttfamily \#include $<$Sound\+Recorder.\+hpp$>$}



Inheritance diagram for sf\+::Sound\+Recorder\+:
% FIG 0


Collaboration diagram for sf\+::Sound\+Recorder\+:
% FIG 1
\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
virtual \textbf{ $\sim$\+Sound\+Recorder} ()
\begin{DoxyCompactList}\small\item\em destructor \end{DoxyCompactList}\item 
bool \textbf{ start} (unsigned int sample\+Rate=44100)
\begin{DoxyCompactList}\small\item\em Start the capture. \end{DoxyCompactList}\item 
void \textbf{ stop} ()
\begin{DoxyCompactList}\small\item\em Stop the capture. \end{DoxyCompactList}\item 
unsigned int \textbf{ get\+Sample\+Rate} () const
\begin{DoxyCompactList}\small\item\em Get the sample rate. \end{DoxyCompactList}\item 
bool \textbf{ set\+Device} (const std\+::string \&name)
\begin{DoxyCompactList}\small\item\em Set the audio capture device. \end{DoxyCompactList}\item 
const std\+::string \& \textbf{ get\+Device} () const
\begin{DoxyCompactList}\small\item\em Get the name of the current audio capture device. \end{DoxyCompactList}\item 
void \textbf{ set\+Channel\+Count} (unsigned int channel\+Count)
\begin{DoxyCompactList}\small\item\em Set the channel count of the audio capture device. \end{DoxyCompactList}\item 
unsigned int \textbf{ get\+Channel\+Count} () const
\begin{DoxyCompactList}\small\item\em Get the number of channels used by this recorder. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Static Public Member Functions}
\begin{DoxyCompactItemize}
\item 
static std\+::vector$<$ std\+::string $>$ \textbf{ get\+Available\+Devices} ()
\begin{DoxyCompactList}\small\item\em Get a list of the names of all available audio capture devices. \end{DoxyCompactList}\item 
static std\+::string \textbf{ get\+Default\+Device} ()
\begin{DoxyCompactList}\small\item\em Get the name of the default audio capture device. \end{DoxyCompactList}\item 
static bool \textbf{ is\+Available} ()
\begin{DoxyCompactList}\small\item\em Check if the system supports audio capture. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Protected Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Sound\+Recorder} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
void \textbf{ set\+Processing\+Interval} (\textbf{ Time} interval)
\begin{DoxyCompactList}\small\item\em Set the processing interval. \end{DoxyCompactList}\item 
virtual bool \textbf{ on\+Start} ()
\begin{DoxyCompactList}\small\item\em Start capturing audio data. \end{DoxyCompactList}\item 
virtual bool \textbf{ on\+Process\+Samples} (const \textbf{ Int16} $\ast$samples, std\+::size\+\_\+t sample\+Count)=0
\begin{DoxyCompactList}\small\item\em Process a new chunk of recorded samples. \end{DoxyCompactList}\item 
virtual void \textbf{ on\+Stop} ()
\begin{DoxyCompactList}\small\item\em Stop capturing audio data. \end{DoxyCompactList}\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
Abstract base class for capturing sound data. 

\doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer} provides a simple interface to access the audio recording capabilities of the computer (the microphone). As an abstract base class, it only cares about capturing sound samples, the task of making something useful with them is left to the derived class. Note that S\+F\+ML provides a built-\/in specialization for saving the captured data to a sound buffer (see \doxyref{sf\+::\+Sound\+Buffer\+Recorder}{p.}{classsf_1_1_sound_buffer_recorder}).

A derived class has only one virtual function to override\+: \begin{DoxyItemize}
\item on\+Process\+Samples provides the new chunks of audio samples while the capture happens\end{DoxyItemize}
Moreover, two additional virtual functions can be overridden as well if necessary\+: \begin{DoxyItemize}
\item on\+Start is called before the capture happens, to perform custom initializations \item on\+Stop is called after the capture ends, to perform custom cleanup\end{DoxyItemize}
A derived class can also control the frequency of the on\+Process\+Samples calls, with the set\+Processing\+Interval protected function. The default interval is chosen so that recording thread doesn\textquotesingle{}t consume too much C\+PU, but it can be changed to a smaller value if you need to process the recorded data in real time, for example.

The audio capture feature may not be supported or activated on every platform, thus it is recommended to check its availability with the \doxyref{is\+Available()}{p.}{classsf_1_1_sound_recorder_aab2bd0fee9e48d6cfd449b1cb078ce5a} function. If it returns false, then any attempt to use an audio recorder will fail.

If you have multiple sound input devices connected to your computer (for example\+: microphone, external soundcard, webcam mic, ...) you can get a list of all available devices through the \doxyref{get\+Available\+Devices()}{p.}{classsf_1_1_sound_recorder_a26198c5c11efcd61f426f326fe314afe} function. You can then select a device by calling \doxyref{set\+Device()}{p.}{classsf_1_1_sound_recorder_a8eb3e473292c16e874322815836d3cd3} with the appropriate device. Otherwise the default capturing device will be used.

By default the recording is in 16-\/bit mono. Using the set\+Channel\+Count method you can change the number of channels used by the audio capture device to record. Note that you have to decide whether you want to record in mono or stereo before starting the recording.

It is important to note that the audio capture happens in a separate thread, so that it doesn\textquotesingle{}t block the rest of the program. In particular, the on\+Process\+Samples virtual function (but not on\+Start and not on\+Stop) will be called from this separate thread. It is important to keep this in mind, because you may have to take care of synchronization issues if you share data between threads. Another thing to bear in mind is that you must call \doxyref{stop()}{p.}{classsf_1_1_sound_recorder_a8d9c8346aa9aa409cfed4a1101159c4c} in the destructor of your derived class, so that the recording thread finishes before your object is destroyed.

Usage example\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{keyword}{class }CustomRecorder : \textcolor{keyword}{public} sf::SoundRecorder}
\DoxyCodeLine{\{}
\DoxyCodeLine{    \string~CustomRecorder()}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Make sure to stop the recording thread}}
\DoxyCodeLine{        stop();}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keyword}{virtual} \textcolor{keywordtype}{bool} onStart() \textcolor{comment}{// optional}}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Initialize whatever has to be done before the capture starts}}
\DoxyCodeLine{        ...}
\DoxyCodeLine{}
\DoxyCodeLine{        \textcolor{comment}{// Return true to start playing}}
\DoxyCodeLine{        \textcolor{keywordflow}{return} \textcolor{keyword}{true};}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keyword}{virtual} \textcolor{keywordtype}{bool} onProcessSamples(\textcolor{keyword}{const} Int16* samples, std::size\_t sampleCount)}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Do something with the new chunk of samples (store them, send them, ...)}}
\DoxyCodeLine{        ...}
\DoxyCodeLine{}
\DoxyCodeLine{        \textcolor{comment}{// Return true to continue playing}}
\DoxyCodeLine{        \textcolor{keywordflow}{return} \textcolor{keyword}{true};}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keyword}{virtual} \textcolor{keywordtype}{void} onStop() \textcolor{comment}{// optional}}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Clean up whatever has to be done after the capture ends}}
\DoxyCodeLine{        ...}
\DoxyCodeLine{    \}}
\DoxyCodeLine{\}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Usage}}
\DoxyCodeLine{\textcolor{keywordflow}{if} (CustomRecorder::isAvailable())}
\DoxyCodeLine{\{}
\DoxyCodeLine{    CustomRecorder recorder;}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keywordflow}{if} (!recorder.start())}
\DoxyCodeLine{        \textcolor{keywordflow}{return} -\/1;}
\DoxyCodeLine{}
\DoxyCodeLine{    ...}
\DoxyCodeLine{    recorder.stop();}
\DoxyCodeLine{\}}
\end{DoxyCode}


\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Sound\+Buffer\+Recorder}{p.}{classsf_1_1_sound_buffer_recorder} 
\end{DoxySeeAlso}


Definition at line 45 of file Sound\+Recorder.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_sound_recorder_acc599e61aaa47edaae88cf43f0a43549}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!````~SoundRecorder@{$\sim$SoundRecorder}}
\index{````~SoundRecorder@{$\sim$SoundRecorder}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{$\sim$SoundRecorder()}
{\footnotesize\ttfamily virtual sf\+::\+Sound\+Recorder\+::$\sim$\+Sound\+Recorder (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [virtual]}}



destructor 

\mbox{\label{classsf_1_1_sound_recorder_a50ebad413c4f157408a0fa49f23212a9}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!SoundRecorder@{SoundRecorder}}
\index{SoundRecorder@{SoundRecorder}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{SoundRecorder()}
{\footnotesize\ttfamily sf\+::\+Sound\+Recorder\+::\+Sound\+Recorder (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}}



Default constructor. 

This constructor is only meant to be called by derived classes. 

\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_sound_recorder_a26198c5c11efcd61f426f326fe314afe}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!getAvailableDevices@{getAvailableDevices}}
\index{getAvailableDevices@{getAvailableDevices}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{getAvailableDevices()}
{\footnotesize\ttfamily static std\+::vector$<$std\+::string$>$ sf\+::\+Sound\+Recorder\+::get\+Available\+Devices (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [static]}}



Get a list of the names of all available audio capture devices. 

This function returns a vector of strings, containing the names of all available audio capture devices.

\begin{DoxyReturn}{Returns}
A vector of strings containing the names 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_sound_recorder_a610e98e7a73b316ce26b7c55234f86e9}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!getChannelCount@{getChannelCount}}
\index{getChannelCount@{getChannelCount}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{getChannelCount()}
{\footnotesize\ttfamily unsigned int sf\+::\+Sound\+Recorder\+::get\+Channel\+Count (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the number of channels used by this recorder. 

Currently only mono and stereo are supported, so the value is either 1 (for mono) or 2 (for stereo).

\begin{DoxyReturn}{Returns}
Number of channels
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Channel\+Count}{p.}{classsf_1_1_sound_recorder_ae4e22ba67d12a74966eb05fad55a317c} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_recorder_ad1d450a80642dab4b632999d72a1bf23}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!getDefaultDevice@{getDefaultDevice}}
\index{getDefaultDevice@{getDefaultDevice}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{getDefaultDevice()}
{\footnotesize\ttfamily static std\+::string sf\+::\+Sound\+Recorder\+::get\+Default\+Device (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [static]}}



Get the name of the default audio capture device. 

This function returns the name of the default audio capture device. If none is available, an empty string is returned.

\begin{DoxyReturn}{Returns}
The name of the default audio capture device 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_sound_recorder_ab16cd53c6884cbf3380c017cee72ba81}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!getDevice@{getDevice}}
\index{getDevice@{getDevice}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{getDevice()}
{\footnotesize\ttfamily const std\+::string\& sf\+::\+Sound\+Recorder\+::get\+Device (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the name of the current audio capture device. 

\begin{DoxyReturn}{Returns}
The name of the current audio capture device 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_sound_recorder_aed292c297a3e0d627db4eb5c18f58c44}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!getSampleRate@{getSampleRate}}
\index{getSampleRate@{getSampleRate}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{getSampleRate()}
{\footnotesize\ttfamily unsigned int sf\+::\+Sound\+Recorder\+::get\+Sample\+Rate (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the sample rate. 

The sample rate defines the number of audio samples captured per second. The higher, the better the quality (for example, 44100 samples/sec is CD quality).

\begin{DoxyReturn}{Returns}
Sample rate, in samples per second 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_sound_recorder_aab2bd0fee9e48d6cfd449b1cb078ce5a}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!isAvailable@{isAvailable}}
\index{isAvailable@{isAvailable}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{isAvailable()}
{\footnotesize\ttfamily static bool sf\+::\+Sound\+Recorder\+::is\+Available (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [static]}}



Check if the system supports audio capture. 

This function should always be called before using the audio capture features. If it returns false, then any attempt to use \doxyref{sf\+::\+Sound\+Recorder}{p.}{classsf_1_1_sound_recorder} or one of its derived classes will fail.

\begin{DoxyReturn}{Returns}
True if audio capture is supported, false otherwise 
\end{DoxyReturn}
Here is the caller graph for this function\+:
% FIG 2
\mbox{\label{classsf_1_1_sound_recorder_a2670124cbe7a87c7e46b4840807f4fd7}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!onProcessSamples@{onProcessSamples}}
\index{onProcessSamples@{onProcessSamples}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{onProcessSamples()}
{\footnotesize\ttfamily virtual bool sf\+::\+Sound\+Recorder\+::on\+Process\+Samples (\begin{DoxyParamCaption}\item[{const \textbf{ Int16} $\ast$}]{samples,  }\item[{std\+::size\+\_\+t}]{sample\+Count }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}, {\ttfamily [pure virtual]}}



Process a new chunk of recorded samples. 

This virtual function is called every time a new chunk of recorded data is available. The derived class can then do whatever it wants with it (storing it, playing it, sending it over the network, etc.).


\begin{DoxyParams}{Parameters}
{\em samples} & Pointer to the new chunk of recorded samples \\
\hline
{\em sample\+Count} & Number of samples pointed by {\itshape samples} \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True to continue the capture, or false to stop it 
\end{DoxyReturn}


Implemented in \textbf{ sf\+::\+Sound\+Buffer\+Recorder} \doxyref{}{p.}{classsf_1_1_sound_buffer_recorder_a9ceb94de14632ae8c1b78faf603b4767}.

\mbox{\label{classsf_1_1_sound_recorder_a7af418fb036201d3f85745bef78ce77f}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!onStart@{onStart}}
\index{onStart@{onStart}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{onStart()}
{\footnotesize\ttfamily virtual bool sf\+::\+Sound\+Recorder\+::on\+Start (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}, {\ttfamily [virtual]}}



Start capturing audio data. 

This virtual function may be overridden by a derived class if something has to be done every time a new capture starts. If not, this function can be ignored; the default implementation does nothing.

\begin{DoxyReturn}{Returns}
True to start the capture, or false to abort it 
\end{DoxyReturn}


Reimplemented in \textbf{ sf\+::\+Sound\+Buffer\+Recorder} \doxyref{}{p.}{classsf_1_1_sound_buffer_recorder_a531a7445fc8a48eaf9fc039c83f17c6f}.

\mbox{\label{classsf_1_1_sound_recorder_aefc36138ca1e96c658301280e4a31b64}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!onStop@{onStop}}
\index{onStop@{onStop}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{onStop()}
{\footnotesize\ttfamily virtual void sf\+::\+Sound\+Recorder\+::on\+Stop (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}, {\ttfamily [virtual]}}



Stop capturing audio data. 

This virtual function may be overridden by a derived class if something has to be done every time the capture ends. If not, this function can be ignored; the default implementation does nothing. 

Reimplemented in \textbf{ sf\+::\+Sound\+Buffer\+Recorder} \doxyref{}{p.}{classsf_1_1_sound_buffer_recorder_ab8e53849312413431873a5869d509f1e}.

\mbox{\label{classsf_1_1_sound_recorder_ae4e22ba67d12a74966eb05fad55a317c}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!setChannelCount@{setChannelCount}}
\index{setChannelCount@{setChannelCount}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{setChannelCount()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Recorder\+::set\+Channel\+Count (\begin{DoxyParamCaption}\item[{unsigned int}]{channel\+Count }\end{DoxyParamCaption})}



Set the channel count of the audio capture device. 

This method allows you to specify the number of channels used for recording. Currently only 16-\/bit mono and 16-\/bit stereo are supported.


\begin{DoxyParams}{Parameters}
{\em channel\+Count} & Number of channels. Currently only mono (1) and stereo (2) are supported.\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Channel\+Count}{p.}{classsf_1_1_sound_recorder_a610e98e7a73b316ce26b7c55234f86e9} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_recorder_a8eb3e473292c16e874322815836d3cd3}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!setDevice@{setDevice}}
\index{setDevice@{setDevice}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{setDevice()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Recorder\+::set\+Device (\begin{DoxyParamCaption}\item[{const std\+::string \&}]{name }\end{DoxyParamCaption})}



Set the audio capture device. 

This function sets the audio capture device to the device with the given {\itshape name}. It can be called on the fly (i.\+e\+: while recording). If you do so while recording and opening the device fails, it stops the recording.


\begin{DoxyParams}{Parameters}
{\em name} & The name of the audio capture device\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True, if it was able to set the requested device
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Available\+Devices}{p.}{classsf_1_1_sound_recorder_a26198c5c11efcd61f426f326fe314afe}, \doxyref{get\+Default\+Device}{p.}{classsf_1_1_sound_recorder_ad1d450a80642dab4b632999d72a1bf23} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_recorder_a85b7fb8a86c08b5084f8f142767bccf6}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!setProcessingInterval@{setProcessingInterval}}
\index{setProcessingInterval@{setProcessingInterval}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{setProcessingInterval()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Recorder\+::set\+Processing\+Interval (\begin{DoxyParamCaption}\item[{\textbf{ Time}}]{interval }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}}



Set the processing interval. 

The processing interval controls the period between calls to the on\+Process\+Samples function. You may want to use a small interval if you want to process the recorded data in real time, for example.

Note\+: this is only a hint, the actual period may vary. So don\textquotesingle{}t rely on this parameter to implement precise timing.

The default processing interval is 100 ms.


\begin{DoxyParams}{Parameters}
{\em interval} & Processing interval \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_sound_recorder_a715f0fd2f228c83d79aaedca562ae51f}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!start@{start}}
\index{start@{start}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{start()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Recorder\+::start (\begin{DoxyParamCaption}\item[{unsigned int}]{sample\+Rate = {\ttfamily 44100} }\end{DoxyParamCaption})}



Start the capture. 

The {\itshape sample\+Rate} parameter defines the number of audio samples captured per second. The higher, the better the quality (for example, 44100 samples/sec is CD quality). This function uses its own thread so that it doesn\textquotesingle{}t block the rest of the program while the capture runs. Please note that only one capture can happen at the same time. You can select which capture device will be used, by passing the name to the \doxyref{set\+Device()}{p.}{classsf_1_1_sound_recorder_a8eb3e473292c16e874322815836d3cd3} method. If none was selected before, the default capture device will be used. You can get a list of the names of all available capture devices by calling \doxyref{get\+Available\+Devices()}{p.}{classsf_1_1_sound_recorder_a26198c5c11efcd61f426f326fe314afe}.


\begin{DoxyParams}{Parameters}
{\em sample\+Rate} & Desired capture rate, in number of samples per second\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True, if start of capture was successful
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{stop}{p.}{classsf_1_1_sound_recorder_a8d9c8346aa9aa409cfed4a1101159c4c}, \doxyref{get\+Available\+Devices}{p.}{classsf_1_1_sound_recorder_a26198c5c11efcd61f426f326fe314afe} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 3
\mbox{\label{classsf_1_1_sound_recorder_a8d9c8346aa9aa409cfed4a1101159c4c}} 
\index{sf::SoundRecorder@{sf::SoundRecorder}!stop@{stop}}
\index{stop@{stop}!sf::SoundRecorder@{sf::SoundRecorder}}
\doxysubsubsection{stop()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Recorder\+::stop (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Stop the capture. 

\begin{DoxySeeAlso}{See also}
\doxyref{start}{p.}{classsf_1_1_sound_recorder_a715f0fd2f228c83d79aaedca562ae51f} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 4


The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Audio/\textbf{ Sound\+Recorder.\+hpp}\end{DoxyCompactItemize}
