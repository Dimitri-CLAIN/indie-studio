\doxysection{sf\+::Socket\+Selector Class Reference}
\label{classsf_1_1_socket_selector}\index{sf::SocketSelector@{sf::SocketSelector}}


Multiplexer that allows to read from multiple sockets.  




{\ttfamily \#include $<$Socket\+Selector.\+hpp$>$}

\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Socket\+Selector} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
\textbf{ Socket\+Selector} (const \textbf{ Socket\+Selector} \&copy)
\begin{DoxyCompactList}\small\item\em Copy constructor. \end{DoxyCompactList}\item 
\textbf{ $\sim$\+Socket\+Selector} ()
\begin{DoxyCompactList}\small\item\em Destructor. \end{DoxyCompactList}\item 
void \textbf{ add} (\textbf{ Socket} \&socket)
\begin{DoxyCompactList}\small\item\em Add a new socket to the selector. \end{DoxyCompactList}\item 
void \textbf{ remove} (\textbf{ Socket} \&socket)
\begin{DoxyCompactList}\small\item\em Remove a socket from the selector. \end{DoxyCompactList}\item 
void \textbf{ clear} ()
\begin{DoxyCompactList}\small\item\em Remove all the sockets stored in the selector. \end{DoxyCompactList}\item 
bool \textbf{ wait} (\textbf{ Time} timeout=\textbf{ Time\+::\+Zero})
\begin{DoxyCompactList}\small\item\em Wait until one or more sockets are ready to receive. \end{DoxyCompactList}\item 
bool \textbf{ is\+Ready} (\textbf{ Socket} \&socket) const
\begin{DoxyCompactList}\small\item\em Test a socket to know if it is ready to receive data. \end{DoxyCompactList}\item 
\textbf{ Socket\+Selector} \& \textbf{ operator=} (const \textbf{ Socket\+Selector} \&right)
\begin{DoxyCompactList}\small\item\em Overload of assignment operator. \end{DoxyCompactList}\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
Multiplexer that allows to read from multiple sockets. 

\doxyref{Socket}{p.}{classsf_1_1_socket} selectors provide a way to wait until some data is available on a set of sockets, instead of just one. This is convenient when you have multiple sockets that may possibly receive data, but you don\textquotesingle{}t know which one will be ready first. In particular, it avoids to use a thread for each socket; with selectors, a single thread can handle all the sockets.

All types of sockets can be used in a selector\+: \begin{DoxyItemize}
\item \doxyref{sf\+::\+Tcp\+Listener}{p.}{classsf_1_1_tcp_listener} \item \doxyref{sf\+::\+Tcp\+Socket}{p.}{classsf_1_1_tcp_socket} \item \doxyref{sf\+::\+Udp\+Socket}{p.}{classsf_1_1_udp_socket}\end{DoxyItemize}
A selector doesn\textquotesingle{}t store its own copies of the sockets (socket classes are not copyable anyway), it simply keeps a reference to the original sockets that you pass to the \char`\"{}add\char`\"{} function. Therefore, you can\textquotesingle{}t use the selector as a socket container, you must store them outside and make sure that they are alive as long as they are used in the selector.

Using a selector is simple\+: \begin{DoxyItemize}
\item populate the selector with all the sockets that you want to observe \item make it wait until there is data available on any of the sockets \item test each socket to find out which ones are ready\end{DoxyItemize}
Usage example\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{comment}{// Create a socket to listen to new connections}}
\DoxyCodeLine{sf::TcpListener listener;}
\DoxyCodeLine{listener.listen(55001);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create a list to store the future clients}}
\DoxyCodeLine{std::list<sf::TcpSocket*> clients;}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Create a selector}}
\DoxyCodeLine{sf::SocketSelector selector;}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Add the listener to the selector}}
\DoxyCodeLine{selector.add(listener);}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Endless loop that waits for new connections}}
\DoxyCodeLine{\textcolor{keywordflow}{while} (running)}
\DoxyCodeLine{\{}
\DoxyCodeLine{    \textcolor{comment}{// Make the selector wait for data on any socket}}
\DoxyCodeLine{    \textcolor{keywordflow}{if} (selector.wait())}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Test the listener}}
\DoxyCodeLine{        \textcolor{keywordflow}{if} (selector.isReady(listener))}
\DoxyCodeLine{        \{}
\DoxyCodeLine{            \textcolor{comment}{// The listener is ready: there is a pending connection}}
\DoxyCodeLine{            sf::TcpSocket* client = \textcolor{keyword}{new} sf::TcpSocket;}
\DoxyCodeLine{            \textcolor{keywordflow}{if} (listener.accept(*client) == sf::Socket::Done)}
\DoxyCodeLine{            \{}
\DoxyCodeLine{                \textcolor{comment}{// Add the new client to the clients list}}
\DoxyCodeLine{                clients.push\_back(client);}
\DoxyCodeLine{}
\DoxyCodeLine{                \textcolor{comment}{// Add the new client to the selector so that we will}}
\DoxyCodeLine{                \textcolor{comment}{// be notified when he sends something}}
\DoxyCodeLine{                selector.add(*client);}
\DoxyCodeLine{            \}}
\DoxyCodeLine{            \textcolor{keywordflow}{else}}
\DoxyCodeLine{            \{}
\DoxyCodeLine{                \textcolor{comment}{// Error, we won't get a new connection, delete the socket}}
\DoxyCodeLine{                \textcolor{keyword}{delete} client;}
\DoxyCodeLine{            \}}
\DoxyCodeLine{        \}}
\DoxyCodeLine{        \textcolor{keywordflow}{else}}
\DoxyCodeLine{        \{}
\DoxyCodeLine{            \textcolor{comment}{// The listener socket is not ready, test all other sockets (the clients)}}
\DoxyCodeLine{            \textcolor{keywordflow}{for} (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end(); ++it)}
\DoxyCodeLine{            \{}
\DoxyCodeLine{                sf::TcpSocket\& client = **it;}
\DoxyCodeLine{                \textcolor{keywordflow}{if} (selector.isReady(client))}
\DoxyCodeLine{                \{}
\DoxyCodeLine{                    \textcolor{comment}{// The client has sent some data, we can receive it}}
\DoxyCodeLine{                    sf::Packet packet;}
\DoxyCodeLine{                    \textcolor{keywordflow}{if} (client.receive(packet) == sf::Socket::Done)}
\DoxyCodeLine{                    \{}
\DoxyCodeLine{                        ...}
\DoxyCodeLine{                    \}}
\DoxyCodeLine{                \}}
\DoxyCodeLine{            \}}
\DoxyCodeLine{        \}}
\DoxyCodeLine{    \}}
\DoxyCodeLine{\}}
\end{DoxyCode}


\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Socket}{p.}{classsf_1_1_socket} 
\end{DoxySeeAlso}


Definition at line 43 of file Socket\+Selector.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_socket_selector_a741959c5158aeb1e4457cad47d90f76b}} 
\index{sf::SocketSelector@{sf::SocketSelector}!SocketSelector@{SocketSelector}}
\index{SocketSelector@{SocketSelector}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{SocketSelector()\hspace{0.1cm}{\footnotesize\ttfamily [1/2]}}
{\footnotesize\ttfamily sf\+::\+Socket\+Selector\+::\+Socket\+Selector (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Default constructor. 

\mbox{\label{classsf_1_1_socket_selector_a50b1b955eb7ecb2e7c2764f3f4722fbf}} 
\index{sf::SocketSelector@{sf::SocketSelector}!SocketSelector@{SocketSelector}}
\index{SocketSelector@{SocketSelector}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{SocketSelector()\hspace{0.1cm}{\footnotesize\ttfamily [2/2]}}
{\footnotesize\ttfamily sf\+::\+Socket\+Selector\+::\+Socket\+Selector (\begin{DoxyParamCaption}\item[{const \textbf{ Socket\+Selector} \&}]{copy }\end{DoxyParamCaption})}



Copy constructor. 


\begin{DoxyParams}{Parameters}
{\em copy} & Instance to copy \\
\hline
\end{DoxyParams}
\mbox{\label{classsf_1_1_socket_selector_a9069cd61208260b8ed9cf233afa1f73d}} 
\index{sf::SocketSelector@{sf::SocketSelector}!````~SocketSelector@{$\sim$SocketSelector}}
\index{````~SocketSelector@{$\sim$SocketSelector}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{$\sim$SocketSelector()}
{\footnotesize\ttfamily sf\+::\+Socket\+Selector\+::$\sim$\+Socket\+Selector (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Destructor. 



\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_socket_selector_ade952013232802ff7b9b33668f8d2096}} 
\index{sf::SocketSelector@{sf::SocketSelector}!add@{add}}
\index{add@{add}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{add()}
{\footnotesize\ttfamily void sf\+::\+Socket\+Selector\+::add (\begin{DoxyParamCaption}\item[{\textbf{ Socket} \&}]{socket }\end{DoxyParamCaption})}



Add a new socket to the selector. 

This function keeps a weak reference to the socket, so you have to make sure that the socket is not destroyed while it is stored in the selector. This function does nothing if the socket is not valid.


\begin{DoxyParams}{Parameters}
{\em socket} & Reference to the socket to add\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{remove}{p.}{classsf_1_1_socket_selector_a98b6ab693a65b82caa375639232357c1}, \doxyref{clear}{p.}{classsf_1_1_socket_selector_a76e650acb0199d4be91e90a493fbc91a} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_socket_selector_a76e650acb0199d4be91e90a493fbc91a}} 
\index{sf::SocketSelector@{sf::SocketSelector}!clear@{clear}}
\index{clear@{clear}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{clear()}
{\footnotesize\ttfamily void sf\+::\+Socket\+Selector\+::clear (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



Remove all the sockets stored in the selector. 

This function doesn\textquotesingle{}t destroy any instance, it simply removes all the references that the selector has to external sockets.

\begin{DoxySeeAlso}{See also}
\doxyref{add}{p.}{classsf_1_1_socket_selector_ade952013232802ff7b9b33668f8d2096}, \doxyref{remove}{p.}{classsf_1_1_socket_selector_a98b6ab693a65b82caa375639232357c1} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_socket_selector_a917a4bac708290a6782e6686fd3bf889}} 
\index{sf::SocketSelector@{sf::SocketSelector}!isReady@{isReady}}
\index{isReady@{isReady}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{isReady()}
{\footnotesize\ttfamily bool sf\+::\+Socket\+Selector\+::is\+Ready (\begin{DoxyParamCaption}\item[{\textbf{ Socket} \&}]{socket }\end{DoxyParamCaption}) const}



Test a socket to know if it is ready to receive data. 

This function must be used after a call to Wait, to know which sockets are ready to receive data. If a socket is ready, a call to receive will never block because we know that there is data available to read. Note that if this function returns true for a \doxyref{Tcp\+Listener}{p.}{classsf_1_1_tcp_listener}, this means that it is ready to accept a new connection.


\begin{DoxyParams}{Parameters}
{\em socket} & \doxyref{Socket}{p.}{classsf_1_1_socket} to test\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if the socket is ready to read, false otherwise
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{is\+Ready}{p.}{classsf_1_1_socket_selector_a917a4bac708290a6782e6686fd3bf889} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_socket_selector_ae6395c7a8d29a9ea14939cc5d1ba3a33}} 
\index{sf::SocketSelector@{sf::SocketSelector}!operator=@{operator=}}
\index{operator=@{operator=}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{operator=()}
{\footnotesize\ttfamily \textbf{ Socket\+Selector}\& sf\+::\+Socket\+Selector\+::operator= (\begin{DoxyParamCaption}\item[{const \textbf{ Socket\+Selector} \&}]{right }\end{DoxyParamCaption})}



Overload of assignment operator. 


\begin{DoxyParams}{Parameters}
{\em right} & Instance to assign\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
Reference to self 
\end{DoxyReturn}
\mbox{\label{classsf_1_1_socket_selector_a98b6ab693a65b82caa375639232357c1}} 
\index{sf::SocketSelector@{sf::SocketSelector}!remove@{remove}}
\index{remove@{remove}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{remove()}
{\footnotesize\ttfamily void sf\+::\+Socket\+Selector\+::remove (\begin{DoxyParamCaption}\item[{\textbf{ Socket} \&}]{socket }\end{DoxyParamCaption})}



Remove a socket from the selector. 

This function doesn\textquotesingle{}t destroy the socket, it simply removes the reference that the selector has to it.


\begin{DoxyParams}{Parameters}
{\em socket} & Reference to the socket to remove\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{add}{p.}{classsf_1_1_socket_selector_ade952013232802ff7b9b33668f8d2096}, \doxyref{clear}{p.}{classsf_1_1_socket_selector_a76e650acb0199d4be91e90a493fbc91a} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_socket_selector_a9cfda5475f17925e65889394d70af702}} 
\index{sf::SocketSelector@{sf::SocketSelector}!wait@{wait}}
\index{wait@{wait}!sf::SocketSelector@{sf::SocketSelector}}
\doxysubsubsection{wait()}
{\footnotesize\ttfamily bool sf\+::\+Socket\+Selector\+::wait (\begin{DoxyParamCaption}\item[{\textbf{ Time}}]{timeout = {\ttfamily \textbf{ Time\+::\+Zero}} }\end{DoxyParamCaption})}



Wait until one or more sockets are ready to receive. 

This function returns as soon as at least one socket has some data available to be received. To know which sockets are ready, use the is\+Ready function. If you use a timeout and no socket is ready before the timeout is over, the function returns false.


\begin{DoxyParams}{Parameters}
{\em timeout} & Maximum time to wait, (use \doxyref{Time\+::\+Zero}{p.}{classsf_1_1_time_a8db127b632fa8da21550e7282af11fa0} for infinity)\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True if there are sockets ready, false otherwise
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{is\+Ready}{p.}{classsf_1_1_socket_selector_a917a4bac708290a6782e6686fd3bf889} 
\end{DoxySeeAlso}


The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Network/\textbf{ Socket\+Selector.\+hpp}\end{DoxyCompactItemize}
