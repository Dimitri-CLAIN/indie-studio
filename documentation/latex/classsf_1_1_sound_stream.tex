\doxysection{sf\+::Sound\+Stream Class Reference}
\label{classsf_1_1_sound_stream}\index{sf::SoundStream@{sf::SoundStream}}


Abstract base class for streamed audio sources.  




{\ttfamily \#include $<$Sound\+Stream.\+hpp$>$}



Inheritance diagram for sf\+::Sound\+Stream\+:
% FIG 0


Collaboration diagram for sf\+::Sound\+Stream\+:
% FIG 1
\doxysubsection*{Classes}
\begin{DoxyCompactItemize}
\item 
struct \textbf{ Chunk}
\begin{DoxyCompactList}\small\item\em Structure defining a chunk of audio data to stream. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
virtual \textbf{ $\sim$\+Sound\+Stream} ()
\begin{DoxyCompactList}\small\item\em Destructor. \end{DoxyCompactList}\item 
void \textbf{ play} ()
\begin{DoxyCompactList}\small\item\em Start or resume playing the audio stream. \end{DoxyCompactList}\item 
void \textbf{ pause} ()
\begin{DoxyCompactList}\small\item\em Pause the audio stream. \end{DoxyCompactList}\item 
void \textbf{ stop} ()
\begin{DoxyCompactList}\small\item\em Stop playing the audio stream. \end{DoxyCompactList}\item 
unsigned int \textbf{ get\+Channel\+Count} () const
\begin{DoxyCompactList}\small\item\em Return the number of channels of the stream. \end{DoxyCompactList}\item 
unsigned int \textbf{ get\+Sample\+Rate} () const
\begin{DoxyCompactList}\small\item\em Get the stream sample rate of the stream. \end{DoxyCompactList}\item 
\textbf{ Status} \textbf{ get\+Status} () const
\begin{DoxyCompactList}\small\item\em Get the current status of the stream (stopped, paused, playing) \end{DoxyCompactList}\item 
void \textbf{ set\+Playing\+Offset} (\textbf{ Time} time\+Offset)
\begin{DoxyCompactList}\small\item\em Change the current playing position of the stream. \end{DoxyCompactList}\item 
\textbf{ Time} \textbf{ get\+Playing\+Offset} () const
\begin{DoxyCompactList}\small\item\em Get the current playing position of the stream. \end{DoxyCompactList}\item 
void \textbf{ set\+Loop} (bool loop)
\begin{DoxyCompactList}\small\item\em Set whether or not the stream should loop after reaching the end. \end{DoxyCompactList}\item 
bool \textbf{ get\+Loop} () const
\begin{DoxyCompactList}\small\item\em Tell whether or not the stream is in loop mode. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Protected Types}
\begin{DoxyCompactItemize}
\item 
enum \{ \textbf{ No\+Loop} = -\/1
 \}
\end{DoxyCompactItemize}
\doxysubsection*{Protected Member Functions}
\begin{DoxyCompactItemize}
\item 
\textbf{ Sound\+Stream} ()
\begin{DoxyCompactList}\small\item\em Default constructor. \end{DoxyCompactList}\item 
void \textbf{ initialize} (unsigned int channel\+Count, unsigned int sample\+Rate)
\begin{DoxyCompactList}\small\item\em Define the audio stream parameters. \end{DoxyCompactList}\item 
virtual bool \textbf{ on\+Get\+Data} (\textbf{ Chunk} \&data)=0
\begin{DoxyCompactList}\small\item\em Request a new chunk of audio samples from the stream source. \end{DoxyCompactList}\item 
virtual void \textbf{ on\+Seek} (\textbf{ Time} time\+Offset)=0
\begin{DoxyCompactList}\small\item\em Change the current playing position in the stream source. \end{DoxyCompactList}\item 
virtual \textbf{ Int64} \textbf{ on\+Loop} ()
\begin{DoxyCompactList}\small\item\em Change the current playing position in the stream source to the beginning of the loop. \end{DoxyCompactList}\end{DoxyCompactItemize}
\doxysubsection*{Additional Inherited Members}


\doxysubsection{Detailed Description}
Abstract base class for streamed audio sources. 

Unlike audio buffers (see \doxyref{sf\+::\+Sound\+Buffer}{p.}{classsf_1_1_sound_buffer}), audio streams are never completely loaded in memory. Instead, the audio data is acquired continuously while the stream is playing. This behavior allows to play a sound with no loading delay, and keeps the memory consumption very low.

\doxyref{Sound}{p.}{classsf_1_1_sound} sources that need to be streamed are usually big files (compressed audio musics that would eat hundreds of MB in memory) or files that would take a lot of time to be received (sounds played over the network).

\doxyref{sf\+::\+Sound\+Stream}{p.}{classsf_1_1_sound_stream} is a base class that doesn\textquotesingle{}t care about the stream source, which is left to the derived class. S\+F\+ML provides a built-\/in specialization for big files (see \doxyref{sf\+::\+Music}{p.}{classsf_1_1_music}). No network stream source is provided, but you can write your own by combining this class with the network module.

A derived class has to override two virtual functions\+: \begin{DoxyItemize}
\item on\+Get\+Data fills a new chunk of audio data to be played \item on\+Seek changes the current playing position in the source\end{DoxyItemize}
It is important to note that each \doxyref{Sound\+Stream}{p.}{classsf_1_1_sound_stream} is played in its own separate thread, so that the streaming loop doesn\textquotesingle{}t block the rest of the program. In particular, the On\+Get\+Data and On\+Seek virtual functions may sometimes be called from this separate thread. It is important to keep this in mind, because you may have to take care of synchronization issues if you share data between threads.

Usage example\+: 
\begin{DoxyCode}{0}
\DoxyCodeLine{\textcolor{keyword}{class }CustomStream : \textcolor{keyword}{public} sf::SoundStream}
\DoxyCodeLine{\{}
\DoxyCodeLine{\textcolor{keyword}{public}:}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keywordtype}{bool} open(\textcolor{keyword}{const} std::string\& location)}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Open the source and get audio settings}}
\DoxyCodeLine{        ...}
\DoxyCodeLine{        \textcolor{keywordtype}{unsigned} \textcolor{keywordtype}{int} channelCount = ...;}
\DoxyCodeLine{        \textcolor{keywordtype}{unsigned} \textcolor{keywordtype}{int} sampleRate = ...;}
\DoxyCodeLine{}
\DoxyCodeLine{        \textcolor{comment}{// Initialize the stream -\/-\/ important!}}
\DoxyCodeLine{        initialize(channelCount, sampleRate);}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{keyword}{private}:}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keyword}{virtual} \textcolor{keywordtype}{bool} onGetData(Chunk\& data)}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Fill the chunk with audio data from the stream source}}
\DoxyCodeLine{        \textcolor{comment}{// (note: must not be empty if you want to continue playing)}}
\DoxyCodeLine{        data.samples = ...;}
\DoxyCodeLine{        data.sampleCount = ...;}
\DoxyCodeLine{}
\DoxyCodeLine{        \textcolor{comment}{// Return true to continue playing}}
\DoxyCodeLine{        \textcolor{keywordflow}{return} \textcolor{keyword}{true};}
\DoxyCodeLine{    \}}
\DoxyCodeLine{}
\DoxyCodeLine{    \textcolor{keyword}{virtual} \textcolor{keywordtype}{void} onSeek(Uint32 timeOffset)}
\DoxyCodeLine{    \{}
\DoxyCodeLine{        \textcolor{comment}{// Change the current position in the stream source}}
\DoxyCodeLine{        ...}
\DoxyCodeLine{    \}}
\DoxyCodeLine{\}}
\DoxyCodeLine{}
\DoxyCodeLine{\textcolor{comment}{// Usage}}
\DoxyCodeLine{CustomStream stream;}
\DoxyCodeLine{stream.open(\textcolor{stringliteral}{"path/to/stream"});}
\DoxyCodeLine{stream.play();}
\end{DoxyCode}


\begin{DoxySeeAlso}{See also}
\doxyref{sf\+::\+Music}{p.}{classsf_1_1_music} 
\end{DoxySeeAlso}


Definition at line 45 of file Sound\+Stream.\+hpp.



\doxysubsection{Member Enumeration Documentation}
\mbox{\label{classsf_1_1_sound_stream_a565522050e090f50307f32cc004f59a0}} 
\doxysubsubsection{anonymous enum}
{\footnotesize\ttfamily anonymous enum\hspace{0.3cm}{\ttfamily [protected]}}

\begin{DoxyEnumFields}{Enumerator}
\raisebox{\heightof{T}}[0pt][0pt]{\index{NoLoop@{NoLoop}!sf::SoundStream@{sf::SoundStream}}\index{sf::SoundStream@{sf::SoundStream}!NoLoop@{NoLoop}}}\mbox{\label{classsf_1_1_sound_stream_a565522050e090f50307f32cc004f59a0a2f2c638731fdff0d6fe4e3e82b6f6146}} 
No\+Loop&\char`\"{}\+Invalid\char`\"{} end\+Seeks value, telling us to continue uninterrupted \\
\hline

\end{DoxyEnumFields}


Definition at line 183 of file Sound\+Stream.\+hpp.



\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\label{classsf_1_1_sound_stream_a1fafb9f1ca572d23d7d6a17921860d85}} 
\index{sf::SoundStream@{sf::SoundStream}!````~SoundStream@{$\sim$SoundStream}}
\index{````~SoundStream@{$\sim$SoundStream}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{$\sim$SoundStream()}
{\footnotesize\ttfamily virtual sf\+::\+Sound\+Stream\+::$\sim$\+Sound\+Stream (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [virtual]}}



Destructor. 

\mbox{\label{classsf_1_1_sound_stream_a769d08f4c3c6b4340ef3a838329d2e5c}} 
\index{sf::SoundStream@{sf::SoundStream}!SoundStream@{SoundStream}}
\index{SoundStream@{SoundStream}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{SoundStream()}
{\footnotesize\ttfamily sf\+::\+Sound\+Stream\+::\+Sound\+Stream (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}}



Default constructor. 

This constructor is only meant to be called by derived classes. 

\doxysubsection{Member Function Documentation}
\mbox{\label{classsf_1_1_sound_stream_a1f70933912dd9498f4dc99feefed27f3}} 
\index{sf::SoundStream@{sf::SoundStream}!getChannelCount@{getChannelCount}}
\index{getChannelCount@{getChannelCount}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{getChannelCount()}
{\footnotesize\ttfamily unsigned int sf\+::\+Sound\+Stream\+::get\+Channel\+Count (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Return the number of channels of the stream. 

1 channel means a mono sound, 2 means stereo, etc.

\begin{DoxyReturn}{Returns}
Number of channels 
\end{DoxyReturn}
Here is the caller graph for this function\+:
% FIG 2
\mbox{\label{classsf_1_1_sound_stream_a49d263f9bbaefec4b019bd05fda59b25}} 
\index{sf::SoundStream@{sf::SoundStream}!getLoop@{getLoop}}
\index{getLoop@{getLoop}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{getLoop()}
{\footnotesize\ttfamily bool sf\+::\+Sound\+Stream\+::get\+Loop (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Tell whether or not the stream is in loop mode. 

\begin{DoxyReturn}{Returns}
True if the stream is looping, false otherwise
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Loop}{p.}{classsf_1_1_sound_stream_a43fade018ffba7e4f847a9f00b353f3d} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_stream_ae288f3c72edbad9cc7ee938ce5b907c1}} 
\index{sf::SoundStream@{sf::SoundStream}!getPlayingOffset@{getPlayingOffset}}
\index{getPlayingOffset@{getPlayingOffset}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{getPlayingOffset()}
{\footnotesize\ttfamily \textbf{ Time} sf\+::\+Sound\+Stream\+::get\+Playing\+Offset (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the current playing position of the stream. 

\begin{DoxyReturn}{Returns}
Current playing position, from the beginning of the stream
\end{DoxyReturn}
\begin{DoxySeeAlso}{See also}
\doxyref{set\+Playing\+Offset}{p.}{classsf_1_1_sound_stream_af416a5f84c8750d2acb9821d78bc8646} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 3
\mbox{\label{classsf_1_1_sound_stream_a7da448dc40d81a33b8dc555fbf0d3fbf}} 
\index{sf::SoundStream@{sf::SoundStream}!getSampleRate@{getSampleRate}}
\index{getSampleRate@{getSampleRate}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{getSampleRate()}
{\footnotesize\ttfamily unsigned int sf\+::\+Sound\+Stream\+::get\+Sample\+Rate (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const}



Get the stream sample rate of the stream. 

The sample rate is the number of audio samples played per second. The higher, the better the quality.

\begin{DoxyReturn}{Returns}
Sample rate, in number of samples per second 
\end{DoxyReturn}
Here is the caller graph for this function\+:
% FIG 4
\mbox{\label{classsf_1_1_sound_stream_a64a8193ed728da37c115c65de015849f}} 
\index{sf::SoundStream@{sf::SoundStream}!getStatus@{getStatus}}
\index{getStatus@{getStatus}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{getStatus()}
{\footnotesize\ttfamily \textbf{ Status} sf\+::\+Sound\+Stream\+::get\+Status (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption}) const\hspace{0.3cm}{\ttfamily [virtual]}}



Get the current status of the stream (stopped, paused, playing) 

\begin{DoxyReturn}{Returns}
Current status 
\end{DoxyReturn}


Reimplemented from \textbf{ sf\+::\+Sound\+Source} \doxyref{}{p.}{classsf_1_1_sound_source_aa8d313c31b968159582a999aa66e5ed7}.

Here is the caller graph for this function\+:
% FIG 5
\mbox{\label{classsf_1_1_sound_stream_a9c351711198ee1aa77c2fefd3ced4d2c}} 
\index{sf::SoundStream@{sf::SoundStream}!initialize@{initialize}}
\index{initialize@{initialize}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{initialize()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Stream\+::initialize (\begin{DoxyParamCaption}\item[{unsigned int}]{channel\+Count,  }\item[{unsigned int}]{sample\+Rate }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}}



Define the audio stream parameters. 

This function must be called by derived classes as soon as they know the audio settings of the stream to play. Any attempt to manipulate the stream (\doxyref{play()}{p.}{classsf_1_1_sound_stream_afdc08b69cab5f243d9324940a85a1144}, ...) before calling this function will fail. It can be called multiple times if the settings of the audio stream change, but only when the stream is stopped.


\begin{DoxyParams}{Parameters}
{\em channel\+Count} & Number of channels of the stream \\
\hline
{\em sample\+Rate} & Sample rate, in samples per second \\
\hline
\end{DoxyParams}
Here is the caller graph for this function\+:
% FIG 6
\mbox{\label{classsf_1_1_sound_stream_a968ec024a6e45490962c8a1121cb7c5f}} 
\index{sf::SoundStream@{sf::SoundStream}!onGetData@{onGetData}}
\index{onGetData@{onGetData}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{onGetData()}
{\footnotesize\ttfamily virtual bool sf\+::\+Sound\+Stream\+::on\+Get\+Data (\begin{DoxyParamCaption}\item[{\textbf{ Chunk} \&}]{data }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}, {\ttfamily [pure virtual]}}



Request a new chunk of audio samples from the stream source. 

This function must be overridden by derived classes to provide the audio samples to play. It is called continuously by the streaming loop, in a separate thread. The source can choose to stop the streaming loop at any time, by returning false to the caller. If you return true (i.\+e. continue streaming) it is important that the returned array of samples is not empty; this would stop the stream due to an internal limitation.


\begin{DoxyParams}{Parameters}
{\em data} & \doxyref{Chunk}{p.}{structsf_1_1_sound_stream_1_1_chunk} of data to fill\\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
True to continue playback, false to stop 
\end{DoxyReturn}


Implemented in \textbf{ sf\+::\+Music} \doxyref{}{p.}{classsf_1_1_music_aca1bcb4e5d56a854133e74bd86374463}.

\mbox{\label{classsf_1_1_sound_stream_a3f717d18846f261fc375d71d6c7e41da}} 
\index{sf::SoundStream@{sf::SoundStream}!onLoop@{onLoop}}
\index{onLoop@{onLoop}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{onLoop()}
{\footnotesize\ttfamily virtual \textbf{ Int64} sf\+::\+Sound\+Stream\+::on\+Loop (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}, {\ttfamily [virtual]}}



Change the current playing position in the stream source to the beginning of the loop. 

This function can be overridden by derived classes to allow implementation of custom loop points. Otherwise, it just calls on\+Seek(\+Time\+::\+Zero) and returns 0.

\begin{DoxyReturn}{Returns}
The seek position after looping (or -\/1 if there\textquotesingle{}s no loop) 
\end{DoxyReturn}


Reimplemented in \textbf{ sf\+::\+Music} \doxyref{}{p.}{classsf_1_1_music_aa68a64bdaf5d16e9ed64f202f5c45e03}.

\mbox{\label{classsf_1_1_sound_stream_a907036dd2ca7d3af5ead316e54b75997}} 
\index{sf::SoundStream@{sf::SoundStream}!onSeek@{onSeek}}
\index{onSeek@{onSeek}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{onSeek()}
{\footnotesize\ttfamily virtual void sf\+::\+Sound\+Stream\+::on\+Seek (\begin{DoxyParamCaption}\item[{\textbf{ Time}}]{time\+Offset }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [protected]}, {\ttfamily [pure virtual]}}



Change the current playing position in the stream source. 

This function must be overridden by derived classes to allow random seeking into the stream source.


\begin{DoxyParams}{Parameters}
{\em time\+Offset} & New playing position, relative to the beginning of the stream \\
\hline
\end{DoxyParams}


Implemented in \textbf{ sf\+::\+Music} \doxyref{}{p.}{classsf_1_1_music_a15119cc0419c16bb334fa0698699c02e}.

\mbox{\label{classsf_1_1_sound_stream_a932ff181e661503cad288b4bb6fe45ca}} 
\index{sf::SoundStream@{sf::SoundStream}!pause@{pause}}
\index{pause@{pause}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{pause()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Stream\+::pause (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [virtual]}}



Pause the audio stream. 

This function pauses the stream if it was playing, otherwise (stream already paused or stopped) it has no effect.

\begin{DoxySeeAlso}{See also}
\doxyref{play}{p.}{classsf_1_1_sound_stream_afdc08b69cab5f243d9324940a85a1144}, \doxyref{stop}{p.}{classsf_1_1_sound_stream_a16cc6a0404b32e42c4dce184bb94d0f4} 
\end{DoxySeeAlso}


Implements \textbf{ sf\+::\+Sound\+Source} \doxyref{}{p.}{classsf_1_1_sound_source_a21553d4e8fcf136231dd8c7ad4630aba}.

\mbox{\label{classsf_1_1_sound_stream_afdc08b69cab5f243d9324940a85a1144}} 
\index{sf::SoundStream@{sf::SoundStream}!play@{play}}
\index{play@{play}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{play()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Stream\+::play (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [virtual]}}



Start or resume playing the audio stream. 

This function starts the stream if it was stopped, resumes it if it was paused, and restarts it from the beginning if it was already playing. This function uses its own thread so that it doesn\textquotesingle{}t block the rest of the program while the stream is played.

\begin{DoxySeeAlso}{See also}
\doxyref{pause}{p.}{classsf_1_1_sound_stream_a932ff181e661503cad288b4bb6fe45ca}, \doxyref{stop}{p.}{classsf_1_1_sound_stream_a16cc6a0404b32e42c4dce184bb94d0f4} 
\end{DoxySeeAlso}


Implements \textbf{ sf\+::\+Sound\+Source} \doxyref{}{p.}{classsf_1_1_sound_source_a6e1bbb1f247ed8743faf3b1ed6f2bc21}.

Here is the caller graph for this function\+:
% FIG 7
\mbox{\label{classsf_1_1_sound_stream_a43fade018ffba7e4f847a9f00b353f3d}} 
\index{sf::SoundStream@{sf::SoundStream}!setLoop@{setLoop}}
\index{setLoop@{setLoop}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{setLoop()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Stream\+::set\+Loop (\begin{DoxyParamCaption}\item[{bool}]{loop }\end{DoxyParamCaption})}



Set whether or not the stream should loop after reaching the end. 

If set, the stream will restart from beginning after reaching the end and so on, until it is stopped or set\+Loop(false) is called. The default looping state for streams is false.


\begin{DoxyParams}{Parameters}
{\em loop} & True to play in loop, false to play once\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Loop}{p.}{classsf_1_1_sound_stream_a49d263f9bbaefec4b019bd05fda59b25} 
\end{DoxySeeAlso}
Here is the caller graph for this function\+:
% FIG 8
\mbox{\label{classsf_1_1_sound_stream_af416a5f84c8750d2acb9821d78bc8646}} 
\index{sf::SoundStream@{sf::SoundStream}!setPlayingOffset@{setPlayingOffset}}
\index{setPlayingOffset@{setPlayingOffset}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{setPlayingOffset()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Stream\+::set\+Playing\+Offset (\begin{DoxyParamCaption}\item[{\textbf{ Time}}]{time\+Offset }\end{DoxyParamCaption})}



Change the current playing position of the stream. 

The playing position can be changed when the stream is either paused or playing. Changing the playing position when the stream is stopped has no effect, since playing the stream would reset its position.


\begin{DoxyParams}{Parameters}
{\em time\+Offset} & New playing position, from the beginning of the stream\\
\hline
\end{DoxyParams}
\begin{DoxySeeAlso}{See also}
\doxyref{get\+Playing\+Offset}{p.}{classsf_1_1_sound_stream_ae288f3c72edbad9cc7ee938ce5b907c1} 
\end{DoxySeeAlso}
\mbox{\label{classsf_1_1_sound_stream_a16cc6a0404b32e42c4dce184bb94d0f4}} 
\index{sf::SoundStream@{sf::SoundStream}!stop@{stop}}
\index{stop@{stop}!sf::SoundStream@{sf::SoundStream}}
\doxysubsubsection{stop()}
{\footnotesize\ttfamily void sf\+::\+Sound\+Stream\+::stop (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})\hspace{0.3cm}{\ttfamily [virtual]}}



Stop playing the audio stream. 

This function stops the stream if it was playing or paused, and does nothing if it was already stopped. It also resets the playing position (unlike \doxyref{pause()}{p.}{classsf_1_1_sound_stream_a932ff181e661503cad288b4bb6fe45ca}).

\begin{DoxySeeAlso}{See also}
\doxyref{play}{p.}{classsf_1_1_sound_stream_afdc08b69cab5f243d9324940a85a1144}, \doxyref{pause}{p.}{classsf_1_1_sound_stream_a932ff181e661503cad288b4bb6fe45ca} 
\end{DoxySeeAlso}


Implements \textbf{ sf\+::\+Sound\+Source} \doxyref{}{p.}{classsf_1_1_sound_source_a06501a25b12376befcc7ee1ed4865fda}.



The documentation for this class was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
cmake/\+S\+F\+M\+L-\/2.\+5.\+1/include/\+S\+F\+M\+L/\+Audio/\textbf{ Sound\+Stream.\+hpp}\end{DoxyCompactItemize}
